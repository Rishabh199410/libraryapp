﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using LibraryBILayer.Admin;
using LibraryUtility;

namespace LibraryApp
{
    public class ListObject
    {      
      
        public SelectList EmptyList;       
        public SelectList CDRServiceList;       
        public SelectList RoleList;       
        public ListObject(int SValue, string SelectListType)
        { 
            switch (SelectListType)
            {
                case "EmptyList":
                    EmptyList = new SelectList(new List<string>(),"0"," -- SELECT -- ");
                    break;

                case "CDRServiceList":                   
                    var lstCDRService = ManageLibrary.GetService((Int32)Enums.ServiceTypeID.CDRServices);
                    CDRServiceList = new SelectList(lstCDRService, "ServiceID", "ServiceType", SValue);
                    break;

                case "RoleList":
                    var lstRoleList = ManageAdminRoleClaim.GetAdminRole();
                    RoleList = new SelectList(lstRoleList, "RoleID", "RoleName", SValue);
                    break;
            }
        }
    }
}
