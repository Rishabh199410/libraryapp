﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryApp.ViewModel
{
    public class UploadServiceViewModel
    {
        public string Subject { get; set; }
        public string Question { get; set; }
        public int TotalWords { get; set; }
        public int ServiceID { get; set; }
        public int EducationLevelID { get; set; }
        public int TimeID { get; set; }
        public decimal Price { get; set; }
        public List<ServiceModel> ServiceIDList { get; set; }
    }

    public class ServiceModel
    {
        public int ServiceID { get; set; }
       
    }
}