﻿using LibraryBILayer.Comman;
using LibraryDataLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LibraryApp.Controllers
{
    public class OtherController : Controller
    {
        // GET: Other
        SysEmail emailSys = new SysEmail();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Success(string token)
        {
            try
            {
                string orderid = SolLibrary.GetTokenOrderID(token);
                int result = SolLibrary.checkOrder(orderid, "Paid");

                if (result == 0)
                {
                    System.Guid guid = System.Guid.NewGuid();

                    /*---MAKE PAYMENT----*/
                    var data = new tbl_orderLibrary();
                    //data.PaymentID = paymentId;
                    data.IsActive = true;
                    data.IsAnswerActive = true;
                    //data.PayerID = PayerID;
                    //data.paymentDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    data.status = "Paid";
                    data.paymentDate = DateTime.Now;
                    data.CurType = "AUD";
                    int i = SolLibrary.UpdateSolutionPayment(data, orderid);

                    //SolLibrary.UpdatePaymentDate(orderid,token);
                    var Livedata = SolLibrary.GetStudentMemberid(orderid);
                    /*---GET MEMBERID---*/
                    string memberid = Livedata[0].memberid;
                    decimal totalAmount = Convert.ToDecimal(Livedata[0].TotalAmount);

                    /*---GET STUDENT NAME & EMAIL---*/
                    var studentData = SolLibrary.GetStudentDetails(memberid);

                    string name = studentData[0].Name;
                    string email = studentData[0].Email;

                    var getData = SolLibrary.GetSubjectByID(orderid);

                    foreach (var mainD in getData)
                    {
                        string mainSubject = SolLibrary.GetFileSubject(mainD.Sol_ID);

                        string CartID = SolLibrary.GetCartdataID(mainD.Sol_ID)[0].SolCartID;

                        string price = SolLibrary.GetCartdataID(mainD.Sol_ID)[0].Amount.ToString();

                        /*----SEND MAIL TO CLIENT WITH DOWNLOAD URL----*/
                        StreamReader reader = new StreamReader(Server.MapPath("~/Emailer/final.html"));
                        string readFile = reader.ReadToEnd();
                        string myString = "";
                        myString = readFile;
                     
                        myString = myString.Replace("@@logo_Images@@", "https://www.classmaster.com.au/img/logo.png");
                        myString = myString.Replace("$$orderid$$", orderid);
                        myString = myString.Replace("$$price$$", price);
                        myString = myString.Replace("$$Url$$","https://www.classmaster.com.au/solution?od=" + orderid + "&view_id=" + mainD.Sol_ID);

                        emailSys.sendmail2(email, "Solution Order [" + CartID + "] has been placed", myString);
                        reader.Close();

                        /*----send mail to cs-----*/
                        StreamReader reader2 = new StreamReader(Server.MapPath("~/Emailer/ReceivedMail.html"));
                        string readfile2 = reader2.ReadToEnd();
                        string mystring2 = "";
                        mystring2 = readfile2;
                        mystring2 = mystring2.Replace("$$assignmentid$$", orderid);
                        mystring2 = mystring2.Replace("$$name$$", name);
                        mystring2 = mystring2.Replace("$$email$$", email);

                        emailSys.sendmail2("cc@eduwebexperts.com", "Solution payment [" + orderid + "] has been recieved !!!!", mystring2);
                        reader2.Close();
                    }

                    if (i == 1)
                    {
                        ViewBag.PaymentID = orderid;
                    }
                    return View();
                }
                else
                {
                    return Redirect("/payment");
                }

            }
            catch (Exception ex)
            {
               
                return Redirect("/failed");
            }
        }
        public ActionResult Failed(string token)
        {
            var data = new tbl_orderLibrary();
            //data.PaymentID = paymentId;
            data.IsActive = false;
            data.IsAnswerActive = false;
            //data.PayerID = PayerID;
            data.paymentDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            data.status = "Failed";
            int i = SolLibrary.UpdateSolutionPayment(data, SolLibrary.GetTokenOrderID(token));

            return View();
        }
        public ActionResult Page404()
        {
            return View();
        }
        public ActionResult Page500()
        {
            return View();
        }
    }
}