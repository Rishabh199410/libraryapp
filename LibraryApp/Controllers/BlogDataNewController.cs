﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LibraryBILayer.Admin;
using LibraryDataLayer;
namespace LibraryApp.Controllers
{
    public class BlogDataNewController : Controller
    {
        // GET: Blog
        public ActionResult Index()
        {
            ViewBag.Blog = ManageBlog.GetBlogList(true);
            ViewBag.BlogCat = ManageBlog.GetBlogCatListD(true);
            return View();
        }
        public ActionResult BlogInfo()
        {
            string mainURL = Request.Url.ToString();
            var SplitURL = mainURL.Split('/').Last();

            tblBlogLib blogLib = ManageBlog.GetBogLib(SplitURL);

            ViewBag.LatestBlogs = ManageBlog.GetLatestBlogByCategory(blogLib.BlogCat);
            return View(blogLib);
        }
        public ActionResult _BlogCategory()
        {
            ViewBag.BlogCat = ManageBlog.GetBlogCatListD(true);

            return View();
        }
   
        public ActionResult BlogCategory()
        {
            string Url = Request.Url.ToString();

            var lastPart = Url.Split('/').Last();

            var tblBlogCat = ManageBlog.GetBlogCatListDUrl(lastPart);

            ViewBag.Blogs = ManageBlog.GetBlogCatListDUrls(tblBlogCat.Id);
            ViewBag.LatestBlogs = ManageBlog.GetLatestBlogByCategory(tblBlogCat.Id);            

            ViewBag.BlogCat = ManageBlog.GetBlogCatListD(true);
            return View(tblBlogCat);
        }
    }
}