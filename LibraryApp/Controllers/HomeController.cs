﻿using Ionic.Zip;
using LibraryApp.Models;
using LibraryBILayer.Comman;
using LibraryDataLayer;
using LibraryUtility;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LibraryApp.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        UserAuthentication userAuth = new UserAuthentication();
        SysEmail emailSys = new SysEmail();
        UserProfile userProfile = new UserProfile();
        [HttpGet]
        public ActionResult Index(int? page)
        {
            ViewBag.SubjectList = SolLibrary.GetSubjectList();

            int pageSize = 30;
            int pageNumber = (page ?? 1);
            return View(SolLibrary.GetSolutionLibrary().ToPagedList(pageNumber, pageSize));
        }
       
        [ChildActionOnly]
        public ActionResult GetNavigationMenu()
        {
            List<tblusernavigation> manuList = Account.GetNavigationMenu();
            return PartialView("~/Views/Home/_Navigation.cshtml", manuList);
        }

        public ActionResult Page(string parameterName)
        {
            string mainURL = Request.Url.ToString();
            var last2 = mainURL.Split('/');
            var SplitURL = last2[4] + "/" + mainURL.Split('/').Last();

            ViewBag.SubjectList = SolLibrary.GetSubjectList();

            string ip_address = SolLibrary.GetIPAddress();
            ViewBag.Count = SolLibrary.CountCart(ip_address);

            ViewBag.MyList = SolLibrary.GetSolutionLibrID(SplitURL);

            /*----SET SESSION----*/
            HttpCookie objadmprof = Request.Cookies["User"];

            if (objadmprof != null)
            {
                ViewBag.User = objadmprof["StudID"];
            }

            return View();
        }
        [HttpPost]
        public JsonResult AutoComplete(string prefix)
        {
           // List<tblSolutionSubject> lstSubject = new List<tblSolutionSubject>();
            // lstSubject = ManageSubject.ObjSubjects(false);
            XpertWebEntities entities = new XpertWebEntities();
            var customers = (from customer in entities.tblSolutionSubjects
                             where customer.Subject.StartsWith(prefix)
                             select new
                             {
                                 label = customer.Subject,
                                 val = customer.SolSubID
                             }).ToList();


            return Json(customers);
        }
        public ActionResult QuestionList(int? page)
        {
            string mainURL = Request.Url.ToString();
            var SplitURL = mainURL.Split('/').Last();
            ViewBag.SubjectList = SolLibrary.GetSubjectList();

            int pageSize = 30;
            int pageNumber = (page ?? 1);
            ViewBag.CountArticle = SolLibrary.CountArticle(SplitURL);
            ViewBag.SubjectName = SolLibrary.getSubject(SplitURL);

            return View(SolLibrary.GetSolutionLibrMyID(SplitURL).ToPagedList(pageNumber, pageSize));
        }
        public ActionResult Cart()
        {    
            /*----BIND DATA IN LIST----*/
            var listObj = new List<SolutionCart>();

            var getCart = SolLibrary.GetCartDetails(SolLibrary.GetIPAddress());
            foreach (var cartm in getCart)
            {
                SolutionCart cartobj = new SolutionCart();

                cartobj.solcartid = cartm.solcartid;
                cartobj.id = cartm.id;
                cartobj.price = cartm.price;
                cartobj.subject = cartm.subject;
                cartobj.desc = cartm.desc;
                cartobj.qty = cartm.qty;
                listObj.Add(cartobj);
            }

            ViewBag.MyCart = listObj;

            return View();
        }
        public ActionResult SaveAddress(decimal? cost, decimal? qty, int QuestionID)
        {
            /*----SET SESSION----*/
            HttpCookie objadmprof = Request.Cookies["User"];

            if (objadmprof != null)
            {

                /*---CHECK ADDRESS IS EXIST---*/


                //tbl_studAddress data = new tbl_studAddress();
                //data.address = obj.address;
                //data.city = obj.city;
                //data.date = DateTime.Now;
                //data.phone = obj.phone;
                //data.state = obj.state;
                //data.zip_code = obj.zip_code;
                //data.studid = userProfile.StudID;
                //data.update_time = DateTime.Now;
                //int i = SolLibrary.updateAdr(data);

                /*---GET TOTAL COST----*/
                TempData["Cost"] = cost;
                TempData["ItemName"] = "Solution Library";

                //string OrderID = DateTime.Now.ToString("yyMMdd") + objadmprof["StudID"].Substring(1, 3) + DateTime.Now.ToString("mmssfff");
                string OrderID = DateTime.Now.ToString("mmssfff");

                /*---GET TOTAL QUANTITY----*/
                //int qty = SolLibrary.SumQTY(userProfile.StudID);

                /*----ADD ORDER LIBRARY-----*/
                tbl_orderLibrary order = new tbl_orderLibrary();
                order.OrderID = OrderID.ToUpper();
                order.memberid = objadmprof["StudID"].ToString();
                order.qty = Convert.ToInt32(qty);
                order.IsAnswerActive = false;
                order.IsActive = false;
                order.TotalAmount = Convert.ToInt32(cost);
                order.CurType = "USD";
                order.CreatedDate = DateTime.Now;

                /*---SAVE ORDER DATA----*/
                SolLibrary.Order_Library(order);

                /*---UPDATE SOLUTION ORDER----*/
                SolLibrary.UpdateOrderID(SolLibrary.GetIPAddress(), OrderID.ToUpper());

                //var payment = PayPalPaymentService.CreatePayment2(GetBaseUrl(), "sale", Convert.ToDouble(cost), TempData["ItemName"].ToString(), TempData["ItemName"].ToString(), 4.37, 1, "AUD", TempData["ItemName"].ToString());
                //string url = LivePaypal.getItemNameAndCost(TempData["ItemName"].ToString(), cost.ToString(), "AUD", "live", qty, OrderID);
                // var payment = PayPalPaymentService.CreatePayment(GetBaseUrl(), "sale", cost, OrderID, OrderID, Convert.ToDouble(CalculateTax(cost.ToString(), "0")), 1, "AUD", OrderID);
                var payment = PayPalMaster.getItemNameAndCost(OrderID, cost.ToString(), "USD", "live", Convert.ToInt32(qty), QuestionID);
                /*---UPDATE TOKEN----*/
                return Redirect(payment);
            }
            else
            {
                return Redirect("~/login");
            }
        }
        public static string CalculateTax(string Amount, string Discount)
        {
            Amount = Amount.Replace("$", "");
            Discount = Discount.Replace("$", "");
            decimal Amt = Convert.ToDecimal(Amount) - Convert.ToDecimal(Discount);
            decimal TaxRate = ((Amt + Convert.ToDecimal(0.30)) / Convert.ToDecimal(0.961)) - Amt;
            TaxRate = Decimal.Round(TaxRate, 2);

            return TaxRate.ToString();
        }
        public string GetBaseUrl()
        {
            return Request.Url.Scheme + "://" + Request.Url.Host + ":" + Request.Url.Port;
        }
        public ActionResult QuestSuccess()
        {
            return View();
        }
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddQuestion(tblQuestion tblQuestion, HttpPostedFileBase[] file)
        {
            string SessionID = string.Empty;
            HttpCookie cookieSession = Request.Cookies["SessionID"];
            if (cookieSession == null)
            {
                HttpCookie cookieQuestion = new HttpCookie("SessionID");
                cookieQuestion["QuestionID"] = System.Web.HttpContext.Current.Session.SessionID;
                Response.Cookies.Add(cookieQuestion);
            }

            SessionID = Request.Cookies["SessionID"]["QuestionID"].ToString();
            HttpCookie objadmprof = Request.Cookies["User"];
            tblquestiondetail tblquestiondetail = new tblquestiondetail();
            string originalPath = System.Configuration.ConfigurationManager.AppSettings["sol"].ToString();
            string questionPath = System.Configuration.ConfigurationManager.AppSettings["ques"].ToString();
            string ZIppath = "";
            //var filenames = file;
            if (file[0] != null)
            {
                //Guid guid = Guid.NewGuid();
                //string encoded = Convert.ToBase64String(guid.ToByteArray());
                //encoded = encoded
                //  .Replace("9/", "_")
                //  .Replace("+", "-");
                string mainID = DateTime.Now.ToString("ddMMyyyyssHHfff");
                /*----GET ID----*/
                int getID = Convert.ToInt32(DateTime.Now.ToString("fff"));

                string path = "";


                //for (int i = 0; i < file.Length; i++)
                int i = 0;
                foreach (var m in file)
                {
                    HttpPostedFileBase PostedFile = m;

                    string FileName = Path.GetFileName(m.FileName);

                    string extension = Path.GetExtension(FileName);

                    path = originalPath + "Q" + i + mainID + getID + extension;

                    PostedFile.SaveAs(Server.MapPath(path));
                    i++;
                }

                /*-----MAKE ZIP FILE------*/
                string modifiedPath = System.Configuration.ConfigurationManager.AppSettings["QuestionModifiedPath"].ToString();
                string Orgpath = Server.MapPath(modifiedPath);//Location for inside Test Folder  
                string mYpath = Server.MapPath(originalPath);

                string[] Filenames = Directory.GetFiles(mYpath, "*" + getID + "*", SearchOption.AllDirectories);
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFiles(Filenames, "Q" + mainID + getID.ToString());//Zip file inside filename  
                    zip.Save(Server.MapPath(modifiedPath + "Q" + mainID + getID.ToString() + ".zip"));//location and name for creating zip file  
                }

                /*----DELETE FILES----*/
                foreach (string filePath in Filenames)
                {
                    System.IO.File.Delete(filePath);
                }

                ZIppath = modifiedPath + "Q" + mainID + getID + ".zip";
            }

            string QuesID = "QD" + DateTime.Now.ToString("ddMMssfff") + "VN" + DateTime.Now.ToString("yyyyfff");

            tblQuestion.CreatedOn = DateTime.Now;
            tblQuestion.QuestionId = QuesID;
            tblQuestion.IsActive = false;
            tblQuestion.FileNames = ZIppath;
            tblQuestion.Subject = tblQuestion.Subject;
            tblQuestion.StatusID = (Int32)Enums.StatusID.Pending;
            tblQuestion.ServiceTypeID = (Int32)Enums.ServiceTypeID.Question24;
            tblQuestion.PaymentStatus = (Int32)Enums.PaymentStatus.Pending;
            tblQuestion.SessionID = SessionID;


            if (Session["single_ques"] != null)
            {
                tblQuestion.Sol_ID = Convert.ToInt32(Session["single_ques"]);
            }

            ManageQuestion.AddQuestion(tblQuestion);

            tblquestiondetail.QuestionID = tblQuestion.Id;
            tblquestiondetail.TotalWords = 0;
            tblquestiondetail.CreatedOn = DateTime.UtcNow;
            tblquestiondetail.Price = 0;
            ManageQuestion.AddQuestionDetail(tblquestiondetail);

            if (objadmprof != null)
            {
                ManageQuestion.UpdateMemberQuestion(QuesID, Request.Cookies["User"]["StudID"].ToString());

                string otherMail = System.Configuration.ConfigurationManager.AppSettings["newMail"].ToString();
                /*----send mail to cs-----*/
                StreamReader reader2 = new StreamReader(Server.MapPath("~/Emailer/question.html"));
                string readfile2 = reader2.ReadToEnd();
                string mystring2 = "";
                mystring2 = readfile2;
                mystring2 = mystring2.Replace("$$assignmentid$$", QuesID);
                mystring2 = mystring2.Replace("$$name$$", Request.Cookies["User"]["Name"].ToString());
                mystring2 = mystring2.Replace("$$email$$", Request.Cookies["User"]["Email"].ToString());

                emailSys.sendmail2(otherMail, "Question [" + QuesID + "] has been recieved !!!!", mystring2);
                reader2.Close();              

                return Redirect("/question-success");
            }
            else
            {
                Session["Ques"] = QuesID;
                return Redirect("/login");
            }

        }
        //[HttpPost]
        //public ActionResult GetCartItem(int ques)
        //{
        //    string ip_address = SolLibrary.GetIPAddress();
        //    /*----ADD CART DETAILS-----*/
        //    if (TempData["cartid"] == null)
        //    {
        //        System.Guid guid = System.Guid.NewGuid();
        //        string CartID = "SL" + DateTime.Now.ToString("yyMMddhhmm").ToUpper() + DateTime.Now.ToString("ss").ToUpper();
        //        TempData["cartid"] = CartID;
        //    }
        //    var item = SolLibrary.GetSolutionLibrID(ques);

        //    /*-----COUNT CART ITEM-------*/
        //    int count_cart = SolLibrary.CheckSol(ip_address, item[0].Sol_ID, TempData["cartid"].ToString());

        //    if (count_cart == 0)
        //    {
        //        tbl_SolCart obj = new tbl_SolCart();

        //        obj.date = DateTime.Now;
        //        obj.IP = ip_address;
        //        obj.qty = 1;
        //        obj.SolCartID = TempData["cartid"].ToString();
        //        obj.Sol_ID = item[0].Sol_ID;
        //        obj.IsActive = false;
        //        obj.Amount =Convert.ToInt32(item[0].Amount);
        //        int i = SolLibrary.AddCart(obj);
        //        /*----END----*/
        //    }


        //    ViewBag.Count = SolLibrary.CountCart(ip_address);
        //    Session["solution_ip"] = ip_address;

        //    //return Json(listObj, JsonRequestBehavior.AllowGet);
        //    return Redirect("~/cart");
        //}

        public ActionResult DelCart(int id)
        {
            /*---DELETE CART----*/
            int i = SolLibrary.DelCart(id);

            string ip_address = SolLibrary.GetIPAddress();

            return Redirect("~/cart");
        }
        [HttpPost]
        public ActionResult Search(string searchtxt)
        {
            ViewBag.Data = SolLibrary.GetSearchData(searchtxt);
            ViewBag.Search = searchtxt;

            return View();
        }
        public ActionResult QuestionPage()
        {
            return View();
        }
        public ActionResult Package()
        {
            return View();
        }
        public ActionResult SingleAnswer()
        {
            return View();
        }
        public ActionResult Terms()
        {
            return View();
        }
        public ActionResult Privacy()
        {
            return View();
        }

        [HttpGet]
        public ActionResult PaymnentSuccess(int c_id)
        {
            SolLibrary.UpdateQuestionID(c_id);
            return View();
        }

        [HttpGet]
        public ActionResult PaymnentFailure(int c_id)
        {
            return View();
        }
    }  
}