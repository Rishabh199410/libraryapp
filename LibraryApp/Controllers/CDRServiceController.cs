﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ionic.Zip;
using LibraryApp.Models;
using LibraryApp.ViewModel;
using LibraryBILayer.Admin;
using LibraryBILayer.Comman;
using LibraryDataLayer;
using LibraryUtility;

namespace LibraryApp.Controllers
{
    public class CDRServiceController : Controller
    {
        SysEmail emailSys = new SysEmail();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Service()
        {
            return View();
        }

        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddCDRService(UploadServiceViewModel request, HttpPostedFileBase[] file)
        {
            string SessionID = string.Empty;
            HttpCookie cookieSession = Request.Cookies["SessionID"];
            if (cookieSession == null)
            {
                HttpCookie cookieQuestion = new HttpCookie("SessionID");
                cookieQuestion["QuestionID"] = System.Web.HttpContext.Current.Session.SessionID;
                Response.Cookies.Add(cookieQuestion);
            }

            SessionID = Request.Cookies["SessionID"]["QuestionID"].ToString();
            HttpCookie objadmprof = Request.Cookies["User"];
            tblQuestion tblQuestion = new tblQuestion();
            tblquestiondetail tblquestiondetail = new tblquestiondetail();
            ReturnModel model = new ReturnModel();

            model.IsSuccess = 0;

            //if (objadmprof == null)
            //{
            //    //model.IsSuccess = -1;
            //    //model.Message = "Please login to upload the document";
            //    //return Json(model);
            //    return Redirect("/login");
            //}


            string originalPath = System.Configuration.ConfigurationManager.AppSettings["sol"].ToString();
            string questionPath = System.Configuration.ConfigurationManager.AppSettings["ques"].ToString();
            string ZIppath = "";

            if (file[0] != null)
            {
                string mainID = DateTime.Now.ToString("ddMMyyyyssHHfff");
                int getID = Convert.ToInt32(DateTime.Now.ToString("fff"));
                string path = "";

                for (int i = 0; i < file.Count(); i++)
                {
                    HttpPostedFileBase PostedFile = file[i];
                    string FileName = Path.GetFileName(PostedFile.FileName);
                    string extension = Path.GetExtension(FileName);
                    path = originalPath + "Q" + i + mainID + getID + extension;
                    PostedFile.SaveAs(Server.MapPath(path));
                    i++;
                }

                /*-----MAKE ZIP FILE------*/
                string modifiedPath = System.Configuration.ConfigurationManager.AppSettings["QuestionModifiedPath"].ToString();
                string Orgpath = Server.MapPath(modifiedPath);//Location for inside Test Folder  
                string mYpath = Server.MapPath(originalPath);

                string[] Filenames = Directory.GetFiles(mYpath, "*" + getID + "*", SearchOption.AllDirectories);
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFiles(Filenames, "Q" + mainID + getID.ToString());//Zip file inside filename  
                    zip.Save(Server.MapPath(modifiedPath + "Q" + mainID + getID.ToString() + ".zip"));//location and name for creating zip file  
                }

                /*----DELETE FILES----*/
                foreach (string filePath in Filenames)
                {
                    System.IO.File.Delete(filePath);
                }
                ZIppath = modifiedPath + "Q" + mainID + getID + ".zip";
            }        

            string QuesID = "QD" + DateTime.Now.ToString("ddMMssfff") + "VN" + DateTime.Now.ToString("yyyyfff");
            if (objadmprof != null)
            {
                tblQuestion.StudID = Request.Cookies["User"]["StudID"].ToString();
            }
            
            tblQuestion.CreatedOn = DateTime.Now;
            tblQuestion.QuestionId = QuesID;
            tblQuestion.IsActive = false;               
            tblQuestion.Subject = request.Subject;           
            tblQuestion.Question = request.Question;
            tblQuestion.FileNames = ZIppath;
            tblQuestion.StatusID = (Int32)Enums.StatusID.Pending;
            tblQuestion.ServiceTypeID = (Int32)Enums.ServiceTypeID.CDRServices;
            tblQuestion.PaymentStatus = (Int32)Enums.PaymentStatus.Pending;
            tblQuestion.SessionID = SessionID;

            if (Session["single_ques"] != null)
            {
                tblQuestion.Sol_ID = Convert.ToInt32(Session["single_ques"]);
            }
            ManageQuestion.AddQuestion(tblQuestion);

            tblquestiondetail.QuestionID = tblQuestion.Id;
            tblquestiondetail.ServiceID = request.ServiceID;              
            tblquestiondetail.CreatedOn = DateTime.UtcNow;
            ManageQuestion.AddQuestionDetail(tblquestiondetail);

            if (objadmprof != null)
            {
                string otherMail = System.Configuration.ConfigurationManager.AppSettings["newMail"].ToString();

                /*----send mail to cs-----*/
                StreamReader reader2 = new StreamReader(Server.MapPath("~/Emailer/question.html"));
                string readfile2 = reader2.ReadToEnd();
                string mystring2 = "";
                mystring2 = readfile2;
                mystring2 = mystring2.Replace("$$assignmentid$$", QuesID);
                mystring2 = mystring2.Replace("$$name$$", Request.Cookies["User"]["Name"].ToString());
                mystring2 = mystring2.Replace("$$email$$", Request.Cookies["User"]["Email"].ToString());

                emailSys.sendmail2(otherMail, "Question [" + QuesID + "] has been recieved !!!!", mystring2);
                reader2.Close();
            }

            if (objadmprof == null)
            {
                return Redirect("/login");
            }

            model.IsSuccess = 1;
            model.Message = "We receive your assignment. Will contact you soon.";
            return Redirect("/question-success");
        }
    }
}