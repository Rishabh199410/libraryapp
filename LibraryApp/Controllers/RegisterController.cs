﻿using LibraryApp.Models;
using LibraryBILayer.Comman;
using LibraryDataLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LibraryApp.Controllers
{
    public class RegisterController : Controller
    {
        // GET: Register
        UserAuthentication userAuth = new UserAuthentication();
        UserProfile userProfile = new UserProfile();

        [HttpGet]
        public ActionResult RegisterUs(string ques)
        {
            HttpCookie objadmprof = Request.Cookies["User"];

            ///*----SIGN UP MEMBER----*/
            //tblCollegeStudent member = new tblCollegeStudent();
            //member.Name = profile.DisplayName;
            //member.Email = profile.Emails.Find(email => email.Type == "account").Value;
            //member.Password = profile.DisplayName.Replace(" ", "") + "@321";
            //member.StudID = memberid;
            //member.TotalEarning = 0;
            //var checkStudid = MakeMember.getStudID(member.Email);

            //if (Session["solution_ip"] != null)
            //{
            //    if (MakeMember.checkStudentExists(member.Email) == 0)
            //    {
            //        MakeMember.AddStudent(member);
            //    }
            //    return Redirect("/order-payment");
            //}
            if (objadmprof != null)
            {
                if (ques != null)
                {

                    string ip_address = SolLibrary.GetIPAddress();
                    /*----ADD CART DETAILS-----*/
                    if (TempData["cartid"] == null)
                    {
                        System.Guid guid = System.Guid.NewGuid();
                        string CartID = "SL" + DateTime.Now.ToString("yyMMddhhmm").ToUpper() + DateTime.Now.ToString("ss").ToUpper();
                        TempData["cartid"] = CartID;
                    }
                    var item = SolLibrary.GetSolutionLibrIDMain(Convert.ToInt32(ques));

                    /*-----COUNT CART ITEM-------*/
                    var count_cart = SolLibrary.CheckSol(ip_address, item[0].Sol_ID);

                    if (count_cart == null)
                    {
                        tbl_SolCart obj = new tbl_SolCart();

                        obj.date = DateTime.Now;
                        obj.IP = ip_address;
                        obj.qty = 1;
                        obj.SolCartID = TempData["cartid"].ToString();
                        obj.Sol_ID = item[0].Sol_ID;
                        obj.IsActive = false;
                        obj.Amount = Convert.ToInt32(item[0].Amount);
                        int i = SolLibrary.AddCart(obj);
                        /*----END----*/
                    }


                    ViewBag.Count = SolLibrary.CountCart(ip_address);
                    Session["solution_ip"] = ip_address;

                    return Redirect("~/cart");
                }
                if (Session["solution_ip"] != null)
                {
                    return Redirect("~/cart");
                }
                if (Session["single_ques"] != null)
                {
                    return Redirect("~/homework-help");
                }
                else
                {
                    return Redirect("~/cart");
                }
            }
            else
            {
                if (ques != null)
                {
                    ViewBag.ques = ques;
                    string ip_address = SolLibrary.GetIPAddress();
                    /*----ADD CART DETAILS-----*/
                    if (TempData["cartid"] == null)
                    {
                        System.Guid guid = System.Guid.NewGuid();
                        string CartID = "SL" + DateTime.Now.ToString("yyMMddhhmm").ToUpper() + DateTime.Now.ToString("ss").ToUpper();
                        TempData["cartid"] = CartID;
                    }
                    var item = SolLibrary.GetSolutionLibrIDMain(Convert.ToInt32(ques));

                    /*-----COUNT CART ITEM-------*/
                    var count_cart = SolLibrary.CheckSol(ip_address, item[0].Sol_ID);

                    if (count_cart == null)
                    {
                        tbl_SolCart obj = new tbl_SolCart();

                        obj.date = DateTime.Now;
                        obj.IP = ip_address;
                        obj.qty = 1;
                        obj.SolCartID = TempData["cartid"].ToString();
                        obj.Sol_ID = item[0].Sol_ID;
                        obj.IsActive = false;
                        obj.Amount = Convert.ToInt32(item[0].Amount);
                        int i = SolLibrary.AddCart(obj);
                        /*----END----*/
                    }


                    ViewBag.Count = SolLibrary.CountCart(ip_address);
                    Session["solution_ip"] = ip_address;
                }

                return View();
            }
        }
        public ActionResult Login(string ques)
        {
            HttpCookie objadmprof = Request.Cookies["User"];

            if (objadmprof != null)
            {
                if (ques != null)
                {

                    string ip_address = SolLibrary.GetIPAddress();
                    /*----ADD CART DETAILS-----*/
                    if (TempData["cartid"] == null)
                    {
                        System.Guid guid = System.Guid.NewGuid();
                        string CartID = "SL" + DateTime.Now.ToString("yyMMddhhmm").ToUpper() + DateTime.Now.ToString("ss").ToUpper();
                        TempData["cartid"] = CartID;
                    }
                    var item = SolLibrary.GetSolutionLibrIDMain(Convert.ToInt32(ques));

                    /*-----COUNT CART ITEM-------*/
                    var count_cart = SolLibrary.CheckSol(ip_address, item[0].Sol_ID);

                    if (count_cart == null)
                    {
                        tbl_SolCart obj = new tbl_SolCart();

                        obj.date = DateTime.Now;
                        obj.IP = ip_address;
                        obj.qty = 1;
                        obj.SolCartID = TempData["cartid"].ToString();
                        obj.Sol_ID = item[0].Sol_ID;
                        obj.IsActive = false;
                        obj.Amount = Convert.ToInt32(item[0].Amount);
                        int i = SolLibrary.AddCart(obj);
                        /*----END----*/
                    }


                    ViewBag.Count = SolLibrary.CountCart(ip_address);
                    Session["solution_ip"] = ip_address;

                    return RedirectToAction("StudentProfile", "Profile", new { area = "Student" });
                }
                if (Session["solution_ip"] != null)
                {
                    return RedirectToAction("StudentProfile", "Profile", new { area = "Student" });
                }
                if (Session["single_ques"] != null)
                {
                    return Redirect("~/homework-help");
                }
                else
                {
                    return RedirectToAction("StudentProfile", "Profile", new { area = "Student" });
                }
            }
            else
            {
                //if (ques != null)
                //{
                //    ViewBag.ques = ques;
                //    string ip_address = SolLibrary.GetIPAddress();
                //    /*----ADD CART DETAILS-----*/
                //    if (TempData["cartid"] == null)
                //    {
                //        System.Guid guid = System.Guid.NewGuid();
                //        string CartID = "SL" + DateTime.Now.ToString("yyMMddhhmm").ToUpper() + DateTime.Now.ToString("ss").ToUpper();
                //        TempData["cartid"] = CartID;
                //    }
                //    var item = SolLibrary.GetSolutionLibrIDMain(Convert.ToInt32(ques));

                //    /*-----COUNT CART ITEM-------*/
                //    int count_cart = SolLibrary.CheckSol(ip_address, item[0].Sol_ID, TempData["cartid"].ToString());

                //    if (count_cart == 0)
                //    {
                //        tbl_SolCart obj = new tbl_SolCart();

                //        obj.date = DateTime.Now;
                //        obj.IP = ip_address;
                //        obj.qty = 1;
                //        obj.SolCartID = TempData["cartid"].ToString();
                //        obj.Sol_ID = item[0].Sol_ID;
                //        obj.IsActive = false;
                //        obj.Amount = Convert.ToInt32(item[0].Amount);
                //        int i = SolLibrary.AddCart(obj);
                //        /*----END----*/
                //    }


                //    ViewBag.Count = SolLibrary.CountCart(ip_address);
                //    Session["solution_ip"] = ip_address;

                //    return Redirect("~/cart");
                //}


                return View();


            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterUs(tblCollegeStudent obj)
        {
            var check = GetRegisterUs.StudentCheck(obj.Email);
            if (check == null)
            {
                if (obj.Name.ToLower().Contains("sex"))
                {
                    TempData["msg"] = "Spam User";
                    return View();
                }
                else if (obj.Name.ToLower().Contains("$"))
                {
                    TempData["msg"] = "Spam User";
                    return View();
                }
                else
                {                    

                    SysEmail emailObj = new SysEmail();

                    string memberid = "CM" + DateTime.Now.ToString("ddMMyyyy") + DateTime.Now.ToString("mmssfff");

                    tblCollegeStudent member = new tblCollegeStudent();
                    member.Name = obj.Name;
                    member.Email = obj.Email;
                    member.Password = obj.Password;
                    member.PhoneNo = obj.PhoneNo;
                    member.StudID = memberid;
                    member.CreatedDate = DateTime.Now;
                    member.IsActive = false;
                    member.country = obj.country;

                    /*----set session profile---*/
                    HttpCookie cookie = new HttpCookie("User");
                    cookie["Email"] = obj.Email;
                    cookie["Name"] = obj.Name;
                    cookie["PhoneNo"] = obj.PhoneNo;
                    cookie["StudID"] = memberid;
                    Response.Cookies.Add(cookie);

                    string SessionID = string.Empty;
                    HttpCookie cookieSession = Request.Cookies["SessionID"];
                    if (cookieSession != null)
                    {
                        SessionID = Request.Cookies["SessionID"]["QuestionID"].ToString();
                        ManageQuestion.UpdateSessionQuestion(SessionID, memberid);
                    }
                   

                    //userProfile.Email = obj.Email;
                    //userProfile.Name = obj.Name;
                    //userProfile.PhoneNo = obj.PhoneNo;
                    //userProfile.StudID = memberid;

                    //userAuth.SetUserSessionProfile(userProfile);

                    if (Session["solution_ip"] != null)
                    {
                        int result = GetRegisterUs.checkOrAddMember(member);

                        if (result == 1)
                        {
                            string mainMail = System.Configuration.ConfigurationManager.AppSettings["mainMail"].ToString();
                            StreamReader reader = new StreamReader(Server.MapPath("~/Emailer/regis.html"));
                            string readFile = reader.ReadToEnd();
                            string myString = "";
                            myString = readFile;
                            myString = myString.Replace("@@name@@", obj.Name);

                            /*----SEND MAIL NOTIFICATIONS TO CS ID----*/
                            emailObj.sendmail2(obj.Email, "Classmaster Registration Successfull", myString);
                            //ado.SendMail(assignmentID, myString, myStud.email, "Assignment Order - " + assignmentID);
                            reader.Close();

                            return Redirect("~/login");
                        }
                        else
                        {
                            TempData["msg"] = "Already Registerd, Please login";
                            return View();
                        }
                    }
                    if (Session["single_ques"] != null)
                    {
                        GetRegisterUs.checkOrAddMember(member);

                        string mainMail = System.Configuration.ConfigurationManager.AppSettings["mainMail"].ToString();
                        StreamReader reader = new StreamReader(Server.MapPath("~/Emailer/regis.html"));
                        string readFile = reader.ReadToEnd();
                        string myString = "";
                        myString = readFile;
                        myString = myString.Replace("@@name@@", obj.Name);

                        /*----SEND MAIL NOTIFICATIONS TO CS ID----*/
                        emailObj.sendmail2(obj.Email, "Classmaster Registration Successfull", myString);
                        //ado.SendMail(assignmentID, myString, myStud.email, "Assignment Order - " + assignmentID);
                        reader.Close();

                        ManageQuestion.UpdateMemberQuestion(Session["Ques"].ToString(), memberid);

                        string otherMail = System.Configuration.ConfigurationManager.AppSettings["newMail"].ToString();
                        /*----send mail to cs-----*/
                        StreamReader reader2 = new StreamReader(Server.MapPath("~/Emailer/question.html"));
                        string readfile2 = reader2.ReadToEnd();
                        string mystring2 = "";
                        mystring2 = readfile2;
                        mystring2 = mystring2.Replace("$$assignmentid$$", Session["Ques"].ToString());
                        mystring2 = mystring2.Replace("$$name$$", Request.Cookies["User"]["Name"].ToString());
                        mystring2 = mystring2.Replace("$$email$$", Request.Cookies["User"]["Email"].ToString());

                        emailObj.sendmail2(otherMail, "Question [" + Session["Ques"].ToString() + "] has been recieved !!!!", mystring2);
                        reader2.Close();

                        return Redirect("~/question-success");
                    }
                    if (Session["Ques"] != null)
                    {
                        GetRegisterUs.checkOrAddMember(member);

                        string mainMail = System.Configuration.ConfigurationManager.AppSettings["mainMail"].ToString();
                        StreamReader reader = new StreamReader(Server.MapPath("~/Emailer/regis.html"));
                        string readFile = reader.ReadToEnd();
                        string myString = "";
                        myString = readFile;
                        myString = myString.Replace("@@name@@", obj.Name);

                        /*----SEND MAIL NOTIFICATIONS TO CS ID----*/
                        emailObj.sendmail2(obj.Email, "Classmaster Registration Successfull", myString);
                        //ado.SendMail(assignmentID, myString, myStud.email, "Assignment Order - " + assignmentID);
                        reader.Close();

                        ManageQuestion.UpdateMemberQuestion(Session["Ques"].ToString(), memberid);

                        string otherMail = System.Configuration.ConfigurationManager.AppSettings["newMail"].ToString();
                        /*----send mail to cs-----*/
                        StreamReader reader2 = new StreamReader(Server.MapPath("~/Emailer/question.html"));
                        string readfile2 = reader2.ReadToEnd();
                        string mystring2 = "";
                        mystring2 = readfile2;
                        mystring2 = mystring2.Replace("$$assignmentid$$", Session["Ques"].ToString());
                        mystring2 = mystring2.Replace("$$name$$", Request.Cookies["User"]["Name"].ToString());
                        mystring2 = mystring2.Replace("$$email$$", Request.Cookies["User"]["Email"].ToString());

                        emailObj.sendmail2(otherMail, "Question [" + Session["Ques"].ToString() + "] has been recieved !!!!", mystring2);
                        reader2.Close();

                        return Redirect("~/question-success");
                    }
                    else
                    {
                        int result = GetRegisterUs.checkOrAddMember(member);

                        if (result == 1)
                        {

                            string mainMail = System.Configuration.ConfigurationManager.AppSettings["mainMail"].ToString();
                            StreamReader reader = new StreamReader(Server.MapPath("~/Emailer/regis.html"));
                            string readFile = reader.ReadToEnd();
                            string myString = "";
                            myString = readFile;
                            myString = myString.Replace("@@name@@", obj.Name);

                            /*----SEND MAIL NOTIFICATIONS TO CS ID----*/
                            emailObj.sendmail2(obj.Email, "Classmaster Registration Successfull", myString);
                            //ado.SendMail(assignmentID, myString, myStud.email, "Assignment Order - " + assignmentID);
                            reader.Close();

                            return Redirect("~/login");
                        }
                        else
                        {
                            TempData["msg"] = "Already Registerd, Please login";
                            return View();
                        }
                    }
                }
            }
            else
            {
                TempData["msg"] = "Already Registerd, Please login";
                return View();
            }


        }
        [HttpPost]
        public ActionResult Login(tblCollegeStudent obj)
        {
            string SessionID = string.Empty;
            HttpCookie cookieSession = Request.Cookies["SessionID"];
            if (cookieSession != null)
            {
                SessionID = Request.Cookies["SessionID"]["QuestionID"].ToString();
            }           

            GetRegisterUs objd = new GetRegisterUs();
            var data = objd.UserLoginCheck2(obj.Email, obj.Password);

            if (data != null)
            {

                HttpCookie cookie = new HttpCookie("User");
                cookie["Email"] = obj.Email;
                cookie["Name"] = data.Name;
                cookie["PhoneNo"] = data.PhoneNo;
                cookie["StudID"] = data.StudID;
                Response.Cookies.Add(cookie);
                /*----set session profile---*/
                //userProfile.Email = obj.Email;
                //userProfile.Name = data.Name;
                //userProfile.PhoneNo = data.PhoneNo;
                //userProfile.StudID = data.StudID;

                //userAuth.SetUserSessionProfile(userProfile);

                if (Session["Ques"] != null)
                {
                    SysEmail emailObj = new SysEmail();

                    ManageQuestion.UpdateMemberQuestion(Session["Ques"].ToString(), data.StudID);

                    string otherMail = System.Configuration.ConfigurationManager.AppSettings["newMail"].ToString();
                    /*----send mail to cs-----*/
                    StreamReader reader2 = new StreamReader(Server.MapPath("~/Emailer/question.html"));
                    string readfile2 = reader2.ReadToEnd();
                    string mystring2 = "";
                    mystring2 = readfile2;
                    mystring2 = mystring2.Replace("$$assignmentid$$", Session["Ques"].ToString());
                    mystring2 = mystring2.Replace("$$name$$", Request.Cookies["User"]["Name"].ToString());
                    mystring2 = mystring2.Replace("$$email$$", Request.Cookies["User"]["Email"].ToString());

                    emailObj.sendmail2(otherMail, "Question [" + Session["Ques"].ToString() + "] has been recieved !!!!", mystring2);
                    reader2.Close();

                    return Redirect("~/question-success");
                }

                if (Session["single_ques"] != null)
                {
                    SysEmail emailObj = new SysEmail();

                    ManageQuestion.UpdateMemberQuestion(Session["Ques"].ToString(), data.StudID);

                    string otherMail = System.Configuration.ConfigurationManager.AppSettings["newMail"].ToString();
                    /*----send mail to cs-----*/
                    StreamReader reader2 = new StreamReader(Server.MapPath("~/Emailer/question.html"));
                    string readfile2 = reader2.ReadToEnd();
                    string mystring2 = "";
                    mystring2 = readfile2;
                    mystring2 = mystring2.Replace("$$assignmentid$$", Session["Ques"].ToString());
                    mystring2 = mystring2.Replace("$$name$$", Request.Cookies["User"]["Name"].ToString());
                    mystring2 = mystring2.Replace("$$email$$", Request.Cookies["User"]["Email"].ToString());

                    emailObj.sendmail2(otherMail, "Question [" + Session["Ques"].ToString() + "] has been recieved !!!!", mystring2);
                    reader2.Close();

                    return Redirect("~/question-success");
                }
                else
                {
                    ManageQuestion.UpdateSessionQuestion(SessionID, data.StudID);
                    return RedirectToAction("StudentProfile", "Profile", new { area = "Student" });
                }
            }
            else
            {
                TempData["msg"] = "Incorrect details, Please login again";
                return View();
            }
        }
        public ActionResult Logout()
        {
            if (Request.Cookies["User"] != null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);

            }
            return Redirect("~/");
        }

        [HttpGet]
        public ActionResult ForgetPassword()
        {
            return View();
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult ForgetPassword(string email)
        {         
            ReturnModel model = new ReturnModel();
            SysEmail emailObj = new SysEmail();

            model.IsSuccess = 0;

            if (string.IsNullOrWhiteSpace(email) || string.IsNullOrEmpty(email))
            {
                TempData["msg"] = "Please enter email id";
                return View();
            }

            forgetpasswordlink item = new forgetpasswordlink();
            item.Link = Guid.NewGuid();

            model.IsSuccess = GetRegisterUs.InsertPasswordLink(item, email);
            if (model.IsSuccess == -1)
            {
                TempData["msg"] = "Email id does not exists";
                return View();
            }

            if (model.IsSuccess > 0)
            {
                string url = "https://www.classmaster.com.au/reset-password?Link=" + item.Link;
                StreamReader reader = new StreamReader(Server.MapPath("~/Emailer/ForgetPassword.html"));
                string readFile = reader.ReadToEnd();
                string myString = "";
                myString = readFile;
                myString = myString.Replace("@@link@@", url);
                emailObj.sendmail2(email, "Forget password link", myString);
                reader.Close();

                TempData["msg"] = "Password reset link sent to your registered email id";
                return View();
            }

            TempData["msg"] = "Some server error occurred! Please try again later";
            return View();
        }

        [HttpGet]
        public ActionResult ResetPassword(string Link)
        {
            ViewBag.Success = 0;
            var item = GetRegisterUs.ForgetPasswordLinkCheckCheck(Link);

            if (item != null)
            {
                ViewBag.Success = 1;
                ViewBag.Link = Link;
            }
            return View();
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult ResetPassword(string password, string Link)
        {
            ReturnModel model = new ReturnModel();
            SysEmail emailObj = new SysEmail();

            model.IsSuccess = 0;

            if (string.IsNullOrWhiteSpace(password) || string.IsNullOrEmpty(password))
            {
                TempData["msg"] = "Password cannot be empty";
                return View();
            }

            model.IsSuccess = GetRegisterUs.ResetPassword(password, Link);
            if (model.IsSuccess > 0)
            {
                TempData["msg"] = "Password updated successfully";
                return Redirect("~/login");
            }
            return View();
        }
    }
}