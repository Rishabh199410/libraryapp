﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LibraryDataLayer;
namespace LibraryApp.Models
{
    public class FillDropDown
    {
        public SelectList GetBlogCategory()
        {
            SelectList ConList;
            using (var context = new XpertWebEntities())
            {
                var ObjMastersCat = (from c in context.tblBlogCats
                                     select new DropDownProperty()
                                     {
                                         ID = c.Id,
                                         Value = c.BlogCatName
                                     }).ToList();
                ConList = new SelectList(ObjMastersCat, "ID", "Value");
                return ConList;
            }
        }
    }
    public class DropDownProperty
    {
        public long ID { get; set; }
        public string Value { get; set; }
    }
}