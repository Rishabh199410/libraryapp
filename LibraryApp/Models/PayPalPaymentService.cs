﻿using PayPal.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LibraryBILayer.Comman;
namespace LibraryApp
{
    public static class PayPalPaymentService
    {
        public static Payment CreatePayment(string baseUrl, string intent, double price, string itemName, string itemdesc, double tax, int qty, string our_currency, string orderno)
        {
            // ### Api Context
            // Pass in a `APIContext` object to authenticate 
            // the call and to send a unique request id 
            // (that ensures idempotency). The SDK generates
            // a request id if you do not pass one explicitly. 
            var apiContext = PayPalConfiguration.GetAPIContext();

            // Payment Resource
            var payment = new Payment()
            {
                intent = intent,    // `sale` or `authorize`
                payer = new Payer() { payment_method = "paypal" },
                transactions = GetTransactionsList(price, itemName, itemdesc, tax, qty, our_currency, orderno),
                redirect_urls = GetReturnUrls(baseUrl, intent)
            };

            // Create a payment using a valid APIContext
            var createdPayment = payment.Create(apiContext);

            SolLibrary.UpdateToken(itemdesc, createdPayment.token);

            return createdPayment;
        }
        //public static Payment CreatePayment2(string baseUrl, string intent, double price, string itemName, string itemdesc, double tax, int qty, string our_currency, string orderno)
        //{
        //    // ### Api Context
        //    // Pass in a `APIContext` object to authenticate 
        //    // the call and to send a unique request id 
        //    // (that ensures idempotency). The SDK generates
        //    // a request id if you do not pass one explicitly. 
        //    var apiContext = PayPalConfiguration.GetAPIContext();

        //    // Payment Resource
        //    var payment = new Payment()
        //    {
        //        intent = intent,    // `sale` or `authorize`
        //        payer = new Payer() { payment_method = "paypal" },
        //        transactions = GetTransactionsList(price, itemName, itemdesc, tax, qty, our_currency, orderno),
        //        redirect_urls = GetReturnUrlss(baseUrl, intent)
        //    };

        //    // Create a payment using a valid APIContext
        //    var createdPayment = payment.Create(apiContext);

        //    return createdPayment;
        //}
        private static List<Transaction> GetTransactionsList(double price, string itemName, string itemdesc, double tax, int qty, string our_currency, string orderno)
        {
            // A transaction defines the contract of a payment
            // what is the payment for and who is fulfilling it. 
            var transactionList = new List<Transaction>();

            // The Payment creation API requires a list of Transaction; 
            // add the created Transaction to a List
            double totalPrice = price + tax;
            transactionList.Add(new Transaction()
            {
                description = itemdesc,
                invoice_number = GetRandomInvoiceNumber(),
                amount = new Amount()
                {
                    currency = our_currency,
                    total = totalPrice.ToString(),       // Total must be equal to sum of shipping, tax and subtotal.
                    details = new Details() // Details: Let's you specify details of a payment amount.
                    {
                        tax = tax.ToString(),
                        shipping = "0",
                        subtotal = price.ToString()
                    }
                },
                item_list = new ItemList()
                {
                    items = new List<Item>()
            {
                new Item()
                {
                   name = itemName,
                            currency = our_currency,
                            price = price.ToString(),
                            quantity = qty.ToString(),
                            sku = orderno
                }
                    }
                }
            });
            return transactionList;
        }

        private static RedirectUrls GetReturnUrls(string baseUrl, string intent)
        {
            var returnUrl = intent == "sale" ? "/paycapture" : "/success";

            // Redirect URLS
            // These URLs will determine how the user is redirected from PayPal 
            // once they have either approved or canceled the payment.
            return new RedirectUrls()
            {
                cancel_url = baseUrl + "/failed",
                return_url = baseUrl + returnUrl
            };
        }
        //private static RedirectUrls GetReturnUrlss(string baseUrl, string intent)
        //{
        //    var returnUrl = intent == "sale" ? "/Comman/SolutionPayment/PaymentSuccessful" : "/Comman/SolutionPayment/AuthorizeSuccessful";

        //    // Redirect URLS
        //    // These URLs will determine how the user is redirected from PayPal 
        //    // once they have either approved or canceled the payment.
        //    return new RedirectUrls()
        //    {
        //        cancel_url = baseUrl + "/Comman/SolutionPayment/PaymentCancelled",
        //        return_url = baseUrl + returnUrl
        //    };
        //}

        public static Payment ExecutePayment(string paymentId, string payerId)
        {

            var apiContext = PayPalConfiguration.GetAPIContext();

            var paymentExecution = new PaymentExecution() { payer_id = payerId };
            var payment = new Payment() { id = paymentId };

            // Execute the payment.
            var executedPayment = payment.Execute(apiContext, paymentExecution);

            return executedPayment;
        }
        //public static Capture CapturePayment(string paymentId)
        //{
        //    var apiContext = PayPalConfiguration.GetAPIContext();

        //    var payment = Payment.Get(apiContext, paymentId);
        //    var auth = payment.transactions[0].related_resources[0].authorization;

        //    // Specify an amount to capture.  By setting 'is_final_capture' to true, all remaining funds held by the authorization will be released from the funding instrument.
        //    var capture = new Capture()
        //    {
        //        amount = new Amount()
        //        {
        //            currency = "USD",
        //            total = "4.54"
        //        },
        //        is_final_capture = true
        //    };

        //    // Capture an authorized payment by POSTing to
        //    // URI v1/payments/authorization/{authorization_id}/capture
        //    var responseCapture = auth.Capture(apiContext, capture);

        //    return responseCapture;
        //}

        public static string GetRandomInvoiceNumber()
        {
            return new Random().Next(999999).ToString();
        }
    }
}