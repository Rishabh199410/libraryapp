﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryApp.Models
{
    public class AdminRolesModel
    {
        public int AdminNavigationID { get; set; }
        public string AdminNavigation { get; set; }
        public bool IsSelected { get; set; }
        public bool? IsParent { get; set; }
   
    }   
}