﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace LibraryApp.Models
{
    public class RemHTML
    {
        public static string StripHTML(string input)
        {
            string pattern = @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;";

            return Regex.Replace(input, pattern, string.Empty);
        }
    }
}