﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryApp.Models
{
    public class PayMod
    {
        public string AssignmentID { get; set; }
        public string MemberID { get; set; }
        public double amount { get; set; }
        public string token { get; set; }
        public string status { get; set; }
        public DateTime createdDate { get; set; }
        public int flag { get; set; }
        public string currency { get; set; }
        public string PayMail { get; set; }
    }
    public class PayCon
    {
        public string assignmentId { get; set; }
        public string mailid { get; set; }
        public double? amount { get; set; }
        public string currency { get; set; }
    }
}