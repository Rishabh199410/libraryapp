﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryApp.Models
{
    public class ReturnModel
    {
        public int IsSuccess { get; set; }
        public string Message { get; set; }
    }
}