﻿using LibraryBILayer.User;
using LibraryDataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryApp.Models
{
    public class UserAuthentication
    {
        public string Email { get; set; }
        public string Password { get; set; }


        public bool IsLogin()
        {
            bool result = false;
            if (HttpContext.Current.Session[FixedVariablesUser.SESSION_USER_PROFILE] != null)
            {
                result = true;
            }
            return result;
        }

        public void Logout()
        {
            HttpContext.Current.Session[FixedVariablesUser.SESSION_USER_PROFILE] = null;
        }

        public UserProfile SetUserSessionProfile(UserProfile objUserprofile)
        {
            if (!object.Equals(objUserprofile, null))
            {
                //var cookie = new HttpCookie(FixedVariablesUser.SESSION_USER_PROFILE);
                HttpContext.Current.Session[FixedVariablesUser.SESSION_USER_PROFILE] = objUserprofile;
                //HttpContext.Current.Response.Cache.SetExpires(DateTime.Now.AddMinutes(555));
                //HttpContext.Current.Cache.sete
                // HttpContext.Current.Cache[FixedVariablesUser.SESSION_USER_PROFILE]= objUserprofile;
                //HttpContext.Current.Cache[FixedVariablesUser.SESSION_USER_PROFILE]= objUserprofile;
                //HttpContext.Current.Response.Cache.SetExpires(DateTime.Now.AddMinutes(220));
            }
            return (UserProfile)HttpContext.Current.Session[FixedVariablesUser.SESSION_USER_PROFILE];
        }

        public UserProfile GetUserSessionProfile()
        {
            UserProfile obj = null;
            if (HttpContext.Current.Session[FixedVariablesUser.SESSION_USER_PROFILE] != null)
            {
                obj = (UserProfile)HttpContext.Current.Session[FixedVariablesUser.SESSION_USER_PROFILE];
            }
            return obj;
        }

        public UserProfile UpdateUserSessionProfile(int step)
        {
            UserProfile obj = null;
            if (HttpContext.Current.Session[FixedVariablesUser.SESSION_USER_PROFILE] != null)
            {
                obj = (UserProfile)HttpContext.Current.Session[FixedVariablesUser.SESSION_USER_PROFILE];
                obj.NoOfProcessComplted = step;
            }
            return obj;
        }

        //public UserProfile UpdateUserSessionProfile(long userId, int userType)
        //{
        //    UserProfile obj = null;

        //    obj = new UserProfile(userId, userType);
        //    if (!object.Equals(obj, null))
        //    {
        //        UpdateUserSessionProfile(obj);

        //    }
        //    return obj;
        //}

        public UserProfile UpdateUserSessionProfile(UserProfile objUserprofile)
        {
            if (!object.Equals(objUserprofile, null))
            {
                HttpContext.Current.Session[FixedVariablesUser.SESSION_USER_PROFILE] = null;
                HttpContext.Current.Session[FixedVariablesUser.SESSION_USER_PROFILE] = objUserprofile;
            }
            return (UserProfile)HttpContext.Current.Session[FixedVariablesUser.SESSION_USER_PROFILE];
        }

    }



    public class UserProfile
    {
        UserAuthentication objUserAuth = new UserAuthentication();
        //UserAccount objUsr = new UserAccount();
        tblCollegeStudent tblC = new tblCollegeStudent();
        public long Id { get; set; }
        public int? AccountType { get; set; }
        public string NickName { get; set; }
        public int id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
        public string AcPassword { get; set; }
        public string StudID { get; set; }
        public bool IAgree { get; set; }
        public string PhoneNo { get; set; }
        public bool IsADeleted { get; set; }
        public string Status { get; set; }
        public bool IsExist { get; set; }
        public bool IsCategoryCmplt { get; set; }
        public bool IsWorkSampleCmplt { get; set; }
        public bool IsPersoanlInfoCmplt { get; set; }
        public bool IsProfInfoCmplt { get; set; }
        public int NoOfProcessComplted { get; set; }

        public bool IsSubscribed { get; set; }
        //public UserSubscription SubscriptionDetails { get; set; }
        public string userImage { get; set; }

        public UserProfile()
        {

        }

        public UserProfile(string _email, string _password)
        {
            UserLogin obj = new UserLogin();
            tblC = obj.UserLoginCheck(_email, _password);
            if (tblC != null)
            {
                this.IsExist = true;
                this.Email = tblC.Email;
                this.Name = tblC.Name;
                this.PhoneNo = tblC.PhoneNo;
                this.StudID = tblC.StudID;

                //this.userImage = obj.GetUserImage(this.Id, this.AccountType);



            }
            else
                this.IsExist = false;
        }

        //public UserProfile(long _userId, int _userType)
        //{
        //    UserLogin obj = new UserLogin();
        //    objUsr = obj.GetUserDetails(_userId, _userType);
        //    if (objUsr != null)
        //    {
        //        this.IsExist = true;
        //        this.AccountType = objUsr.AccountType;
        //        this.NickName = objUsr.NickName;
        //        this.Email = objUsr.Email;
        //        this.FirstName = objUsr.FirstName;
        //        this.LastName = objUsr.LastName;
        //        this.Contact = objUsr.Contact;
        //        this.Status = objUsr.Status;
        //        this.Id = objUsr.Id;
        //        this.IsCategoryCmplt = objUsr.IsCategoryCmplt;
        //        this.IsWorkSampleCmplt = objUsr.IsWorkSampleCmplt;
        //        this.IsPersoanlInfoCmplt = objUsr.IsPersoanlInfoCmplt;
        //        this.IsPersoanlInfoCmplt = objUsr.IsPersoanlInfoCmplt;
        //        this.NoOfProcessComplted = objUsr.NoOfProcessComplted;
        //        //this.userImage = obj.GetUserImage(this.Id, this.AccountType);
        //        //this.SubscriptionDetails = obj.GetUserSubscription(this.Id, this.AccountType, SysTimeZone.GetUserTimeZone());
        //        if (!object.Equals(this.SubscriptionDetails, null))
        //        {
        //            this.IsSubscribed = true;
        //        }

        //    }
        //    else
        //        this.IsExist = false;
        //}

    }

    #region System fixed veriable class

    public class FixedVariablesUser
    {
        #region All the Session veriables defined here
        public const string SESSION_USER_PROFILE = "AllUserProfileDataInSession";
        public const string SESSION_SUBCRIPTION_PROFILE = "AllUserSubcriptionDataInSession";
        public const string SESSION_ASSIGNMENT = "Assignment_session_before_login";
        #endregion




    }




    #endregion
}