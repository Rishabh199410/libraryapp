﻿using System.Web;
using System.Web.Mvc;
using LibraryDataLayer;
using LibraryBILayer.User;
using System;

namespace LibraryApp.Areas.NewAdmin.Controllers
{
    public class ProfileController : Controller
    {           
        public ActionResult Index()
        {
            HttpCookie objadmprof = Request.Cookies["User"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Home");
            }
            //tblCollegeStudent collegeStudent = StudentProfile.getStudentById(id);
            return View("~/Areas/Student/Views/Profile/Index.cshtml");
        }

        [HttpGet]
        public ActionResult StudentProfile()
        {
            HttpCookie objadmprof = Request.Cookies["User"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Home");
            }
            string email = objadmprof.Value;
            var email1 = email.Split('&');
            string emailid = email1[0].Replace("Email=", "");
            StudentProfile studentProfile = new StudentProfile();
            var result = studentProfile.UserProfile(emailid);
            return View(result);
        }

        public ActionResult UpdateProfile(string id)
        {
            HttpCookie objadmprof = Request.Cookies["User"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Home");
            }
            using (var contect = new XpertWebEntities())
            {
                StudentProfile studentProfile = new StudentProfile();
                var result = studentProfile.UserProfile(id);
                return View(result);
            }
                
        }

        [HttpPost]
        public ActionResult UpdateProfile(tblCollegeStudent collegeStudent)
        {
            HttpCookie objadmprof = Request.Cookies["User"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Home");
            }
            StudentProfile studentProfile = new StudentProfile();
            int result = studentProfile.UpdateProfile(collegeStudent);
            return RedirectToAction("StudentProfile", "Profile");
        }

        [HttpGet]
        public ActionResult Logout()
        {
            if (Request.Cookies["User"] != null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);

            }
            return Redirect("~/login/Index");
        }
    }
}