﻿using System.Web;
using System.Web.Mvc;
using LibraryDataLayer;
using LibraryBILayer.User;
using System.Collections.Generic;
using LibraryBILayer.Comman;
using LibraryBILayer.User;

namespace LibraryApp.Areas.NewAdmin.Controllers
{
    public class RejectedController : Controller
    {           
        public ActionResult Index()
        {
            HttpCookie objadmprof = Request.Cookies["User"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View("~/Areas/Student/Views/Profile/Index.cshtml");
        }

        public ActionResult RejectedAssignment()
        {

            HttpCookie objadmprof = Request.Cookies["User"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Home");
            }
            RejectedAssignments RejectedAssignments = new RejectedAssignments();
            string studentid = objadmprof.Value;
            var id = studentid.Split('&');
            string Id = id[3].Replace("StudID=", "");
            List<JoinClassForPCR> result = RejectedAssignments.getRejectedAssignment(Id);

            return View(result);
        }

        public ActionResult RejectedresultById(int id)
        {
            HttpCookie objadmprof = Request.Cookies["User"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Home");
            }
            RejectedAssignments RejectedAssignments = new RejectedAssignments();
            string studentid = objadmprof.Value;
            var id1 = studentid.Split('&');
            string StudId = id1[3].Replace("StudID=", "");
            JoinClassForPCR result = RejectedAssignments.getRejectedAssignmentbyId(StudId,id);
            return View(result);
        }
    
    }
}