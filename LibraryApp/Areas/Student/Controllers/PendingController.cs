﻿using System.Web;
using System.Web.Mvc;
using LibraryDataLayer;
using LibraryBILayer.User;
using System.Collections.Generic;
using LibraryBILayer.Comman;
using LibraryBILayer.User;

namespace LibraryApp.Areas.NewAdmin.Controllers
{
    public class PendingController : Controller
    {           
        public ActionResult Index()
        {
            HttpCookie objadmprof = Request.Cookies["User"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View("~/Areas/Student/Views/Profile/Index.cshtml");
        }

        public ActionResult PendingAssignment()
        {

            HttpCookie objadmprof = Request.Cookies["User"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Home");
            }
            PendingAssignments pendingAssignments = new PendingAssignments();
            string studentid = objadmprof.Value;
            var id = studentid.Split('&');
            string Id = id[3].Replace("StudID=", "");
            List<JoinClassForPCR> result = pendingAssignments.getPendingAssignment(Id);
            foreach (var item in result)
            {
                if (item.Price == null)
                    item.Price = 0m;

                if (item.PaymentStatusId == null)
                    item.PaymentStatusId = 0;

                if (item.AssignmentStatusID == null)
                    item.AssignmentStatusID = 0;

            }

            return View(result);
        }

        public ActionResult PendingresultById(int id)
        {
            HttpCookie objadmprof = Request.Cookies["User"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Home");
            }
            PendingAssignments pendingAssignments = new PendingAssignments();
            string studentid = objadmprof.Value;
            var id1 = studentid.Split('&');
            string StudId = id1[3].Replace("StudID=", "");
            JoinClassForPCR result = pendingAssignments.getPendingAssignmentbyId(StudId,id);
            return View(result);
        }
    
    }
}