﻿using System.Web;
using System.Web.Mvc;
using LibraryBILayer.User;
using System.Collections.Generic;
using LibraryBILayer.Comman;

namespace LibraryApp.Areas.NewAdmin.Controllers
{
    public class CompletedController : Controller
    {           
        public ActionResult Index()
        {
            HttpCookie objadmprof = Request.Cookies["User"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View("~/Areas/Student/Views/Profile/Index.cshtml");
        }

        public ActionResult CompletedAssignment()
        {

            HttpCookie objadmprof = Request.Cookies["User"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Home");
            }
            CompletedAssignments CompletedAssignments = new CompletedAssignments();
            string studentid = objadmprof.Value;
            var id = studentid.Split('&');
            string Id = id[3].Replace("StudID=", "");
            List<JoinClassForPCR> result = CompletedAssignments.getCompletedAssignment(Id);

            return View(result);
        }

        public ActionResult CompletedresultById(int id)
        {
            HttpCookie objadmprof = Request.Cookies["User"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Home");
            }
            CompletedAssignments CompletedAssignments = new CompletedAssignments();
            string studentid = objadmprof.Value;
            var id1 = studentid.Split('&');
            string StudId = id1[3].Replace("StudID=", "");
            JoinClassForPCR result = CompletedAssignments.getCompletedAssignmentbyId(StudId,id);
            return View(result);
        }
    
    }
}