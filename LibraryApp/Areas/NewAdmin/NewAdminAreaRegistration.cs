﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LibraryApp.Areas.NewAdmin
{
    public class NewAdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "NewAdmin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {

            
           // context.MapRoute(
           //    "Admin_default2",
           //    "NewAdmin/SolutionLibrary/{action}/{id}",
           //    new { controller = "SolutionLibrary", action = "Index", id = UrlParameter.Optional }
           //);
            context.MapRoute(
                "Admin_default",
                "NewAdmin/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "Admin_Login",
                "adm/login",
                new { controller = "Login", action = "AddLogin", id = UrlParameter.Optional }
            );
            // context.MapRoute(
            //    "Pending_tutors",
            //    "Admin/Tutor/Pending/{userid}",
            //    new { controller = "Tutor", action = "TutorDetails", userid = UrlParameter.Optional }
            //);
        }
    }
}