﻿//using LatestLiveWebBILayer.Admin;
using LibraryBILayer.Admin;
using LibraryDataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryApp.Areas.NewAdmin.Models
{
    public class AdminAuthentication
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }

        public bool IsLogin()
        {
            bool result = false;
            if (HttpContext.Current.Session[FixedVariables.SESSION_ADMIN_PROFILE] != null)
            {
                result = true;
            }
            return result;
        }

        public void Logout()
        {
            HttpContext.Current.Session[FixedVariables.SESSION_ADMIN_PROFILE] = null;
        }

        public AdminProfile SetAdminSessionProfile(AdminProfile objAdmprofile)
        {
            if (!object.Equals(objAdmprofile, null))
            {
                HttpContext.Current.Session[FixedVariables.SESSION_ADMIN_PROFILE] = objAdmprofile;
            }
            return (AdminProfile)HttpContext.Current.Session[FixedVariables.SESSION_ADMIN_PROFILE];
        }

        public AdminProfile GetAdminSessionProfile()
        {
            AdminProfile obj = null;
            if (HttpContext.Current.Session[FixedVariables.SESSION_ADMIN_PROFILE] != null)
            {
                obj = (AdminProfile)HttpContext.Current.Session[FixedVariables.SESSION_ADMIN_PROFILE];
            }
            return obj;
        }

        public int ChangePswd(string EmailId, string Password, string NewPassword)
        {
            int result = 0;
            //Admin Authentication

            using (var context = new XpertWebEntities())
            {
                var objAdminUsr = (from a in context.tblAdminUsers where a.EmailId == EmailId && a.Password == Password select a).FirstOrDefault();
                if (objAdminUsr != null)
                {
                    objAdminUsr.Password = NewPassword;
                    result = context.SaveChanges();
                }
                else
                {
                    result = -1;
                }
            }
            return result;
        }

        

    }










    public class AdminProfile
    {
        AdminAuthontication objAdminAuth = new AdminAuthontication();
        tblAdminUser objAdminUsr = new tblAdminUser();

        public int AdminId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int? RoleID { get; set; }
        public string Contact { get; set; }
        public string JobTitle { get; set; }
        public string Image { get; set; }
        public int RoleId { get; set; }
        public bool RememberMe { get; set; }
        public bool IsExist { get; set; }
        public int Mid { get; set; }
        //public AdminProfile(string Email, string Password)
        //{
        //    tblAdminUser objAdminUsr = new tblAdminUser();
        //    objAdminUsr = objAdminAuth.AdminLogin(Email, Password);
        //    if (objAdminUsr!=null)
        //    {
        //        IsExist = true;
        //    }
        //}
        public AdminProfile()
        {

        }

        public AdminProfile(string _email, string _password)
        {
            //Admin Authentication
            objAdminUsr = objAdminAuth.AdminLogin(_email, _password);
            if (objAdminUsr != null)
            {
                this.IsExist = true;
                this.AdminId = objAdminUsr.AdminId;
                this.FirstName = objAdminUsr.FirstName;
                this.LastName = objAdminUsr.LastName;
                this.Password = objAdminUsr.Password;
                this.Email = objAdminUsr.EmailId;
                this.RoleID = objAdminUsr.RoleID;
              
            }
            else
                this.IsExist = false;

        }

    }


    #region System fixed veriable class

    public class FixedVariables
    {
        #region All the Session veriables defined here
        public const string SESSION_ADMIN_PROFILE = "AllAdminProfileDataInSession";
        #endregion




    }




    #endregion

}




