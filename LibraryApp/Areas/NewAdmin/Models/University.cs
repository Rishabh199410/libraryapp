﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryApp.Areas.NewAdmin.Models
{
    public class University
    {
        [Required(ErrorMessage = "Please enter university name")]
        public string UniversityName { get; set; }
        public string country { get; set; }
        public string shortName { get; set; }
        public string description { get; set; }
        public string Note { get; set; }
        public string Logo { get; set; }
        public DateTime? CreatedOn { get; set; }
        public bool? IsActive { get; set; }
    }
}