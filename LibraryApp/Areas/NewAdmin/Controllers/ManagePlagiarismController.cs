﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LibraryBILayer.Admin;
using PagedList;
using LibraryDataLayer;
using LibraryUtility;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

namespace LibraryApp.Areas.NewAdmin.Controllers
{
    public class ManagePlagiarismController : Controller
    {
        // GET: NewAdmin/Question
        public ActionResult Index()
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            int pageNo = 1;
            int pageSize = 100;
            var GetList = ManageLibrary.QuestionList((Int32)Enums.ServiceTypeID.PlagiarismChecker);
            ViewBag.data = GetList.ToPagedList(pageNo, pageSize);
            ViewBag.TotalCount = GetList.Count;
            ViewBag.pageSize = pageSize;
            ViewBag.IsDeleted = false;
            ViewBag.Page = pageNo;
            return View();
        }


        public ActionResult PlagiarismList(int page = 1)
        {
            int pageSize = 100;
            List<QuestionLib> objQuestion = new List<QuestionLib>();
            //  bool IsDeleted = Convert.ToBoolean(IsActive);
            objQuestion = ManageLibrary.QuestionList((Int32)Enums.ServiceTypeID.PlagiarismChecker);

            ViewData["TotalQuestionCount"] = objQuestion.Count;
            ViewBag.Page = page;
            ViewBag.pageSize = pageSize;
            //ViewBag.IsDeleted = IsActive;
            return Request.IsAjaxRequest()
                             ? (ActionResult)PartialView("PlagiarismList", objQuestion.ToPagedList(page, pageSize))
                             : PartialView("PlagiarismList", objQuestion.ToPagedList(page, pageSize));

        }
        public ActionResult ViewQuestion(int Id)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }

            var data = ManageLibrary.ViewPlagiarismByID(Id);

            return View(data);
        }

        [HttpPost]
        public ActionResult UpdateStatus(QuestionLib model)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }

            if (model.ChangeStatusID > 0)
            {
                model.StatusID = model.ChangeStatusID;
            }
            var data = ManageLibrary.UpdateQuestionID(model.ID, model.StatusID, model.EstimatedPrice);
            return RedirectToAction("Index");
        }

        public ActionResult DownloadFile(string FilePath)
        {
            FilePath = FilePath.Substring(2);
            char[] separator = new char[] { '/' };
            string[] filePart = FilePath.Split(separator);
            string path = AppDomain.CurrentDomain.BaseDirectory + FilePath;
            byte[] fileBytes = System.IO.File.ReadAllBytes(path);
            string fileName = filePart[1];
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);        
        }

        //public ActionResult AddPlagiarism(mstplagiarismprice obj)
        //{
        //    HttpCookie objadmprof = Request.Cookies["Admin"];
        //    if (objadmprof == null)
        //    {
        //        return RedirectToAction("Index", "Panel");
        //    }

        //    obj.CreatedBy = objadmprof["Email"].ToString();
        //    obj.CreatedOn = DateTime.Now;
        //    int result = ManagePlagiarism.AddorUpdatePlagiarism(obj);

        //    if (result == 1)
        //    {
        //        TempData["MessageType"] = "Success";
        //        TempData["CustomMessage"] = "Plagiarism is successfully added.";
        //    }
        //    else
        //    {
        //        TempData["MessageType"] = "Error";
        //        TempData["CustomMessage"] = "Something Went Wrong !";
        //    }
        //    ModelState.Clear();
        //    return RedirectToAction("BlogCatList", "BlogCat", new { area = "NewAdmin" });

        //}

        public ActionResult EditPlagiarism(int Id)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];

            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }

            //DropDownInitialize();
            mstplagiarismprice data = ManagePlagiarism.GetPlagiarism(Id);

            return View("~/Areas/NewAdmin/Views/Plagiarism/EditPlagiarism.cshtml", data);
        }

        [HttpPost]
        public ActionResult EditPlagiarism(mstplagiarismprice obj)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];

            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            //DropDownInitialize();
            // var data = ManageBlog.GetBogCat(Id);

            obj.UpdatedBy = 1; //objadmprof["Email"].ToString();
            obj.UpdatedOn = DateTime.Now;
            //string meGet1 = Regex.Replace(obj.BlogCatName, "<.*?>", String.Empty);
            //meGet1 = meGet1.Replace(" ", "-").ToLower().Substring(0, meGet1.Length > 150 ? 150 : meGet1.Length);

            int result = ManagePlagiarism.AddorUpdatePlagiarism(obj);

            if (result == 1)
            {
                TempData["MessageType"] = "Success";
                TempData["CustomMessage"] = "Plagiarism price is successfully updated.";
            }
            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "Something Went Wrong !";
            }
            ModelState.Clear();
            return RedirectToAction("PlagiarismPriceList", "ManagePlagiarism", new { @Areas = "NewAdmin" });

        }

        //public JsonResult FinalDeletePlagiarism(int Id)
        //{
        //    int result = 0;
        //    long AId = Convert.ToInt64(Id);
        //    // tblSolutionLibrary MngArticle = new tblSolutionLibrary();
        //    HttpCookie objadmprof = Request.Cookies["Admin"];
        //    result = 4;
        //    result = ManagePlagiarism.DeletePlagiarism(Id);

        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult PlagiarismPriceList()
        {
            //int pageNo = 1;
            //int pageSize = 10;
            List<mstplagiarismprice> GetList = ManagePlagiarism.GetPlagiarismList();
            //ViewBag.data = GetList.ToPagedList(pageNo, pageSize);
            //ViewBag.TotalCount = GetList.Count;
            //ViewBag.pageSize = pageSize;
            //ViewBag.IsDeleted = 1;
            //ViewBag.Page = pageNo;
            return View("~/Areas/NewAdmin/Views/Plagiarism/ListPlagiarism.cshtml", GetList);
        }

    }
}