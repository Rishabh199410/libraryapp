﻿using LibraryApp.Areas.NewAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LibraryDataLayer;
using System.IO;
using Ionic.Zip;
using System.Text.RegularExpressions;
using LibraryBILayer.Admin;
using PagedList;

namespace LibraryApp.Areas.NewAdmin.Controllers
{
    public class SolutionLibraryController : Controller
    {
        // GET: Admin/SolutionLibrary
        AdminAuthentication adminAuthentication = new AdminAuthentication();
        public ActionResult Index()
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            //ViewBag.Subject = GetSolutionSubject();
            
                //ViewBag.University = GetUniversity();
            
            return View();
        }
        public ActionResult AddSolution()
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }

            return View();
        }
        public static SelectList GetSolutionSubject()
        {
            SelectList ConList;
            using (var context = new XpertWebEntities())
            {
                var ObjMastersCat = (from c in context.tblSolutionSubjects
                                     select new 

                                     {
                                         SolSubID = c.SolSubID ?? Int32.MinValue,
                                         Subject = c.Subject
                                     }).ToList();
                ConList = new SelectList(ObjMastersCat, "SolSubID", "Subject");
                return ConList;
            }
        }
        public static SelectList GetUniversity(int UID)
        {
            SelectList ConList;
            using (var context = new XpertWebEntities())
            {
                var ObjMastersCat = (from c in context.tblUniversities
                                     join p in context.tblMainSubjects on c.UID equals p.UID
                                     where p.UID==UID
                                     select new
                                     {
                                         UID = c.UID,
                                         University = c.University
                                     }).ToList();
                ConList = new SelectList(ObjMastersCat, "UID", "University");
                return ConList;
            }
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult NewSolution(tblSolutionLibrary obj)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];

            int CheckStatus = Convert.ToInt32(Request.Form["CheckStatus"]);

            if(CheckStatus == 1)
            {
                obj.IsAnswer = true;
            }
            else
            {
                obj.IsAnswer = false;
            }
            int AnswerID = 0;
            int check = ManageLibrary.checkSolution(obj.Title);
            //ViewBag.Subject = GetSolutionSubject();
            //ViewBag.University = GetUniversity();
            //Guid guid = Guid.NewGuid();
            //string encoded = Convert.ToBase64String(guid.ToByteArray());
            //encoded = encoded
            //  .Replace("9/", "_")
            //  .Replace("+", "-");
            //string mainID = encoded.Substring(0, 7).ToUpper();
            string mainID = "SL" + DateTime.Now.ToString("yyyyMMdd") + "DO" + DateTime.Now.ToString("HHmmssfff");
           /*----GET ID----*/
           int getID = ManageLibrary.getSolID();

            if (check == 0)
            {
                //ViewBag.Subject = GetSolutionSubject();
                //ViewBag.University = GetUniversity();
                string path = "";
                string originalPath = System.Configuration.ConfigurationManager.AppSettings["SolutionPath"].ToString();
                string questionPath = System.Configuration.ConfigurationManager.AppSettings["QuestionPath"].ToString();
                string ZIppath = "";
                
                string subPath = "QS" + mainID + getID;
                if (Request.Files.Count > 0)
                {
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        HttpPostedFileBase PostedFile = Request.Files[i];

                        string FileName = Path.GetFileName(PostedFile.FileName);

                        string FirstQuesName = FileName.Substring(0, 2);
                        string extension = Path.GetExtension(FileName);

                        if (FirstQuesName == "QU")
                        {
                            bool exists = System.IO.Directory.Exists(Server.MapPath(questionPath + subPath));

                            if (!exists)
                                System.IO.Directory.CreateDirectory(Server.MapPath(questionPath + subPath));

                            path = questionPath + subPath + "/" + "S" + i + mainID + getID + extension;

                            tblQuestionFile objQues = new tblQuestionFile();
                            objQues.CreatedBy = objadmprof["Email"];
                            objQues.CreatedOn = DateTime.Now;
                            objQues.Question_File_ID = subPath;
                            objQues.Question_File_Name = path;
                            objQues.IsActive = true;

                            ManageLibrary.AddQuestionFile(objQues);
                        }
                        else
                        {
                            path = originalPath + "S" + i + mainID + getID + extension;
                        }

                        PostedFile.SaveAs(Server.MapPath(path));
                        //i++;
                    }

                    /*-----MAKE ZIP FILE------*/
                    string modifiedPath = System.Configuration.ConfigurationManager.AppSettings["SolutionModifiedPath"].ToString();
                    string Orgpath = Server.MapPath(modifiedPath);//Location for inside Test Folder  
                    string mYpath = Server.MapPath(originalPath);

                    string[] Filenames = Directory.GetFiles(mYpath, "*" + getID + "*", SearchOption.AllDirectories);
                    using (ZipFile zip = new ZipFile())
                    {
                        zip.AddFiles(Filenames, "S" + mainID + getID.ToString());//Zip file inside filename  
                        zip.Save(Server.MapPath(modifiedPath + "S" + mainID + getID.ToString() + ".zip"));//location and name for creating zip file  
                    }

                    /*----DELETE FILES----*/
                    foreach (string filePath in Filenames)
                    {
                        System.IO.File.Delete(filePath);
                    }

                    ZIppath = modifiedPath + "S" + mainID + getID + ".zip";


                    if (obj.IsAnswer == true)
                    {
                        /*-----ADD ANSWER LIBRARY----*/
                        var ansObj = new tblAnswerLibrary();
                        //ansObj.FileName = FileName;
                        ansObj.FilePath = ZIppath;
                        ansObj.IsActive = true;
                        ManageLibrary.AddAnswer(ansObj);
                    }

                    /*----UPDATE ID----*/
                    ManageLibrary.updateSolID();

                    //int SolutionID = ManageLibrary.GetTopSolutionLibraryID();
                    //Regex re = new Regex("[;\\\\/:*?\"<>|&']");
                    if (obj.IsAnswer == true)
                    {
                        AnswerID = ManageLibrary.GetAnswerLibraryID();
                    }
                    //string meGet = Regex.Replace(obj.Title, "<.*?>", String.Empty);
                    //meGet = meGet.Replace(",", "").Replace(" ", "-").Replace("---", "-");
                    //System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex("[^0-9a-zA-Z]+");
                    //meGet = rx.Replace(meGet, "-").ToLower().Substring(0, meGet.Length > 150 ? 150 : meGet.Length);

                    string meGet = Regex.Replace(obj.Title, "<.*?>", String.Empty);
                    //System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex("[^0-9a-zA-Z]+");
                    string my_String = Regex.Replace(meGet, @"[^0-9a-zA-Z]+", " ");
                    meGet = my_String.Replace(" ", "-").ToLower().Substring(0, my_String.Length > 150 ? 150 : my_String.Length);

                    string meGet1 = Regex.Replace(ManageLibrary.GetUniversityName(Convert.ToInt32(obj.UID)), "<.*?>", String.Empty);
                    //System.Text.RegularExpressions.Regex rx1 = new System.Text.RegularExpressions.Regex("[^0-9a-zA-Z]+");
                    string my_String1 = Regex.Replace(meGet1, @"[^0-9a-zA-Z]+", " ");
                    meGet1 = my_String1.Replace(" ", "-").ToLower().Substring(0, my_String1.Length > 150 ? 150 : my_String1.Length);
                    //string getUrl = .ToLower().Substring(0, meGet.Length > 120 ? 120 : meGet.Length);

                    //string mURLText = getUrl.Replace(":", "").Replace(".", "").Replace(",", "").Replace(";", "").Replace(".", "").Replace("(", "").Replace(")", "").Replace("?", "").Replace("%", "percent").Replace("[", "").Replace("]", "").Replace("+", "").Replace("&", "").Replace("?", "");
                    /*---ADD SOLUTION LIBRARY----*/
                    var data = new tblSolutionLibrary();
                    data.SolSubID = obj.SolSubID;
                    data.AnswerID = AnswerID;
                    data.Title = obj.Title;
                    data.CreatedOn = DateTime.Now;
                    data.CreatedBy = objadmprof["Email"];
                    data.SubjectName = obj.SubjectName;
                    data.Country = obj.Country;
                    data.SolContent = obj.SolContent;
                    data.Tags = obj.Tags;
                    data.Description = obj.Description;
                    data.Status = true;
                    data.Amount = obj.Amount;
                    if (obj.IsAnswer == true)
                    {
                        data.AnswerPreview = obj.AnswerPreview;
                    }
                    data.UrlText = meGet1 + "/" + meGet;
                    data.UID = obj.UID;
                    data.IsAnswer = obj.IsAnswer;
                    data.MSID = obj.MSID;
                    data.Question_File_ID = subPath;

                    if (ManageLibrary.GetURLID() == null)
                    {
                        data.UrlID = 1;
                    }
                    else
                    {
                        data.UrlID = ManageLibrary.GetMaxURLID() + 1;
                    }

                    int k = ManageLibrary.AddSolution(data);

                    var QuestionData = ManageLibrary.GetQuestionFiles(subPath);

                    foreach (var m in QuestionData)
                    {
                        int SID = Convert.ToInt32(ManageLibrary.CheckIfSolFile(m.Question_File_ID).Sol_ID);
                        ManageLibrary.UpdateSolutionID(m.ID, SID);
                    }
                    if (k == 1)
                    {
                        TempData["MessageType"] = "Success";
                        TempData["CustomMessage"] = "Solution Library is successfully added.";
                    }
                    ModelState.Clear();
                }
                else
                {
                    string meGet = Regex.Replace(obj.Title, "<.*?>", String.Empty);
                    //System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex("[^0-9a-zA-Z]+");
                    string my_String = Regex.Replace(meGet, @"[^0-9a-zA-Z]+", " ");
                    meGet = my_String.Replace(" ", "-").ToLower().Substring(0, my_String.Length > 150 ? 150 : my_String.Length);

                    string meGet1 = Regex.Replace(ManageLibrary.GetUniversityName(Convert.ToInt32(obj.UID)), "<.*?>", String.Empty);
                    //System.Text.RegularExpressions.Regex rx1 = new System.Text.RegularExpressions.Regex("[^0-9a-zA-Z]+");
                    string my_String1 = Regex.Replace(meGet1, @"[^0-9a-zA-Z]+", " ");
                    meGet1 = my_String1.Replace(" ", "-").ToLower().Substring(0, my_String1.Length > 150 ? 150 : my_String1.Length);
                    //string getUrl = .ToLower().Substring(0, meGet.Length > 120 ? 120 : meGet.Length);

                    var data = new tblSolutionLibrary();
                    data.SolSubID = obj.SolSubID;
                    data.AnswerID = AnswerID;
                    data.Title = obj.Title;
                    data.CreatedOn = DateTime.Now;
                    data.CreatedBy = objadmprof["Email"];
                    data.SubjectName = obj.SubjectName;
                    data.Country = obj.Country;
                    data.SolContent = obj.SolContent;
                    data.Tags = obj.Tags;
                    data.Description = obj.Description;
                    data.Status = true;
                    data.Amount = obj.Amount;
                    if (obj.IsAnswer == true)
                    {
                        data.AnswerPreview = obj.AnswerPreview;
                    }
                    data.UrlText = meGet1 + "/" + meGet;
                    data.UID = obj.UID;
                    data.IsAnswer = obj.IsAnswer;
                    data.MSID = obj.MSID;
                    data.Question_File_ID = subPath;

                    if (ManageLibrary.GetURLID() == null)
                    {
                        data.UrlID = 1;
                    }
                    else
                    {
                        data.UrlID = ManageLibrary.GetMaxURLID() + 1;
                    }

                    int k = ManageLibrary.AddSolution(data);

                    var QuestionData = ManageLibrary.GetQuestionFiles(subPath);

                    foreach (var m in QuestionData)
                    {
                        int SID = Convert.ToInt32(ManageLibrary.CheckIfSolFile(m.Question_File_ID).Sol_ID);
                        ManageLibrary.UpdateSolutionID(m.ID, SID);
                    }
                    if (k == 1)
                    {
                        TempData["MessageType"] = "Success";
                        TempData["CustomMessage"] = "Solution Library is successfully added.";
                    }
                    ModelState.Clear();
                }
            }
            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "Solution Library is already existed";
                ModelState.Clear();
            }

            return RedirectToAction("AddSolution", "SolutionLibrary", new { Area = "NewAdmin" });
        }
        //[ValidateInput(false)]
        [HttpPost]
        public ActionResult Index(tblSolutionLibrary obj)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];

            int check = ManageLibrary.checkSolution(obj.Title);
            //ViewBag.Subject = GetSolutionSubject();
            //ViewBag.University = GetUniversity();
            string mainID = "SL" + DateTime.Now.ToString("yyyyMMdd") + "DO" + DateTime.Now.ToString("HHmmssfff");
            /*----GET ID----*/
            int getID = ManageLibrary.getSolID();

            if (check == 0)
            {
                //ViewBag.Subject = GetSolutionSubject();
                //ViewBag.University = GetUniversity();
                string path = "";
                string originalPath = System.Configuration.ConfigurationManager.AppSettings["SolutionPath"].ToString();
                string questionPath = System.Configuration.ConfigurationManager.AppSettings["QuestionPath"].ToString();
                string ZIppath = "";
                string subPath = "QS" + mainID + getID;
                if (Request.Files.Count > 0)
                {
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        HttpPostedFileBase PostedFile = Request.Files[i];

                        string FileName = Path.GetFileName(PostedFile.FileName);

                        string FirstQuesName = FileName.Substring(0, 2);
                        string extension = Path.GetExtension(FileName);

                        if (FirstQuesName == "QU")
                        {                            
                            bool exists = System.IO.Directory.Exists(Server.MapPath(questionPath+subPath));

                            if (!exists)
                                System.IO.Directory.CreateDirectory(Server.MapPath(questionPath+subPath));

                            path = questionPath + subPath + "/" + "S" + i + mainID + getID + extension;

                            tblQuestionFile objQues = new tblQuestionFile();
                            objQues.CreatedBy = objadmprof["Email"];
                            objQues.CreatedOn = DateTime.Now;
                            objQues.Question_File_ID = subPath;
                            objQues.Question_File_Name = path;
                            objQues.IsActive = true;
                            //objQues.Sol_ID = getID;

                            ManageLibrary.AddQuestionFile(objQues);
                        }
                        else
                        {
                            path = originalPath + "S" + i + mainID + getID + extension;
                        }

                        PostedFile.SaveAs(Server.MapPath(path));
                        //i++;
                    }

                    /*-----MAKE ZIP FILE------*/
                    string modifiedPath = System.Configuration.ConfigurationManager.AppSettings["SolutionModifiedPath"].ToString();
                    string Orgpath = Server.MapPath(modifiedPath);//Location for inside Test Folder  
                    string mYpath = Server.MapPath(originalPath);

                    string[] Filenames = Directory.GetFiles(mYpath, "*" + getID + "*", SearchOption.AllDirectories);
                    using (ZipFile zip = new ZipFile())
                    {
                        zip.AddFiles(Filenames, "S" + mainID + getID.ToString());//Zip file inside filename  
                        zip.Save(Server.MapPath(modifiedPath + "S" + mainID + getID.ToString() + ".zip"));//location and name for creating zip file  
                    }

                    /*----DELETE FILES----*/
                    foreach (string filePath in Filenames)
                    {
                        System.IO.File.Delete(filePath);
                    }

                    ZIppath = modifiedPath + "S" + mainID + getID + ".zip";

                    /*-----ADD ANSWER LIBRARY----*/
                    var ansObj = new tblAnswerLibrary();
                    //ansObj.FileName = FileName;
                    ansObj.FilePath = ZIppath;
                    ansObj.IsActive = true;
                    ManageLibrary.AddAnswer(ansObj);

                    /*----UPDATE ID----*/
                    ManageLibrary.updateSolID();

                    //int SolutionID = ManageLibrary.GetTopSolutionLibraryID();
                    //Regex re = new Regex("[;\\\\/:*?\"<>|&']");
                    int AnswerID = ManageLibrary.GetAnswerLibraryID();
                    //int GetMaxID = ManageLibrary.GetMaxURLID();
                    string meGet = Regex.Replace(obj.Title, "<.*?>", String.Empty);
                    System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex("[^0-9a-zA-Z]+");
                    meGet = meGet.Replace(",", "");
                    meGet = rx.Replace(meGet, "-").ToLower().Substring(0, meGet.Length > 150 ? 150 : meGet.Length);
                    //string getUrl = .ToLower().Substring(0, meGet.Length > 120 ? 120 : meGet.Length);
                   

                    string meGet1 = Regex.Replace(ManageLibrary.GetUniversityName(Convert.ToInt32(obj.UID)), "<.*?>", String.Empty);
                    System.Text.RegularExpressions.Regex rx1 = new System.Text.RegularExpressions.Regex("[^0-9a-zA-Z]+");
                    meGet1 = meGet1.Replace(",", "");
                    meGet1 = rx1.Replace(meGet1, "-").ToLower().Substring(0, meGet1.Length > 150 ? 150 : meGet1.Length);

                    //string mURLText = getUrl.Replace(":", "").Replace(".", "").Replace(",", "").Replace(";", "").Replace(".", "").Replace("(", "").Replace(")", "").Replace("?", "").Replace("%", "percent").Replace("[", "").Replace("]", "").Replace("+", "").Replace("&", "").Replace("?", "");
                    /*---ADD SOLUTION LIBRARY----*/
                    var data = new tblSolutionLibrary();
                    data.SolSubID = obj.SolSubID;
                    data.AnswerID = AnswerID;
                    data.Title = obj.Title;
                    data.CreatedOn = DateTime.Now;
                    data.CreatedBy = objadmprof["Email"];
                    data.SolContent = obj.SolContent;
                    data.Status = true;
                    data.Tags = obj.Tags;
                    data.Description = obj.Description;
                    data.Amount = obj.Amount;
                    data.AnswerPreview = obj.AnswerPreview;
                    data.UrlText = meGet1 + "/" + meGet;
                    data.UID = obj.UID;
                    data.MSID = obj.MSID;
                    data.Question_File_ID = subPath;

                    if (ManageLibrary.GetURLID() == null)
                    {
                        data.UrlID = 1;
                    }
                    else
                    {
                        data.UrlID = ManageLibrary.GetMaxURLID() + 1;
                    }

                    int k = ManageLibrary.AddSolution(data);

                    var QuestionData = ManageLibrary.GetQuestionFiles(subPath);

                    foreach(var m in QuestionData)
                    {
                        int SID = Convert.ToInt32(ManageLibrary.CheckIfSolFile(m.Question_File_ID).Sol_ID);
                        ManageLibrary.UpdateSolutionID(m.ID, SID);
                    }

                    if (k == 1)
                    {
                        TempData["MessageType"] = "Success";
                        TempData["CustomMessage"] = "Solution Library is successfully added.";
                    }
                    ModelState.Clear();
                }
                else
                {
                    TempData["MessageType"] = "Error";
                    TempData["CustomMessage"] = "Please Upload Both Answer & Question File";
                }
            }
            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "Solution Library is already existed";
                ModelState.Clear();
            }

            return RedirectToAction("Index", "SolutionLibrary", new { Area = "NewAdmin" });
        }
        [HttpGet]
        public ActionResult ViewSolution()
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }

            int pageNo = 1;
            int pageSize = 50;
            var GetList = ManageLibrary.BindSolutionLibraryStatus0(1);
            ViewBag.data = GetList.ToPagedList(pageNo, pageSize);
            ViewBag.TotalCount = GetList.Count;
            ViewBag.pageSize = pageSize;
            ViewBag.IsDeleted = 1;
            ViewBag.Page = pageNo;
            return View();
        }
        [HttpGet]
        public ActionResult ViewSolutionList(bool IsActive)
        {
            int Status = 1;
            if (IsActive == true)
            {
                Status = 0;
            }


            ViewBag.Subject = ManageLibrary.BindSolutionLibraryStatus0(Status);

            return Json(ViewBag.Subject, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public ActionResult EditSolutionLibrary(int id)
        {
            SolutionLibr objSolution = new SolutionLibr();

            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            ViewBag.Subject = GetSolutionSubject();

            var CheckIfAns = ManageLibrary.CheckIfAnswer(id);
            if (CheckIfAns == true)
            {
                //ViewBag.University = GetUniversity();
                var list = ManageLibrary.BindEditSolutionLibrary(id);
                objSolution.UrlText = list[0].UrlText;
                objSolution.Sol_ID = list[0].Sol_ID;
                objSolution.AnswerID = list[0].AnswerID;
                objSolution.Amount = list[0].Amount;
                objSolution.CheckStatus = list[0].IsAnswer == true ? 1 : 0;
                objSolution.SolContent = list[0].SolContent;
                objSolution.SolSubID = list[0].SolSubID;
                objSolution.Title = list[0].Title;
                objSolution.AnswerfilePath = list[0].AnswerfilePath;
                objSolution.AnswerPreview = list[0].AnswerPreview;
                objSolution.UID = list[0].UID;
                objSolution.MSID = list[0].MSID;
                objSolution.Question_File_ID = list[0].Question_File_ID;
                objSolution.Tags = list[0].Tags;
                objSolution.Description = list[0].Description;
                objSolution.country = list[0].country;
                objSolution.SubjectName = list[0].SubjectName;
                //objSolution.MSID = list[0].MSID;

                return View(objSolution);
            }
            else
            {
                var list = ManageLibrary.BindEditSolutionLibraryWithoutAnswer(id);
                objSolution.UrlText = list[0].UrlText;
                objSolution.Sol_ID = list[0].Sol_ID;
                objSolution.AnswerID = list[0].AnswerID;
                objSolution.Amount = list[0].Amount;
                objSolution.SolContent = list[0].SolContent;
                objSolution.SolSubID = list[0].SolSubID;
                objSolution.CheckStatus = list[0].IsAnswer == true ? 1 : 0;
                objSolution.Title = list[0].Title;
                objSolution.UID = list[0].UID;
                objSolution.MSID = list[0].MSID;
                objSolution.Question_File_ID = list[0].Question_File_ID;
                objSolution.Tags = list[0].Tags;
                objSolution.Description = list[0].Description;
                objSolution.country = list[0].country;
                objSolution.SubjectName = list[0].SubjectName;
                //objSolution.MSID = list[0].MSID;

                return View(objSolution);
            }
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult EditSolutionLibrary(SolutionLibr obj)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (obj.CheckStatus == 1)
            {
                obj.IsAnswer = true;
            }
            else
            {
                obj.IsAnswer = false;
            }
            int AnswerID = 0;
            //ManageLibrary.updateID();
            ViewBag.Subject = GetSolutionSubject();
            //ViewBag.University = GetUniversity();
            string path = "";
            string FileName = "";
            string ZIppath = "";
            string mainID = "SL" + DateTime.Now.ToString("yyyyMMdd") + "DO" + DateTime.Now.ToString("HHmmssfff");
            /*----GET ID----*/
            int getID = ManageLibrary.getSolID();
            string originalPath = System.Configuration.ConfigurationManager.AppSettings["SolutionPath"].ToString();
            string questionPath = System.Configuration.ConfigurationManager.AppSettings["QuestionPath"].ToString();
            string subPath = "QS" + mainID + getID;

            /*----UPDATE QUESTION FILE----*/

            if (Request.Files.Count > 0)
            {
                //int i = 1;
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase PostedFile = Request.Files[i];

                    string FirstQuesName = PostedFile.FileName.Substring(0, 2);

                    FileName = Path.GetFileName(PostedFile.FileName);
                    string extension = Path.GetExtension(FileName);

                    if (FirstQuesName == "QU")
                    {
                        //var QuestionFileCheck = ManageLibrary.CheckIfQuestionFile(obj.Question_File_ID);

                        //if (QuestionFileCheck != null)
                        //{
                        //    ManageLibrary.UpdateQuestionFiles(obj.Question_File_ID, objadmprof["Email"]);
                        //}

                        bool exists = System.IO.Directory.Exists(Server.MapPath(questionPath + subPath));

                        if (!exists)
                            System.IO.Directory.CreateDirectory(Server.MapPath(questionPath + subPath));

                        path = questionPath + subPath + "/" + "S" + i + mainID + getID + extension;

                        tblQuestionFile objQues = new tblQuestionFile();
                        objQues.CreatedBy = objadmprof["Email"];
                        objQues.CreatedOn = DateTime.Now;
                        objQues.Question_File_ID = subPath;
                        objQues.Question_File_Name = path;
                        objQues.IsActive = true;
                        objQues.Sol_ID = obj.ID;
                        ManageLibrary.AddQuestionFile(objQues);
                    }
                    else
                    {
                        path = originalPath + "S" + i + mainID + getID + extension;
                    }

                    PostedFile.SaveAs(Server.MapPath(path));
                    //i++;
                }

                /*-----MAKE ZIP FILE------*/
                string modifiedPath = System.Configuration.ConfigurationManager.AppSettings["SolutionModifiedPath"].ToString();
                string Orgpath = Server.MapPath(modifiedPath);//Location for inside Test Folder  
                string mYpath = Server.MapPath(originalPath);

                string[] Filenames = Directory.GetFiles(mYpath, "*" + getID + "*", SearchOption.AllDirectories);
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFiles(Filenames, "S" + mainID + getID.ToString());//Zip file inside filename  
                    zip.Save(Server.MapPath(modifiedPath + "S" + mainID + getID.ToString() + ".zip"));//location and name for creating zip file  
                }

                /*----DELETE FILES----*/
                foreach (string filePath in Filenames)
                {
                    System.IO.File.Delete(filePath);
                }

                ZIppath = modifiedPath + "S" + mainID + getID + ".zip";
                if (obj.IsAnswer == true)
                {
                    /*-----ADD ANSWER LIBRARY----*/
                    var ansObj = new tblAnswerLibrary();
                    //ansObj.FileName = FileName;
                    ansObj.FilePath = ZIppath;
                    ansObj.IsActive = true;

                    ManageLibrary.AddAnswer(ansObj);
                    AnswerID = ManageLibrary.GetAnswerLibraryID();
                }
            }
            else
            {
                if (obj.IsAnswer == true)
                {
                    AnswerID = obj.AnswerID ?? Int32.MinValue;
                }
            }
            /*----UPDATE ID----*/
            ManageLibrary.updateSolID();

            /*---ADD SOLUTION LIBRARY----*/
            var data = new tblSolutionLibrary();
            data.Sol_ID = obj.Sol_ID ?? Int32.MinValue;
            data.SolSubID = obj.SolSubID;
            data.AnswerID = AnswerID;
            data.Title = obj.Title;
            data.IsAnswer = obj.IsAnswer;
            data.Tags = obj.Tags;
            data.Description = obj.Description;
            data.UpdatedOn = DateTime.Now;
            data.SubjectName = obj.SubjectName;
            data.Country = obj.country;
            data.UpdatedBy = objadmprof["Email"];
            data.SolContent = obj.SolContent;
            data.Amount = obj.Amount;

            //string meGet = Regex.Replace(obj.Title, "<.*?>", String.Empty);
            //System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex("[^0-9a-zA-Z]+");
            //meGet = meGet.Replace(",", "").Replace(" ", "-").Replace("---","-");
            //meGet = rx.Replace(meGet, "-").ToLower().Substring(0, meGet.Length > 150 ? 150 : meGet.Length);
            string meGet = Regex.Replace(obj.Title, "<.*?>", String.Empty);
            //System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex("[^0-9a-zA-Z]+");
            string my_String = Regex.Replace(meGet, @"[^0-9a-zA-Z]+", " ");
            meGet = my_String.Replace(" ", "-").ToLower().Substring(0, my_String.Length > 150 ? 150 : my_String.Length);

            string meGet1 = Regex.Replace(ManageLibrary.GetUniversityName(Convert.ToInt32(obj.UID)), "<.*?>", String.Empty);
            //System.Text.RegularExpressions.Regex rx1 = new System.Text.RegularExpressions.Regex("[^0-9a-zA-Z]+");
            string my_String1 = Regex.Replace(meGet1, @"[^0-9a-zA-Z]+", " ");
            meGet1 = my_String1.Replace(" ", "-").ToLower().Substring(0, my_String1.Length > 150 ? 150 : my_String1.Length);

            // data.UrlText = obj.UrlText;
            data.AnswerPreview = obj.AnswerPreview;
            data.Question_File_ID = subPath;
            data.UrlText = meGet1 + "/" + meGet;
            data.UID = obj.UID;
            data.MSID = obj.MSID;
            int result = ManageLibrary.UpdateSolution(data);

            if (result == 1)
            {
                TempData["MessageType"] = "Success";
                TempData["CustomMessage"] = "Solution Library is successfully updated.";
            }
            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "Some error while processing your request.";
            }

            ModelState.Clear();
            return RedirectToAction("ViewSolution", "SolutionLibrary", new { @area = "NewAdmin" });


            //return View();
        }
        public JsonResult DeleteSolutionLibrary(int Id, bool status)
        {
            int result = 0;
             long AId = Convert.ToInt64(Id);
            // tblSolutionLibrary MngArticle = new tblSolutionLibrary();
            HttpCookie objadmprof = Request.Cookies["Admin"];
            result = ManageLibrary.DeleteSolutionLibrary(Id, status, objadmprof["Email"]);
           
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FinalDeleteSolution(int Id, bool status)
        {
            int result = 0;
            long AId = Convert.ToInt64(Id);
            // tblSolutionLibrary MngArticle = new tblSolutionLibrary();
            HttpCookie objadmprof = Request.Cookies["Admin"];
            result = 4;
            result = ManageLibrary.DeleteSolution(Id, status, objadmprof["Email"]);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteQuestionSolution(int Id)
        {
            int result = 0;

            result = ManageLibrary.DeleteQuestionSolutionLib(Id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult _ListOfSolutionLibrary(int IsActive, int page = 1, string SearchStr = "")
        {
            if (SearchStr != "")
            {
                int pageSize = 50;
                List<SolutionLibr> objFreeSamples = new List<SolutionLibr>();
                //  bool IsDeleted = Convert.ToBoolean(IsActive);
                SolutionLibr MngArticle = new SolutionLibr();
                objFreeSamples = ManageLibrary.BindSolutionLibraryStatusSearch(SearchStr);

                ViewData["TotalArticleCount"] = objFreeSamples.Count;
                ViewBag.Page = page;
                ViewBag.pageSize = pageSize;
                ViewBag.IsDeleted = IsActive;
                return Request.IsAjaxRequest()
                                 ? (ActionResult)PartialView("_ListOfSolutionLibrary", objFreeSamples.ToPagedList(page, pageSize))
                                 : PartialView("_ListOfSolutionLibrary", objFreeSamples.ToPagedList(page, pageSize));
            }
            else
            {
                int pageSize = 50;
                List<SolutionLibr> objFreeSamples = new List<SolutionLibr>();
                //  bool IsDeleted = Convert.ToBoolean(IsActive);
                SolutionLibr MngArticle = new SolutionLibr();
                objFreeSamples = ManageLibrary.BindSolutionLibraryStatus0(IsActive);

                ViewData["TotalArticleCount"] = objFreeSamples.Count;
                ViewBag.Page = page;
                ViewBag.pageSize = pageSize;
                ViewBag.IsDeleted = IsActive;
                return Request.IsAjaxRequest()
                                 ? (ActionResult)PartialView("_ListOfSolutionLibrary", objFreeSamples.ToPagedList(page, pageSize))
                                 : PartialView("_ListOfSolutionLibrary", objFreeSamples.ToPagedList(page, pageSize));
            }
        }
    }
}