﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LibraryBILayer.Admin;
using PagedList;
using LibraryDataLayer;
using LibraryUtility;

namespace LibraryApp.Areas.NewAdmin.Controllers
{
    public class QuestionController : Controller
    {
        // GET: NewAdmin/Question
        public ActionResult Index()
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            int pageNo = 1;
            int pageSize = 100;
            var GetList = ManageLibrary.QuestionList((Int32)Enums.ServiceTypeID.Question24);
            ViewBag.data = GetList.ToPagedList(pageNo, pageSize);
            ViewBag.TotalCount = GetList.Count;
            ViewBag.pageSize = pageSize;
            ViewBag.IsDeleted = false;
            ViewBag.Page = pageNo;
            return View();
        }
        public ActionResult _Question(int page = 1)
        {
            int pageSize = 100;
            List<QuestionLib> objQuestion = new List<QuestionLib>();
            //  bool IsDeleted = Convert.ToBoolean(IsActive);
            objQuestion = ManageLibrary.QuestionList((Int32)Enums.ServiceTypeID.Question24);

            ViewData["TotalQuestionCount"] = objQuestion.Count;
            ViewBag.Page = page;
            ViewBag.pageSize = pageSize;
            //ViewBag.IsDeleted = IsActive;
            return Request.IsAjaxRequest()
                             ? (ActionResult)PartialView("_Question", objQuestion.ToPagedList(page, pageSize))
                             : PartialView("_Question", objQuestion.ToPagedList(page, pageSize));

        }
        public ActionResult ViewQuestion(int Id)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }

            var data = ManageLibrary.QuestionData(Id);

            return View(data);
        }

        public ActionResult UpdateStatus(QuestionLib model)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }

            if (model.ChangeStatusID > 0)
            {
                model.StatusID = model.ChangeStatusID;
            }
            var data = ManageLibrary.UpdateQuestionID(model.ID, model.StatusID, model.EstimatedPrice);
            return RedirectToAction("Index");
        }
    }
}