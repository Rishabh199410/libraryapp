﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LibraryDataLayer;
using LibraryApp.Areas.NewAdmin.Models;
using LibraryBILayer.Admin;

namespace LibraryApp.Areas.NewAdmin.Controllers
{
    public class LoginController : Controller
    {
        AdminAuthentication adminAuth = new AdminAuthentication();
        // GET: NewAdmin/Login
        public ActionResult AddLogin(string returl = "")
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];

            if (objadmprof == null)
            { 
                ViewBag.returl = returl;
                AdminProfile objAdminProfile = new AdminProfile();
                string Email = Request.Cookies["AdminEmail"] != null ? Convert.ToString(Request.Cookies["AdminEmail"].Value) : string.Empty;
                string Password = Request.Cookies["AdminPassword"] != null ? Convert.ToString(Request.Cookies["AdminPassword"].Value) : string.Empty;
                if (!String.IsNullOrEmpty(Email) && !String.IsNullOrEmpty(Password))
                {
                    //string pass = Helper.Decrypt("Q91K3Tu66SysJjfbbB0DgQ==");
                    objAdminProfile.Email = Email;
                    objAdminProfile.Password = Password;
                    objAdminProfile.RememberMe = true;
                }
                //string pass1 = Helper.Decrypt("Q91K3Tu66SysJjfbbB0DgQ==");
                return View(objAdminProfile);
            }
            else
            {
                return RedirectToAction("Index", "Panel", new { area = "NewAdmin" });
            }
        }
        [HttpPost]
        public ActionResult GetLogin(AdminAuthentication objAdmAuth, string returl = "")
        {
            ActionResult actionresult = RedirectToAction("Index", "Panel", new { area = "NewAdmin" });
            AdminAuthentication objAdminAuth = new AdminAuthentication();
            string strUserEmail, strpassword;
            //string pass1 = Helper.Decrypt("Q91K3Tu66SysJjfbbB0DgQ==");
            bool RememberMe;

            if (objAdmAuth != null)
            {
                strUserEmail = objAdmAuth.Email.ToString();
                strpassword = objAdmAuth.Password.ToString();
                RememberMe = objAdmAuth.RememberMe;
                //string pass = Helper.Decrypt("Q91K3Tu66SysJjfbbB0DgQ==");
                AdminProfile objAdminProfile = new AdminProfile(strUserEmail, strpassword);
                if (objAdminProfile.IsExist)
                {
                    //objAdmAuth.SetAdminSessionProfile(objAdminProfile);
                    HttpCookie cookie = new HttpCookie("Admin");
                    cookie["Email"] = objAdminProfile.Email;
                    cookie["AdminId"] = objAdminProfile.AdminId.ToString();
                    cookie["RoleID"] = objAdminProfile.RoleID.ToString();
                    Response.Cookies.Add(cookie);

                    if (RememberMe)
                    {
                        Response.Cookies["AdminEmail"].Value = objAdminProfile.Email;
                        Response.Cookies["AdminEmail"].Expires = DateTime.Now.AddDays(30);
                        Response.Cookies["AdminPassword"].Value = objAdminProfile.Password;
                        Response.Cookies["AdminPassword"].Expires = DateTime.Now.AddDays(30);
                    }
                    else
                    {
                        // Request.Cookies.Remove("AdminEmail");
                        // Request.Cookies.Remove("AdminPassword");

                        Response.Cookies["AdminEmail"].Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies["AdminPassword"].Expires = DateTime.Now.AddDays(-1);
                    }
                    if ((!string.IsNullOrEmpty(returl)))
                    {
                        actionresult = Redirect(returl);
                    }
                    else
                    {
                        actionresult = RedirectToAction("Index", "Panel", new { area = "NewAdmin" });
                    }
                }
                else
                {
                    TempData["Message"] = "Invalid login credentials, please try again";
                }
            }
            else
            {
                TempData["Message"] = "Invalid login credentials, please try again";
            }
            return actionresult;
        }
        [HttpGet]
        public ActionResult Logout()
        {
            if (Request.Cookies["Admin"] != null)
            {
                Response.Cookies["Admin"].Expires = DateTime.Now.AddDays(-1);

            }
            return Redirect("~/adm/login");
        }
        
    }
}