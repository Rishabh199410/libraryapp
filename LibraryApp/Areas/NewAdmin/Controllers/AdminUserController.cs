﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LibraryDataLayer;
using LibraryBILayer.Admin;
using System.Text.RegularExpressions;
using System.IO;
using PagedList;
using LibraryApp.Models;
using LibraryBILayer.Model;

namespace LibraryApp.Areas.NewAdmin.Controllers
{
    public class AdminUserController : Controller
    {
        public ActionResult Index()
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            tblAdminUser model = new tblAdminUser();
            return View(model);
        }

        public ActionResult AdminUserList()
        {
            int pageNo = 1;
            int pageSize = 100;
            var GetList = ManageAdminUser.GetAdminUserList();
            ViewBag.data = GetList.ToPagedList(pageNo, pageSize);
            ViewBag.TotalCount = GetList.Count;
            ViewBag.pageSize = pageSize;
            ViewBag.IsDeleted = 1;
            ViewBag.Page = pageNo;
            return View();
        }

        public ActionResult EditAdminUser(int Id)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];

            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }

            //DropDownInitialize();
            tblAdminUser data = ManageAdminUser.GetAdminUser(Id);

            return View("~/Areas/NewAdmin/Views/AdminUser/EditAdminUser.cshtml", data);
        }

        [HttpPost]
        public ActionResult EditAdminUser(tblAdminUser obj)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];

            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
         

            obj.ModifiedBy = 1; //objadmprof["Email"].ToString();
            obj.ModifiedOn = DateTime.Now;
          
            int result = ManageAdminUser.AddorUpdateAdminUser(obj);

            if (result == 1)
            {
                TempData["MessageType"] = "Success";
                TempData["CustomMessage"] = "User is successfully Updated.";
            }
            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "Something Went Wrong !";
            }
            ModelState.Clear();
            return RedirectToAction("AdminUserList", "AdminUser", new { @Areas = "NewAdmin" });

        }

        public ActionResult AdminUserLists()
        {
            List<AdminUserModel> GetList = ManageAdminUser.GetAdminUserList();
            return View("~/Areas/NewAdmin/Views/AdminUser/AdminUserList.cshtml", GetList);
        }

        [HttpPost]
        public ActionResult AddAdminUser(tblAdminUser obj)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }

            obj.CreatedBy = 1;//objadmprof["Email"].ToString();
            obj.CreatedOn = DateTime.Now;
            int result = ManageAdminUser.AddorUpdateAdminUser(obj);

            if (result == 1)
            {
                TempData["MessageType"] = "Success";
                TempData["CustomMessage"] = "User is successfully added.";
            }
            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "Something Went Wrong !";
            }
            ModelState.Clear();
            return RedirectToAction("AdminUserList", "AdminUser", new { area = "NewAdmin" });

        }
        
        public ActionResult DeleteAdminUser(int Id, int status)
        {
            int result = 0;
            HttpCookie objadmprof = Request.Cookies["Admin"];

            result = ManageAdminUser.DeleteAdminUser(Id, status);//, objadmprof["Email"].ToString());
            //return Json(result);
            return RedirectToAction("AdminUserList", "AdminUser", new { @Areas = "NewAdmin" });
            //return RedirectToAction("UserNavigationList","UserNavigation");
        }

        public ActionResult FinalDeleteAdminUser(int Id)
        {
            int result = 0;
            long AId = Convert.ToInt64(Id);          
            HttpCookie objadmprof = Request.Cookies["Admin"];
            result = 4;
            result = ManageAdminUser.FinalDeleteAdminUser(Id);

            return RedirectToAction("AdminUserList", "AdminUser", new { @Areas = "NewAdmin" });
        }

        public ActionResult _ListOfAdminUser(int IsActive, int page = 1)
        {
            int pageSize = 100;
            List<tblAdminUser> objFreeSamples = new List<tblAdminUser>();
            //  bool IsDeleted = Convert.ToBoolean(IsActive);
            // SolutionLibr MngArticle = new SolutionLibr();
            objFreeSamples = ManageAdminUser.GetAdminUserList(IsActive);

            ViewData["TotalAdminUserCount"] = objFreeSamples.Count;
            ViewBag.Page = page;
            ViewBag.pageSize = pageSize;
            ViewBag.IsDeleted = IsActive;
            return Request.IsAjaxRequest()
                             ? (ActionResult)PartialView("_ListOfAdminUser", objFreeSamples.ToPagedList(page, pageSize))
                             : PartialView("_ListOfAdminUser", objFreeSamples.ToPagedList(page, pageSize));
        }
    }
}