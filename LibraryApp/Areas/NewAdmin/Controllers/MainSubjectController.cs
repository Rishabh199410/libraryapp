﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LibraryApp.Areas.NewAdmin.Models;
using LibraryBILayer.Admin;
using LibraryDataLayer;
using PagedList;

namespace LibraryApp.Areas.NewAdmin.Controllers
{
    public class MainSubjectController : Controller
    {
        // GET: NewAdmin/MainSubject
        AdminAuthentication adminAuthentication = new AdminAuthentication();
        public ActionResult AddMainSubject()
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            ViewBag.Subject = GetSubjectCategory();
            ViewBag.University = GetUniversity();
            return View();
        }
        public static SelectList GetSubjectCategory()
        {
            SelectList ConList;
            using (var context = new XpertWebEntities())
            {
                var ObjMastersCat = (from c in context.tblSolutionSubjects
                                     orderby c.id descending
                                     select new
                                     {
                                         SolSubID = c.SolSubID ?? Int32.MinValue,
                                         Subject = c.Subject
                                     }).ToList();
                ConList = new SelectList(ObjMastersCat, "SolSubID", "Subject");
                return ConList;
            }
        }
        public static SelectList GetUniversity()
        {
            SelectList ConList;
            using (var context = new XpertWebEntities())
            {
                var ObjMastersCat = (from c in context.tblUniversities
                                     select new
                                     {
                                         UID = c.UID,
                                         University = c.University
                                     }).ToList();
                ConList = new SelectList(ObjMastersCat, "UID", "University");
                return ConList;
            }
        }
        [HttpPost]
        public ActionResult AddMainSubject(SolutionLibr obj)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];

            ViewBag.Subject = GetSubjectCategory();
            ViewBag.University = GetUniversity();
            tblMainSubject mainSub = new tblMainSubject();
            mainSub.CreatedBy = objadmprof["Email"];
            mainSub.CreatedOn = DateTime.Now;
            mainSub.SubjectName = obj.Subject;
            mainSub.Country = obj.country;
            mainSub.IsActive = false;
            mainSub.SolSubID = obj.SolSubID;
            mainSub.SubjectCode = obj.SubjectCode;
            mainSub.UID = obj.UID;

            int result = ManageMainSubject.AddOrUpdateMainSubject(mainSub);

            if (result == 1)
            {
                TempData["MessageType"] = "Success";
                TempData["CustomMessage"] = "Main Subject is successfully added.";
            }
            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "Main Subject is already existed";

            }
            ModelState.Clear();
            return View();
        }
        public ActionResult ViewMainSubject()
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            int pageNo = 1;
            int pageSize = 100;

            var GetList = ManageMainSubject.GetAllMainSubject(false);
            ViewBag.data = GetList.ToPagedList(pageNo, pageSize);
            ViewBag.TotalCount = GetList.Count;
            ViewBag.pageSize = pageSize;
            ViewBag.IsDeleted = false;
            ViewBag.Page = pageNo;
            return View();
        }
        public ActionResult _ListOfMainSubject(bool IsActive, int page = 1, string SearchStr = "")
        {
            if (SearchStr != "")
            {
                int pageSize = 100;
                List<SolutionLibr> objSolLib = new List<SolutionLibr>();
                //  bool IsDeleted = Convert.ToBoolean(IsActive);
                objSolLib = ManageMainSubject.GetSearchAllMainSubject(SearchStr,IsActive);

                ViewData["TotalSubjectCount"] = objSolLib.Count;
                ViewBag.Page = page;
                ViewBag.pageSize = pageSize;
                ViewBag.IsDeleted = IsActive;
                return Request.IsAjaxRequest()
                                 ? (ActionResult)PartialView("_ListOfMainSubject", objSolLib.ToPagedList(page, pageSize))
                                 : PartialView("_ListOfMainSubject", objSolLib.ToPagedList(page, pageSize));
            }
            else
            {
                int pageSize = 100;
                List<SolutionLibr> objSolLib = new List<SolutionLibr>();
                //  bool IsDeleted = Convert.ToBoolean(IsActive);
                objSolLib = ManageMainSubject.GetAllMainSubject(IsActive);

                ViewData["TotalSubjectCount"] = objSolLib.Count;
                ViewBag.Page = page;
                ViewBag.pageSize = pageSize;
                ViewBag.IsDeleted = IsActive;
                return Request.IsAjaxRequest()
                                 ? (ActionResult)PartialView("_ListOfMainSubject", objSolLib.ToPagedList(page, pageSize))
                                 : PartialView("_ListOfMainSubject", objSolLib.ToPagedList(page, pageSize));
            }
        }
        public JsonResult DeleteMainSubject(int Id, bool status)
        {
            int result = 0;
            long AId = Convert.ToInt64(Id);
            // tblSolutionLibrary MngArticle = new tblSolutionLibrary();
            HttpCookie objadmprof = Request.Cookies["Admin"];
            result = ManageMainSubject.DeleteSubjectData(Id, status, objadmprof["Email"]);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FinalDeleteData(int Id, bool status)
        {
            int result = 0;
            long AId = Convert.ToInt64(Id);
            // tblSolutionLibrary MngArticle = new tblSolutionLibrary();
            HttpCookie objadmprof = Request.Cookies["Admin"];
            result = ManageMainSubject.FinalDeleteSubjectData(Id, status, objadmprof["Email"]);
            result = 4;
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult EditMainSubject(int id)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }

            var data = ManageMainSubject.getSingleMain(id);

            SolutionLibr obj = new SolutionLibr();
            obj.MSID = data.MSID;
            obj.SolSubID = data.SolSubID;
            obj.UID = data.UID;
            obj.country = data.Country;
            obj.SubjectCode = data.SubjectCode;
            obj.Subject = data.SubjectName;

            ViewBag.Subject = GetSubjectCategory();
            ViewBag.University = GetUniversity();

            return View(obj);
        }
        [HttpPost]
        public ActionResult EditMainSubject(SolutionLibr obj)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            ViewBag.Subject = GetSubjectCategory();
            ViewBag.University = GetUniversity();

            tblMainSubject objSubject = new tblMainSubject();
            objSubject.SubjectCode = obj.SubjectCode;
            objSubject.IsActive = false;
            objSubject.UpdatedBy = objadmprof["Email"];
            objSubject.UpdatedOn = DateTime.Now;
            objSubject.Country = obj.country;
            objSubject.SubjectName = obj.Subject;
            objSubject.MSID = Convert.ToInt32(obj.MSID);
            objSubject.UID = obj.UID;
            objSubject.SolSubID = obj.SolSubID;

            int result = ManageMainSubject.AddOrUpdateMainSubject(objSubject);

            if (result == 1)
            {
                TempData["MessageType"] = "Success";
                TempData["CustomMessage"] = "Main Subject is successfully updated.";
            }
            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "Main Subject is already existed";

            }

            //ModelState.Clear();

            return RedirectToAction("ViewMainSubject", "MainSubject", new { Area = "NewAdmin" });
        }
    }
}