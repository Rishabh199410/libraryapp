﻿using LibraryApp.Areas.NewAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LibraryDataLayer;
using System.Web.Mvc;
using LibraryBILayer.Comman;
using PagedList;
using System.IO;

namespace LibraryApp.Areas.NewAdmin.Controllers
{
    public class UniversityController : Controller
    {
        // GET: NewAdmin/University
        AdminAuthentication adminAuthentication = new AdminAuthentication();
        public ActionResult AddUniversity()
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            return View();
        }
        [HttpPost]
        public ActionResult AddUniversity(University obj)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            string filepath = "";

            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase PostedFile = Request.Files[0];
                string FileName = Path.GetFileName(PostedFile.FileName);
                string extension = Path.GetExtension(FileName);
                filepath = "~/img/logos/"+ DateTime.Now.ToString("yyyyddMMss")+ extension;
                PostedFile.SaveAs(Server.MapPath(filepath));
            }

            tblUniversity objUniversity = new tblUniversity();
            objUniversity.CreatedOn = DateTime.Now;
            objUniversity.CreatedBy = objadmprof["Email"];
            objUniversity.University = obj.UniversityName;
            objUniversity.IsActive = false;
            objUniversity.URL = obj.UniversityName.ToLower().Replace(" ", "-");
            objUniversity.country = obj.country;
            objUniversity.description = obj.description;
            objUniversity.Logo = filepath;
            objUniversity.Note = obj.Note;
            objUniversity.shortName = obj.shortName;            

            int result = ManageUniversity.AddOrUpdateUniversity(objUniversity);

            if (result == 1)
            {
                TempData["MessageType"] = "Success";
                TempData["CustomMessage"] = "University is successfully added.";
            }
            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "University is already existed";
                
            }
            ModelState.Clear();
            return View();
        }
        public ActionResult ViewUniversity()
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            int pageNo = 1;
            int pageSize = 100;
            var GetList = ManageUniversity.ObjUniversity(false);
            ViewBag.data = GetList.ToPagedList(pageNo, pageSize);
            ViewBag.TotalCount = GetList.Count;
            ViewBag.pageSize = pageSize;
            ViewBag.IsDeleted = false;
            ViewBag.Page = pageNo;
            return View();
        }
        public ActionResult _ListOfUniversity(bool IsActive, int page = 1)
        {
            int pageSize = 100;
            List<tblUniversity> objUniversity = new List<tblUniversity>();
            //  bool IsDeleted = Convert.ToBoolean(IsActive);
            objUniversity = ManageUniversity.ObjUniversity(IsActive);

            ViewData["TotalUniversityCount"] = objUniversity.Count;
            ViewBag.Page = page;
            ViewBag.pageSize = pageSize;
            ViewBag.IsDeleted = IsActive;
            return Request.IsAjaxRequest()
                             ? (ActionResult)PartialView("_ListOfUniversity", objUniversity.ToPagedList(page, pageSize))
                             : PartialView("_ListOfUniversity", objUniversity.ToPagedList(page, pageSize));

        }
        public JsonResult DeleteUniversity(int Id, bool status)
        {
            int result = 0;
            long AId = Convert.ToInt64(Id);
            // tblSolutionLibrary MngArticle = new tblSolutionLibrary();
            HttpCookie objadmprof = Request.Cookies["Admin"];
            result = ManageUniversity.DeleteUniversityData(Id, status, objadmprof["Email"]);
            result = 4;
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FinalDeleteUniversity(int Id, bool status)
        {
            int result = 0;
            long AId = Convert.ToInt64(Id);
            // tblSolutionLibrary MngArticle = new tblSolutionLibrary();
            HttpCookie objadmprof = Request.Cookies["Admin"];
            result = ManageUniversity.FinalDeleteUniversityData(Id, status, objadmprof["Email"]);
            result = 4;
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult EditUniversity(int id)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }

            var data = ManageUniversity.getUniversity(id);

            tblUniversity obj = new tblUniversity();
            obj.University = data.University;
            obj.UID = data.UID;
            obj.Logo = data.Logo;
            obj.Note = data.Note;
            obj.description = data.description;
            obj.shortName = data.shortName;
            obj.URL = data.URL;
            obj.country = data.country;
            return View(obj);
        }
        [HttpPost]
        public ActionResult EditUniversity(tblUniversity obj)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            string filepath = "";
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase PostedFile = Request.Files[0];
                string FileName = Path.GetFileName(PostedFile.FileName);
                if(FileName=="")
                {
                    filepath = null;
                }
                else
                {
                    string extension = Path.GetExtension(FileName);
                    filepath = "~/img/logos/" + DateTime.Now.ToString("yyyyddMMss") + extension;
                    PostedFile.SaveAs(Server.MapPath(filepath));
                }               
            }
            else
            {
                filepath = obj.Logo;
            }

            tblUniversity objUniversity = new tblUniversity();
            objUniversity.University = obj.University;
            objUniversity.IsActive = false;
            objUniversity.UpdatedBy = objadmprof["Email"];
            objUniversity.UpdatedOn = DateTime.Now;
            objUniversity.UID = obj.UID;
            objUniversity.URL = obj.University.ToLower().Replace(" ", "-");
            objUniversity.country = obj.country;
            objUniversity.description = obj.description;
            objUniversity.Logo = filepath;
            objUniversity.Note = obj.Note;
            objUniversity.shortName = obj.shortName;
            int result = ManageUniversity.AddOrUpdateUniversity(objUniversity);

            if (result == 1)
            {
                TempData["MessageType"] = "Success";
                TempData["CustomMessage"] = "University is successfully updated.";
            }
            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "University is already existed";

            }
            // ModelState.Clear();
            return RedirectToAction("ViewUniversity", "University", new { Area = "NewAdmin" });
            }
    }
}