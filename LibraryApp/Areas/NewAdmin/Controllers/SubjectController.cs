﻿using LibraryApp.Areas.NewAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LibraryDataLayer;
using LibraryBILayer.Comman;
using PagedList;

namespace LibraryApp.Areas.NewAdmin.Controllers
{
    public class SubjectController : Controller
    {
        // GET: NewAdmin/Subject
        AdminAuthentication adminAuthentication = new AdminAuthentication();
        public ActionResult AddSubject()
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            return View();
        }
        [HttpPost]
        public ActionResult AddSubject(tblSolutionSubject obj)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];

            int GetMax = Convert.ToInt32(SolLibrary.GetMaxValue());

            tblSolutionSubject objSubject = new tblSolutionSubject();
            objSubject.SolSubID = GetMax + 1;
            objSubject.CreatedOn = DateTime.Now;
            objSubject.CreatedBy = objadmprof["Email"];
            objSubject.Subject = obj.Subject;
            objSubject.IsActive = false;
            objSubject.Url = obj.Subject.ToLower().Replace(" ", "-");

            int result = ManageSubject.AddOrUpdateSubject(objSubject);

            if (result == 1)
            {
                TempData["MessageType"] = "Success";
                TempData["CustomMessage"] = "Subject is successfully added.";
            }
            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "Subject is already existed";

            }
            ModelState.Clear();
            return View();
        }
        public ActionResult ViewSubject()
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            int pageNo = 1;
            int pageSize = 100;
            var GetList = ManageSubject.ObjSubjects(false);
            ViewBag.data = GetList.ToPagedList(pageNo, pageSize);
            ViewBag.TotalCount = GetList.Count;
            ViewBag.pageSize = pageSize;
            ViewBag.IsDeleted = false;
            ViewBag.Page = pageNo;
            return View();
        }
        public ActionResult _ListOfSubject(bool IsActive, int page = 1)
        {
            int pageSize = 100;
            List<tblSolutionSubject> objUniversity = new List<tblSolutionSubject>();
            //  bool IsDeleted = Convert.ToBoolean(IsActive);
            objUniversity = ManageSubject.ObjSubjects(IsActive);

            ViewData["TotalSubjectCount"] = objUniversity.Count;
            ViewBag.Page = page;
            ViewBag.pageSize = pageSize;
            ViewBag.IsDeleted = IsActive;
            return Request.IsAjaxRequest()
                             ? (ActionResult)PartialView("_ListOfSubject", objUniversity.ToPagedList(page, pageSize))
                             : PartialView("_ListOfSubject", objUniversity.ToPagedList(page, pageSize));

        }
        public JsonResult DeleteSubject(int Id, bool status)
        {
            int result = 0;
            long AId = Convert.ToInt64(Id);
            // tblSolutionLibrary MngArticle = new tblSolutionLibrary();
            HttpCookie objadmprof = Request.Cookies["Admin"];
            result = ManageSubject.DeleteSubjectData(Id, status, objadmprof["Email"]);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FinalSubjectDelete(int Id, bool status)
        {
            int result = 0;
            long AId = Convert.ToInt64(Id);
            // tblSolutionLibrary MngArticle = new tblSolutionLibrary();
            HttpCookie objadmprof = Request.Cookies["Admin"];

            result = ManageSubject.FinalDeleteSubjectData(Id, status, objadmprof["Email"]);
            result = 4;
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult EditSubject(int id)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }

            var data = ManageSubject.getSubject(id);

            tblSolutionSubject obj = new tblSolutionSubject();
            obj.Subject = data.Subject;
            obj.id = data.id;
            return View(obj);
        }
        [HttpPost]
        public ActionResult EditSubject(tblSolutionSubject obj)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];

            tblSolutionSubject objSubject = new tblSolutionSubject();
            objSubject.Subject = obj.Subject;
            objSubject.IsActive = false;
            objSubject.UpdatedBy = objadmprof["Email"];
            objSubject.UpdatedOn = DateTime.Now;
            objSubject.id = obj.id;
            objSubject.Url = obj.Subject.ToLower().Replace(" ", "-");
            int result = ManageSubject.AddOrUpdateSubject(objSubject);

            if (result == 1)
            {
                TempData["MessageType"] = "Success";
                TempData["CustomMessage"] = "Subject is successfully updated.";
            }
            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "Subject is already existed";

            }
            return RedirectToAction("ViewSubject", "Subject", new { Area = "NewAdmin" });
        }
    }
}