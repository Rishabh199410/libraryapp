﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using LibraryDataLayer;
using LibraryBILayer.Admin;
using PagedList;
using LibraryApp.Models;

namespace LibraryApp.Areas.NewAdmin.Controllers
{
    public class ManageRoleClaimController : Controller
    {
        public ActionResult Index()
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            return View();
        }

        [ChildActionOnly]
        public ActionResult GetAdminNavigation()
        {
            List<AdminRolesModel> list = new List<AdminRolesModel>();
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            List<tbladminnavigation> navigationList = ManageAdminRoleClaim.GetNavigation();
            foreach (var item in navigationList)
            {
                AdminRolesModel model = new AdminRolesModel();
                model.AdminNavigation = item.AdminNavigation;
                model.AdminNavigationID = item.AdminNavigationID;
                model.IsParent = item.IsParent;
                model.IsSelected = false;
                list.Add(model);
            }
            return View("~/Areas/NewAdmin/Views/ManageRoleClaim/_GetAdminNavigation.cshtml", list);
        }
        public ActionResult GetAssignedNavigationList(int RoleID)
        {
            List<AdminRolesModel> list = new List<AdminRolesModel>();
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            List<tbladminnavigation> navigationList = ManageAdminRoleClaim.GetNavigation();
            List<tbladminroleclaim> claimList = ManageAdminRoleClaim.GetRoleClaimsByID(RoleID);
            foreach (var item in navigationList)
            {
                AdminRolesModel model = new AdminRolesModel();
                model.AdminNavigation = item.AdminNavigation;
                model.AdminNavigationID = item.AdminNavigationID;
                model.IsParent = item.IsParent;
                if (claimList.Any(x => x.AdminNavigationID == item.AdminNavigationID))
                    model.IsSelected = true;
                list.Add(model);
            }
            return View("~/Areas/NewAdmin/Views/ManageRoleClaim/_GetAdminNavigation.cshtml", list);
        }

        public JsonResult AssignRoles(List<int> roleIDList, int RoleID)
        {
            int returnValue = 0;
            List<tbladminroleclaim> roleClaim = new List<tbladminroleclaim>();
            if (ManageAdminRoleClaim.RemoveRoleClaims(RoleID))
            {
                foreach (var item in roleIDList)
                {
                    tbladminroleclaim model = new tbladminroleclaim();
                    model.AdminNavigationID = item;
                    model.RoleID = RoleID;
                    model.CreatedBy = 1;
                    model.ModifiedBy = 1;
                    model.ModifiedOn = DateTime.UtcNow;
                    model.CreatedOn = DateTime.UtcNow;
                    roleClaim.Add(model);
                }
                returnValue = ManageAdminRoleClaim.AddRoleClaims(roleClaim);
            }
            return Json(returnValue);
        }
    }
}