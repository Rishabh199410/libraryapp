﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LibraryDataLayer;
using LibraryBILayer.Admin;
using System.Text.RegularExpressions;
using System.IO;
using PagedList;
using LibraryApp.Models;

namespace LibraryApp.Areas.NewAdmin.Controllers
{
    public class AdminRoleController : Controller
    {

        public ActionResult Index()
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            return View();
        }

        public ActionResult AdminRoleList()
       {
            int pageNo = 1;
            int pageSize = 100;
            var GetList = ManageAdminRole.GetAdminRoleList();
            ViewBag.data = GetList.ToPagedList(pageNo, pageSize);
            ViewBag.TotalCount = GetList.Count;
            ViewBag.pageSize = pageSize;
            ViewBag.IsDeleted = 1;
            ViewBag.Page = pageNo;
            return View();
        }

        public ActionResult EditAdminRole(int Id)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];

            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }

            //DropDownInitialize();
            tbladminrole data = ManageAdminRole.GetAdminRole(Id);

            return View("~/Areas/NewAdmin/Views/AdminRole/EditAdminRole.cshtml", data);
        }

        [HttpPost]
        public ActionResult EditAdminRole(tbladminrole obj)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];

            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            //DropDownInitialize();
            // var data = ManageBlog.GetBogCat(Id);

            obj.ModifiedBy = 1; //objadmprof["Email"].ToString();
            obj.ModifiedOn = DateTime.Now;
            //string meGet1 = Regex.Replace(obj.BlogCatName, "<.*?>", String.Empty);
            //meGet1 = meGet1.Replace(" ", "-").ToLower().Substring(0, meGet1.Length > 150 ? 150 : meGet1.Length);

            int result = ManageAdminRole.AddorUpdateAdminRole(obj);

            if (result == 1)
            {
                TempData["MessageType"] = "Success";
                TempData["CustomMessage"] = "Role is successfully Updated.";
            }
            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "Something Went Wrong !";
            }
            ModelState.Clear();
            return RedirectToAction("AdminRoleList", "AdminRole", new { @Areas = "NewAdmin" });

        }

        public ActionResult AdminRoleLists()
        {
            List<tbladminrole> GetList = ManageAdminRole.GetAdminRoleList();
            return View("~/Areas/NewAdmin/Views/AdminRole/AdminRoleList.cshtml", GetList);
        }

        [HttpPost]
        public ActionResult AddAdminRole(tbladminrole obj)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }

            obj.CreatedBy = 1;//objadmprof["Email"].ToString();
            obj.CreatedOn = DateTime.Now;
            int result = ManageAdminRole.AddorUpdateAdminRole(obj);

            if (result == 1)
            {
                TempData["MessageType"] = "Success";
                TempData["CustomMessage"] = "Role is successfully added.";
            }
            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "Something Went Wrong !";
            }
            ModelState.Clear();
            return RedirectToAction("AdminRoleList", "AdminRole", new { area = "NewAdmin" });

        }
        
        public ActionResult DeleteAdminRole(int Id, int status)
        {
            int result = 0;
            HttpCookie objadmprof = Request.Cookies["Admin"];

            result = ManageAdminRole.DeleteAdminRole(Id, status);//, objadmprof["Email"].ToString());
            //return Json(result);
            return RedirectToAction("AdminRoleList", "AdminRole", new { @Areas = "NewAdmin" });
            //return RedirectToAction("UserNavigationList","UserNavigation");
        }

        public ActionResult FinalDeleteAdminRole(int Id)
        {
            int result = 0;
            long AId = Convert.ToInt64(Id);
            HttpCookie objadmprof = Request.Cookies["Admin"];
            result = 4;
            result = ManageAdminRole.FinalDeleteAdminRole(Id);

            return RedirectToAction("AdminRoleList", "AdminRole", new { @Areas = "NewAdmin" });
        }

        public ActionResult _ListOfAdminRole(int IsActive, int page = 1)
        {
            int pageSize = 100;
            List<tbladminrole> objFreeSamples = new List<tbladminrole>();
            //  bool IsDeleted = Convert.ToBoolean(IsActive);
            // SolutionLibr MngArticle = new SolutionLibr();
            objFreeSamples = ManageAdminRole.GetAdminRoleList(IsActive);

            ViewData["TotalAdminRoleCount"] = objFreeSamples.Count;
            ViewBag.Page = page;
            ViewBag.pageSize = pageSize;
            ViewBag.IsDeleted = IsActive;
            return Request.IsAjaxRequest()
                             ? (ActionResult)PartialView("_ListOfAdminRole", objFreeSamples.ToPagedList(page, pageSize))
                             : PartialView("_ListOfAdminRole", objFreeSamples.ToPagedList(page, pageSize));
        }
    }
}