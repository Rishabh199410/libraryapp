﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LibraryDataLayer;
using LibraryBILayer.Admin;
using System.Text.RegularExpressions;
using System.IO;
using PagedList;
using LibraryApp.Models;

namespace LibraryApp.Areas.NewAdmin.Controllers
{
    public class UserNavigationController : Controller
    {
        public ActionResult EditUserNavigation(int Id)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];

            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }

            //DropDownInitialize();
            tblusernavigation data = UserNavigation.GetUserNavigation(Id);

            return View("~/Areas/NewAdmin/Views/UserNavigation/EditUserNavigation.cshtml", data);
        }

        [HttpPost]
        public ActionResult EditUserNavigation(tblusernavigation obj)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];

            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            //DropDownInitialize();
            // var data = ManageBlog.GetBogCat(Id);

            obj.ModifiedBy = 1; //objadmprof["Email"].ToString();
            obj.ModifiedOn = DateTime.Now;
            //string meGet1 = Regex.Replace(obj.BlogCatName, "<.*?>", String.Empty);
            //meGet1 = meGet1.Replace(" ", "-").ToLower().Substring(0, meGet1.Length > 150 ? 150 : meGet1.Length);

            int result = UserNavigation.AddorUpdateUserNavigation(obj);

            if (result == 1)
            {
                TempData["MessageType"] = "Success";
                TempData["CustomMessage"] = "User Navigation is successfully updated.";
            }
            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "Something Went Wrong !";
            }
            ModelState.Clear();
            return RedirectToAction("UserNavigationList", "UserNavigation", new { @Areas = "NewAdmin" });

        }

        public ActionResult UserNavigationList()
        {
            List<tblusernavigation> GetList = UserNavigation.GetUserNavigationList();
            return View("~/Areas/NewAdmin/Views/UserNavigation/UserNavigationList.cshtml", GetList);
        }

        //public ActionResult AddPlagiarism(mstplagiarismprice obj)
        //{
        //    HttpCookie objadmprof = Request.Cookies["Admin"];
        //    if (objadmprof == null)
        //    {
        //        return RedirectToAction("Index", "Panel");
        //    }

        //    obj.CreatedBy = objadmprof["Email"].ToString();
        //    obj.CreatedOn = DateTime.Now;
        //    int result = ManagePlagiarism.AddorUpdatePlagiarism(obj);

        //    if (result == 1)
        //    {
        //        TempData["MessageType"] = "Success";
        //        TempData["CustomMessage"] = "Plagiarism is successfully added.";
        //    }
        //    else
        //    {
        //        TempData["MessageType"] = "Error";
        //        TempData["CustomMessage"] = "Something Went Wrong !";
        //    }
        //    ModelState.Clear();
        //    return RedirectToAction("BlogCatList", "BlogCat", new { area = "NewAdmin" });

        //}

        public JsonResult DeleteUserNavigation(int Id, int status)
        {
            int result = 0;
            HttpCookie objadmprof = Request.Cookies["Admin"];

            result = UserNavigation.DeleteUserNavigation(Id, status);//, objadmprof["Email"].ToString());
            return Json(result);
            //return RedirectToAction("UserNavigationList","UserNavigation");
        }
    }
}