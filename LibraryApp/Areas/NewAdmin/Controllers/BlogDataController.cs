﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LibraryDataLayer;
using LibraryBILayer.Admin;
using System.Text.RegularExpressions;
using System.IO;
using PagedList;
using LibraryApp.Models;
using OfficeOpenXml;

namespace LibraryApp.Areas.NewAdmin.Controllers
{
    public class BlogDataController : Controller
    {
        // GET: NewAdmin/BlogData
        FillDropDown objddl = new FillDropDown();
        public void DropDownInitialize()
        {
            ViewBag.Category = objddl.GetBlogCategory();
        }
        public ActionResult Index()
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            DropDownInitialize();
            return View();
        }

        public void DownloadExcel()
        {
            var blogList = ManageBlog.GetBlogList(true);
            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");
            Sheet.Cells["A1"].Value = "ID";
            Sheet.Cells["B1"].Value = "Title";
            Sheet.Cells["C1"].Value = "Meta Description";
            Sheet.Cells["D1"].Value = "Meta Keyword";
            Sheet.Cells["E1"].Value = "Content";

            int row = 2;
            foreach (var item in blogList)
            {
                Sheet.Cells[string.Format("A{0}", row)].Value = item.ID;
                Sheet.Cells[string.Format("B{0}", row)].Value = item.Title;
                Sheet.Cells[string.Format("C{0}", row)].Value = item.MetaDesc;
                Sheet.Cells[string.Format("D{0}", row)].Value = item.MetaKey;
                Sheet.Cells[string.Format("E{0}", row)].Value = item.Content;
                row++;
            }

            Sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename=" + "Blogs.xlsx");
            Response.BinaryWrite(Ep.GetAsByteArray());
            Response.End();
        }


        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddBlog(tblBlogLib obj, HttpPostedFileBase file)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            string path = "";
            DropDownInitialize();
            if (file != null)
            {
                // HttpPostedFileBase PostedFile = Request.Files;

                string FileName = Path.GetFileName(file.FileName);

                string extension = Path.GetExtension(FileName);

                path = "~/blog_img/" + "BG" + DateTime.Now.ToString("ddmmssfff") + extension;

                file.SaveAs(Server.MapPath(path));

            }

            string meGet1 = Regex.Replace(obj.Title, "<.*?>", String.Empty);
            meGet1 = meGet1.Replace(" ", "-").ToLower().Substring(0, meGet1.Length > 150 ? 150 : meGet1.Length);
            if (meGet1[meGet1.Length - 1].ToString() == "?")
                meGet1 = meGet1.Replace("?", "");

            obj.CreatedBy = objadmprof["Email"].ToString();
            obj.CreatedOn = DateTime.Now;
            obj.VirtualUrl = meGet1;
            obj.Image = path;
            obj.IsActive = true;

            int result = ManageBlog.AddOrUpdateBlog(obj);

            if (result == 1)
            {
                TempData["MessageType"] = "Success";
                TempData["CustomMessage"] = "Blog is successfully added.";
            }
            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "Something Went Wrong !";
            }
            ModelState.Clear();
            return RedirectToAction("Index", "BlogData", new { area = "NewAdmin" });
        }
        public ActionResult BlogLists()
        {
            int pageNo = 1;
            int pageSize = 100;
            var GetList = ManageBlog.GetBlogList(true);
            ViewBag.data = GetList.ToPagedList(pageNo, pageSize);
            ViewBag.TotalCount = GetList.Count;
            ViewBag.pageSize = pageSize;
            ViewBag.IsDeleted = 1;
            ViewBag.Page = pageNo;
            return View();
        }
        public ActionResult _ListOfBlog(bool IsActive, int page = 1)
        {
            int pageSize = 100;
            List<tblBlogLib> objFreeSamples = new List<tblBlogLib>();
            //  bool IsDeleted = Convert.ToBoolean(IsActive);
            // SolutionLibr MngArticle = new SolutionLibr();
            objFreeSamples = ManageBlog.GetBlogList(IsActive);

            ViewData["TotalBlogCount"] = objFreeSamples.Count;
            ViewBag.Page = page;
            ViewBag.pageSize = pageSize;
            ViewBag.IsDeleted = IsActive;
            return Request.IsAjaxRequest()
                             ? (ActionResult)PartialView("_ListOfBlog", objFreeSamples.ToPagedList(page, pageSize))
                             : PartialView("_ListOfBlog", objFreeSamples.ToPagedList(page, pageSize));
        }
        public JsonResult FinalDeleteBlog(int Id, bool status)
        {
            int result = 0;
            long AId = Convert.ToInt64(Id);
            // tblSolutionLibrary MngArticle = new tblSolutionLibrary();
            HttpCookie objadmprof = Request.Cookies["Admin"];
            result = 4;
            result = ManageBlog.DeleteMyBlog(Id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteBlog(int Id, bool status)
        {
            int result = 0;
            HttpCookie objadmprof = Request.Cookies["Admin"];

            result = ManageBlog.DeleteBlogs(Id, status, objadmprof["Email"]);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult EditBlog(int Id)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];

            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            DropDownInitialize();
            var data = ManageBlog.GetBogLib(Id);

            tblBlogLib myBlog = new tblBlogLib();
            myBlog.ID = Id;
            myBlog.Image = data.Image;
            myBlog.Title = data.Title;
            myBlog.MetaDesc = data.MetaDesc;
            myBlog.MetaKey = data.MetaKey;
            myBlog.Content = data.Content;
            myBlog.VirtualUrl = data.VirtualUrl;
            myBlog.BlogCat = data.BlogCat;
            return View(myBlog);
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult EditBlog(tblBlogLib obj, HttpPostedFileBase file)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];

            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            DropDownInitialize();
            string path = "";

            if (file != null)
            {
                // HttpPostedFileBase PostedFile = Request.Files;

                string FileName = Path.GetFileName(file.FileName);

                string extension = Path.GetExtension(FileName);

                path = "~/blog_img/" + "BG" + DateTime.Now.ToString("ddmmssfff") + extension;

                file.SaveAs(Server.MapPath(path));

            }
            else
            {
                path = obj.Image;
            }

            string meGet1 = Regex.Replace(obj.Title, "<.*?>", String.Empty);
            //meGet1 = meGet1.Replace(",", "").Replace(" ", "-").Replace("---", "-");
            //System.Text.RegularExpressions.Regex rx1 = new System.Text.RegularExpressions.Regex("[^0-9a-zA-Z]+");
            meGet1 = meGet1.Replace(" ", "-").ToLower().Substring(0, meGet1.Length > 150 ? 150 : meGet1.Length);

            obj.CreatedBy = objadmprof["Email"].ToString();
            obj.CreatedOn = DateTime.Now;
            obj.VirtualUrl = obj.VirtualUrl;

            obj.Image = path;
            obj.IsActive = true;

            int result = ManageBlog.AddOrUpdateBlog(obj);

            if (result == 1)
            {
                TempData["MessageType"] = "Success";
                TempData["CustomMessage"] = "Blog is successfully added.";
            }
            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "Something Went Wrong !";
            }
            ModelState.Clear();
            return RedirectToAction("BlogLists", "BlogData", new { area = "NewAdmin" });
        }

        public ActionResult UploadBlog(FormCollection formCollection)
        {
            if (Request != null)
            {
                HttpCookie objadmprof = Request.Cookies["Admin"];
                if (objadmprof == null)
                {
                    return RedirectToAction("Index", "Panel");
                }

                List<tblBlogLib> list = new List<tblBlogLib>();
                HttpPostedFileBase file = Request.Files["UploadedFile"];
                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                    using (var package = new ExcelPackage(file.InputStream))
                    {
                        var currentSheet = package.Workbook.Worksheets;
                        var workSheet = currentSheet.First();
                        var noOfCol = workSheet.Dimension.End.Column;
                        var noOfRow = workSheet.Dimension.End.Row;
                        for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                        {
                            tblBlogLib model = new tblBlogLib();

                            string meGet1 = Regex.Replace(workSheet.Cells[rowIterator, 1].Value.ToString(), "<.*?>", String.Empty);
                            meGet1 = meGet1.Replace(" ", "-").ToLower().Substring(0, meGet1.Length > 150 ? 150 : meGet1.Length);
                            if (meGet1[meGet1.Length - 1].ToString() == "?")
                                meGet1 = meGet1.Replace("?", "");

                            model.MetaDesc = workSheet.Cells[rowIterator, 2].Value.ToString();
                            model.MetaKey = workSheet.Cells[rowIterator, 3].Value.ToString();
                            model.Content = workSheet.Cells[rowIterator, 4].Value.ToString();
                            model.CreatedBy = objadmprof["Email"].ToString();
                            model.CreatedOn = DateTime.Now;
                            model.UpdatedBy = objadmprof["Email"].ToString();
                            model.UpdatedOn = DateTime.Now;
                            model.VirtualUrl = meGet1;
                            model.Title = meGet1;
                            model.IsActive = true;
                            int result = ManageBlog.AddOrUpdateBlog(model);
                        }
                    }
                }

                else
                {
                    TempData["MessageType"] = "Error";
                    TempData["CustomMessage"] = "No file selected";
                }
            }

            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "Something Went Wrong !";
            }
            return RedirectToAction("BlogLists", "BlogData", new { area = "NewAdmin" });
        }
    }
}