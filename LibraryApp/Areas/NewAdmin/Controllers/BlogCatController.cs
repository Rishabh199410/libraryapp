﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using LibraryDataLayer;
using LibraryBILayer.Admin;
using PagedList;
using LibraryApp.Models;

namespace LibraryApp.Areas.NewAdmin.Controllers
{
    public class BlogCatController : Controller
    {
        // GET: NewAdmin/BlogCat
        
        public ActionResult Index()
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            return View();
        }
        [HttpPost]
        public ActionResult AddBlogCat(tblBlogCat obj)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }

            obj.CreatedBy = objadmprof["Email"].ToString();
            obj.CreatedOn = DateTime.Now;
            string meGet1 = Regex.Replace(obj.BlogCatName, "<.*?>", String.Empty);
            meGet1 = meGet1.Replace(" ", "-").ToLower().Substring(0, meGet1.Length > 150 ? 150 : meGet1.Length);
            obj.Virtual_Url = meGet1;
            obj.IsActive = true;
            int result = ManageBlog.AddOrUpdateBlogCat(obj);

            if (result == 1)
            {
                TempData["MessageType"] = "Success";
                TempData["CustomMessage"] = "Blog Category is successfully added.";
            }
            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "Something Went Wrong !";
            }
            ModelState.Clear();
            return RedirectToAction("BlogCatList", "BlogCat", new { area = "NewAdmin" });

        }
        public ActionResult BlogCatList()
        {
            int pageNo = 1;
            int pageSize = 100;
            var GetList = ManageBlog.GetBlogCatList(true);
            ViewBag.data = GetList.ToPagedList(pageNo, pageSize);
            ViewBag.TotalCount = GetList.Count;
            ViewBag.pageSize = pageSize;
            ViewBag.IsDeleted = 1;
            ViewBag.Page = pageNo;
            return View();
        }
        public ActionResult _ListOfBlogCat(bool IsActive, int page = 1)
        {
            int pageSize = 100;
            List<tblBlogCat> objFreeSamples = new List<tblBlogCat>();
            //  bool IsDeleted = Convert.ToBoolean(IsActive);
            // SolutionLibr MngArticle = new SolutionLibr();
            objFreeSamples = ManageBlog.GetBlogCatList(IsActive);

            ViewData["TotalBlogCatCount"] = objFreeSamples.Count;
            ViewBag.Page = page;
            ViewBag.pageSize = pageSize;
            ViewBag.IsDeleted = IsActive;
            return Request.IsAjaxRequest()
                             ? (ActionResult)PartialView("_ListOfBlogCat", objFreeSamples.ToPagedList(page, pageSize))
                             : PartialView("_ListOfBlogCat", objFreeSamples.ToPagedList(page, pageSize));
        }
        public ActionResult EditBlogCat(int Id)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];

            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }

            //DropDownInitialize();
            var data = ManageBlog.GetBogCat(Id);

            return View(data);
        }
        [HttpPost]
        public ActionResult EditBlogCat(tblBlogCat obj)
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];

            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            //DropDownInitialize();
            // var data = ManageBlog.GetBogCat(Id);

            obj.UpdatedBy = objadmprof["Email"].ToString();
            obj.UpdatedOn = DateTime.Now;
            //string meGet1 = Regex.Replace(obj.BlogCatName, "<.*?>", String.Empty);
            //meGet1 = meGet1.Replace(" ", "-").ToLower().Substring(0, meGet1.Length > 150 ? 150 : meGet1.Length);
            
            int result = ManageBlog.AddOrUpdateBlogCat(obj);

            if (result == 1)
            {
                TempData["MessageType"] = "Success";
                TempData["CustomMessage"] = "Blog Category is successfully added.";
            }
            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "Something Went Wrong !";
            }
            ModelState.Clear();
            return RedirectToAction("BlogCatList", "BlogCat", new { area = "NewAdmin" });
            
        }
        public JsonResult FinalDeleteBlogCat(int Id, bool status)
        {
            int result = 0;
            long AId = Convert.ToInt64(Id);
            // tblSolutionLibrary MngArticle = new tblSolutionLibrary();
            HttpCookie objadmprof = Request.Cookies["Admin"];
            result = 4;
            result = ManageBlog.DeleteMyBlogCat(Id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteBlogCat(int Id, bool status)
        {
            int result = 0;
            HttpCookie objadmprof = Request.Cookies["Admin"];

            result = ManageBlog.DeleteBlogCat(Id, status, objadmprof["Email"].ToString());

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}