﻿using LibraryApp.Areas.NewAdmin.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LibraryDataLayer;
using System.Web.Mvc;
using LibraryBILayer.Admin;
using System.IO;
using LibraryBILayer.Comman;
using LibraryBILayer.Model;

namespace LibraryApp.Areas.NewAdmin.Controllers
{
    public class PanelController : Controller
    {
        // GET: NewAdmin/Panel
        AdminAuthentication adminAuthentication = new AdminAuthentication();
        public ActionResult Index()
        {
            return View();
        }
        //public ActionResult RegisterData()
        //{
        //    HttpCookie objadmprof = Request.Cookies["Admin"];
        //    if (objadmprof == null)
        //    {
        //        return RedirectToAction("Index", "Panel");
        //    }

        //    int pageNo = 1;
        //    int pageSize = 500;
        //    List<SP_GETSTUDENT_Result> tblObj = new List<SP_GETSTUDENT_Result>();
        //    tblObj = ManageStudent.StudentDetails(false);
        //    ViewBag.TotalCount = tblObj.Count;
        //    ViewBag.StudentInfo = tblObj.ToPagedList(pageNo, pageSize);
        //    ViewBag.Page = 1;
        //    ViewBag.pageSize = pageSize;
        //    ViewBag.Sort = "name_asc";

        //    return View();
        //}
        //public ActionResult _StudentList(bool IsActive, int page = 1, string SearchStr = "")
        //{
        //    if (SearchStr == "")
        //    {
        //        int pageSize = 20;
        //        List<SP_GETSTUDENT_Result> tblObj = new List<SP_GETSTUDENT_Result>();
        //        tblObj = ManageStudent.StudentDetails(IsActive);

        //        ViewData["TotalStudentCount"] = tblObj.Count;
        //        ViewBag.Page = page;
        //        ViewBag.pageSize = pageSize;

        //        return Request.IsAjaxRequest()
        //               ? (ActionResult)PartialView("_StudentList", tblObj.ToPagedList(page, pageSize))
        //               : PartialView("_StudentList", tblObj.ToPagedList(page, pageSize));
        //    }
        //    else
        //    {

        //        int pageSize = 20;
        //        List<tblCollegeStudent> tblObj = new List<tblCollegeStudent>();
        //        tblObj = ManageStudent.StudentDetailsSearch(SearchStr);

        //        ViewData["TotalStudentCount"] = tblObj.Count;
        //        ViewBag.Page = page;
        //        ViewBag.pageSize = pageSize;


        //        return Request.IsAjaxRequest()
        //               ? (ActionResult)PartialView("_StudentList", tblObj.ToPagedList(page, pageSize))
        //               : PartialView("_StudentList", tblObj.ToPagedList(page, pageSize));
        //    }
        //}
        public JsonResult DeleteStudent(int Id, bool status)
        {
            int result = 0;
            long AId = Convert.ToInt64(Id);
            // tblSolutionLibrary MngArticle = new tblSolutionLibrary();
            HttpCookie objadmprof = Request.Cookies["Admin"];
            result = ManageStudent.DeleteStudentData(Id, status, objadmprof["Email"]);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FinalDeleteStudent(int Id, bool status)
        {
            int result = 0;
            long AId = Convert.ToInt64(Id);
            // tblSolutionLibrary MngArticle = new tblSolutionLibrary();
            HttpCookie objadmprof = Request.Cookies["Admin"];
            result = ManageStudent.FinalDeleteStudentData(Id, status, objadmprof["Email"]);
            result = 4;
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult OrderData()
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }

            int pageNo = 1;
            int pageSize = 25;
            List<tbl_orderLibrary> tblObj = new List<tbl_orderLibrary>();
            tblObj = ManageOrder.GetOrderList(false);
            ViewBag.TotalCount = tblObj.Count;
            ViewBag.StudentInfo = tblObj.ToPagedList(pageNo, pageSize);
            ViewBag.Page = 1;
            ViewBag.pageSize = pageSize;
            ViewBag.Sort = "name_asc";

            return View();
        }
        public ActionResult _OrderData(bool IsActive, int page = 1)
        {
            int pageSize = 25;
            List<tbl_orderLibrary> objFreeSamples = new List<tbl_orderLibrary>();
            //  bool IsDeleted = Convert.ToBoolean(IsActive);
            SolutionLibr MngArticle = new SolutionLibr();
            objFreeSamples = ManageOrder.GetOrderList(IsActive);

            ViewData["TotalArticleCount"] = objFreeSamples.Count;
            ViewBag.Page = page;
            ViewBag.pageSize = pageSize;
            ViewBag.IsDeleted = IsActive;
            return Request.IsAjaxRequest()
                             ? (ActionResult)PartialView("_OrderData", objFreeSamples.ToPagedList(page, pageSize))
                             : PartialView("_OrderData", objFreeSamples.ToPagedList(page, pageSize));
        }
        public ActionResult ViewStudentData(string id)
        {
            var GetData = ManageStudent.GetStudentData(id);
            tblCollegeStudent tbc = new tblCollegeStudent();
            tbc.StudID = GetData.StudID;
            tbc.Email = GetData.Email;
            tbc.Name = GetData.Name;
            tbc.PhoneNo = GetData.PhoneNo;
            tbc.country = GetData.country;
            return View(tbc);
        }
        public ActionResult ViewOrderDetailFull(string id)
        {
            SolutionLibr sol = ManageOrder.GetSolution(id);
            ViewBag.OrderID = id;
            return View(sol);
        }

        [HttpPost]
        public JsonResult getStudentDetails(string Id)
        {
            tblCollegeStudent result = ManageLibrary.GetStudentDatas(Id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddStudent()
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }

            return View();
        }
        [HttpPost]
        public ActionResult AddStudent(tblCollegeStudent obj)
        {
            SysEmail emailObj = new SysEmail();

            string memberid = "CM" + DateTime.Now.ToString("ddMMyyyy") + DateTime.Now.ToString("mmssfff");

            tblCollegeStudent member = new tblCollegeStudent();
            member.Name = obj.Name;
            member.Email = obj.Email;
            member.Password = obj.Password;
            member.PhoneNo = obj.PhoneNo;
            member.StudID = memberid;
            member.CreatedDate = DateTime.Now;
            member.IsActive = false;
            member.country = obj.country;

            string mainMail = System.Configuration.ConfigurationManager.AppSettings["mainMail"].ToString();
            StreamReader reader = new StreamReader(Server.MapPath("~/Emailer/register2.html"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("@@name@@", obj.Name);
            myString = myString.Replace("@@username@@", obj.Email);
            myString = myString.Replace("@@password@@", obj.Password);

            /*----SEND MAIL NOTIFICATIONS TO CS ID----*/
            emailObj.sendmail2(obj.Email, "Classmaster Registration Successfull !", myString);
            //ado.SendMail(assignmentID, myString, myStud.email, "Assignment Order - " + assignmentID);
            reader.Close();

            int result = GetRegisterUs.checkOrAddMember(member);

            if (result == 1)
            {
                TempData["MessageType"] = "Success";
                TempData["CustomMessage"] = "Student registered successfully !!";
            }
            else
            {
                TempData["MessageType"] = "Error";
                TempData["CustomMessage"] = "Student is already existed";

            }
            ModelState.Clear();

            return View();
        }

      
        [ChildActionOnly]
        public ActionResult GetNavigationMenu()
        {
            HttpCookie objadmprof = Request.Cookies["Admin"];
            if (objadmprof == null)
            {
                return RedirectToAction("Index", "Panel");
            }
            List<AdminNavigationModel> manuList = UserNavigation.GetAdminNavigationList(Convert.ToInt32(objadmprof["RoleID"]));
            return PartialView("~/Areas/NewAdmin/Views/Shared/_AdminNavigation.cshtml", manuList);
        }
    }
}