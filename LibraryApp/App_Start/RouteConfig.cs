﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using LibraryDataLayer;

namespace LibraryApp
{
    public class RouteConfig
    {

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //var subSolution = LibraryBILayer.Comman.SolLibrary.GetSolutionLbrURLMain();
            //for (int i = 0; i < subSolution.Count; i++)
            //{
            //    routes.MapRoute(
            //       name: "SolutionSub" + i + "",
            //       url: "solution/" + subSolution[i].UrlText + "/{id}",
            //       defaults: new { controller = "Home", action = "Page", id = UrlParameter.Optional }
            //   );
            //}

            //            routes.MapRoute(
            //    name: "Page2",
            //    url: "{slug}",
            //    defaults: new { controller = "Home", action = "Page", id = UrlParameter.Optional },
            //    constraints: new { slug = new SlugConstraint() }
            //);

            routes.MapRoute(
    name: "CmsRoute",
    url: "{*permalink}",
    defaults: new { controller = "Home", action = "Page", id = UrlParameter.Optional },
    constraints: new { permalink = new CmsUrlConstraint() }
);

            routes.MapRoute(
   name: "CmsRoute2",
   url: "blog/{*permalink}",
   defaults: new { controller = "BlogDataNew", action = "BlogInfo", id = UrlParameter.Optional },
   constraints: new { permalink = new CmsUrlConstraint2() }
);

            routes.MapRoute(
   name: "CmsRoute21",
   url: "blog/category/{*permalink}",
   defaults: new { controller = "BlogDataNew", action = "BlogCategory", id = UrlParameter.Optional },
   constraints: new { permalink = new CmsUrlConstraint21() }
);

            var solution = LibraryBILayer.Comman.SolLibrary.GetSubjectListURL();
            for (int i = 0; i < solution.Count; i++)
            {
                routes.MapRoute(
                   name: "Solution" + i + "",
                   url: "solutions/" + solution[i].Subject.ToLower().Replace(" ", "-") + "/{id}",
                   defaults: new { controller = "Home", action = "QuestionList  ", id = UrlParameter.Optional }
               );
            }
            routes.MapRoute(
                   name: "page",
                   url: "cart",
                   defaults: new { controller = "Home", action = "Cart", id = UrlParameter.Optional }
               );

            routes.MapRoute(
                 name: "pricing",
                 url: "answer-pricing",
                 defaults: new { controller = "Home", action = "Package", id = UrlParameter.Optional }
             );

            routes.MapRoute(
                name: "Singlepricing",
                url: "single-pricing",
                defaults: new { controller = "Home", action = "SingleAnswer", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "priv",
                url: "privacy-policy",
                defaults: new { controller = "Home", action = "Privacy", id = UrlParameter.Optional }
            );


            routes.MapRoute(
                name: "terms",
                url: "terms-condition",
                defaults: new { controller = "Home", action = "Terms", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                  name: "newB",
                  url: "blog",
                  defaults: new { controller = "BlogDataNew", action = "Index", id = UrlParameter.Optional }
              );

            routes.MapRoute(
                  name: "cart",
                  url: "del_cart/{id}",
                  defaults: new { controller = "Home", action = "DelCart", id = UrlParameter.Optional }
              );

            routes.MapRoute(
                  name: "reg",
                  url: "register/{id}",
                  defaults: new { controller = "Register", action = "RegisterUs", id = UrlParameter.Optional }
              );

            routes.MapRoute(
                name: "fg",
                url: "forget-password/{id}",
                defaults: new { controller = "Register", action = "ForgetPassword", id = UrlParameter.Optional }
            );
            routes.MapRoute(
             name: "rp",
             url: "reset-password/{id}",
             defaults: new { controller = "Register", action = "ResetPassword", id = UrlParameter.Optional }
         );


            routes.MapRoute(
                 name: "log",
                 url: "logout/{id}",
                 defaults: new { controller = "Register", action = "Logout", id = UrlParameter.Optional }
             );
            routes.MapRoute(
               name: "logs",
               url: "login/{id}",
               defaults: new { controller = "Register", action = "Login", id = UrlParameter.Optional }
           );
          //  routes.MapRoute(
          //    name: "pay",
          //    url: "payment/{id}",
          //    defaults: new { controller = "Home", action = "SaveAddress", id = UrlParameter.Optional }
          //);
            routes.MapRoute(
             name: "suc",
             url: "success/{token}",
             defaults: new { controller = "Other", action = "Success", token = UrlParameter.Optional }
         );
            routes.MapRoute(
             name: "fai",
             url: "failed/{token}",
             defaults: new { controller = "Other", action = "Failed", token = UrlParameter.Optional }
         );

            routes.MapRoute(
             name: "p4",
             url: "404",
             defaults: new { controller = "Other", action = "Page404", token = UrlParameter.Optional }
         );

            routes.MapRoute(
             name: "suc1",
             url: "question-success",
             defaults: new { controller = "Home", action = "QuestSuccess", token = UrlParameter.Optional }
         );

            routes.MapRoute(
          name: "ps",
          url: "payment-success",
          defaults: new { controller = "Home", action = "PaymentSuccess", token = UrlParameter.Optional }
      );

            routes.MapRoute(
          name: "pf",
          url: "payment-failure",
          defaults: new { controller = "Home", action = "PaymentFailure", token = UrlParameter.Optional }
      );
            routes.MapRoute(
          name: "ques",
          url: "homework-help",
          defaults: new { controller = "Home", action = "QuestionPage", token = UrlParameter.Optional }
      );

            routes.MapRoute(
             name: "p5",
             url: "500",
             defaults: new { controller = "Other", action = "Page500", token = UrlParameter.Optional }
         );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );


        }
    }
    public class CmsUrlConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            var db = new XpertWebEntities();
            if (values[parameterName] != null)
            {
                var permalink = values[parameterName].ToString();
                var newLink = permalink.Replace("solution/", "");
                return db.tblSolutionLibraries.Any(p => p.UrlText == newLink);
            }
            return false;
        }
    }
    public class CmsUrlConstraint2 : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            var db = new XpertWebEntities();
            if (values[parameterName] != null)
            {
                var permalink = values[parameterName].ToString();
                //var newLink = permalink.Replace("solution/", "");
                return db.tblBlogLibs.Any(p => p.VirtualUrl == permalink);
            }
            return false;
        }
    }
    public class CmsUrlConstraint21 : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            var db = new XpertWebEntities();
            if (values[parameterName] != null)
            {
                var permalink = values[parameterName].ToString();
                //var newLink = permalink.Replace("solution/", "");
                return db.tblBlogCats.Any(p => p.Virtual_Url == permalink);
            }
            return false;
        }
    }
}
