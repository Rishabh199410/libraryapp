﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace LibraryApp
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundle/js").Include(
                        "~/js/jquery.datetimepicker.full.min.js",
                        "~/js/jquery-ui.min.js",
                        "~/js/bootstrap.min.js",
                        "~/js/jquery-plugin-collection.js",
                        "~/js/jquery.tabSlideOut.v1.3.js",
                        "~/js/MyMain.js",
                        "~/js/custom.js"));

            bundles.Add(new StyleBundle("~/bundle/mcss").Include(
                "~/css/font-awesome.min.css",
                     "~/css/bootstrap.min.css",
                     //"~/css/newMenu.css",
                    // "~/css/pe-icon-7-stroke.css",
                     "~/css/jquery-ui.min.css",
                     //"~/css/utility-classes.css",
                    // "~/css/css-plugin-collections.css",
                     //"~/css/menuzord-skins/menuzord-rounded-boxed.css",
                     "~/css/style-main.css"
                    // "~/css/custom-bootstrap-margin-padding.css",
                    // "~/css/responsive.css",
                   //  "~/css/colors/theme-skin-color-set-1.css",
                    // "~/css/jquery.datetimepicker.css"
                    ));

            BundleTable.EnableOptimizations = true;

        }
    }
}