﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LibraryDataLayer;
using LibraryBILayer.Admin;

namespace LibraryApp
{
    [RoutePrefix("api/app")]
    public class ValuesController : ApiController
    {
        ManageMainSubject objManage = new ManageMainSubject();
        // GET api/<controller>      

        [HttpGet]
        [Route("universityDetails")]
        public List<tblUniversity> BindUniversityDetails()
        {


            List<tblUniversity> universityDetails = new List<tblUniversity>();
            try
            {
                universityDetails = objManage.BindUniversity();
            }
            catch (ApplicationException ex)
            {
                throw new HttpResponseException(new HttpResponseMessage { StatusCode = HttpStatusCode.BadRequest, ReasonPhrase = ex.Message });
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage { StatusCode = HttpStatusCode.BadGateway, ReasonPhrase = ex.Message });
            }

            return universityDetails;
        }

        [HttpGet]
        [Route("categoryDetails")]
        public List<tblSolutionSubject> BindCategoryDetails(int UID)
        {

            List<tblSolutionSubject> categoryDetails = new List<tblSolutionSubject>();
            try
            {
                categoryDetails = objManage.BindCategory(UID).GroupBy(c=>c.Subject).Select(c=>c.First()).ToList();
            }
            catch (ApplicationException ex)
            {
                throw new HttpResponseException(new HttpResponseMessage { StatusCode = HttpStatusCode.BadRequest, ReasonPhrase = ex.Message });
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage { StatusCode = HttpStatusCode.BadGateway, ReasonPhrase = ex.Message });
            }

            return categoryDetails;
        }

        [HttpGet]
        [Route("subjectDetails")]
        public List<tblMainSubject> BindSubjectDetails(int SolSubID, int UID)
        {
            List<tblMainSubject> subjectDetails = new List<tblMainSubject>();
            try
            {
                subjectDetails = objManage.BindSubject(SolSubID,UID);
            }
            catch (ApplicationException ex)
            {
                throw new HttpResponseException(new HttpResponseMessage { StatusCode = HttpStatusCode.BadRequest, ReasonPhrase = ex.Message });
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage { StatusCode = HttpStatusCode.BadGateway, ReasonPhrase = ex.Message });
            }

            return subjectDetails;
        }
        [HttpGet]
        [Route("subjectDetailsByName")]
        public List<tblMainSubject> BindSubjectDetailsByCode(int code)
        {
            List<tblMainSubject> subjectDetails = new List<tblMainSubject>();
            try
            {
                subjectDetails = objManage.BindMainSubjectByCode(code);
            }
            catch (ApplicationException ex)
            {
                throw new HttpResponseException(new HttpResponseMessage { StatusCode = HttpStatusCode.BadRequest, ReasonPhrase = ex.Message });
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage { StatusCode = HttpStatusCode.BadGateway, ReasonPhrase = ex.Message });
            }

            return subjectDetails;
        }
        [HttpGet]
        [Route("subjectDetailsByNameCountry")]
        public List<tblMainSubject> BindSubjectDetailsByCountry(string name)
        {
            List<tblMainSubject> subjectDetails = new List<tblMainSubject>();
            try
            {
                subjectDetails = objManage.BindMainSubjectByName(name);
            }
            catch (ApplicationException ex)
            {
                throw new HttpResponseException(new HttpResponseMessage { StatusCode = HttpStatusCode.BadRequest, ReasonPhrase = ex.Message });
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage { StatusCode = HttpStatusCode.BadGateway, ReasonPhrase = ex.Message });
            }

            return subjectDetails;
        }
    }
}