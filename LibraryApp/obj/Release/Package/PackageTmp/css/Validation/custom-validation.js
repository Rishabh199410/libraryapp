﻿function Validate(divContainerID) {   
    var labelText = "";
    var errInput = $(divContainerID).find(".errBox");
    errInput.removeClass("errBox");
    var errLabel = $(divContainerID).find("span.error");
    $(errLabel).remove();
    var hasError = false;

    //===========Required Field Validation==============
    var validate = divContainerID + " .requiredField";
    $(validate).each(function () {
        if (jQuery.trim($(this).val()) == '') {
            labelText = $(this).attr('title');
            $(this).parent().append('<span class="error errMessage">' + labelText + ' is Mandatory.</span>');
            $(this).addClass("errBox");           
            hasError = true;
        }
    });

    //===========Check Password Policy Field Validator==============
    var validatePassPolicy = divContainerID + " .passValidateField";
    $(validatePassPolicy).each(function () {
        if ($(this).val() != '') {
            var pwdRegex = /(?=.*[!@#$&*])(?=.*[0-9]).{8}/;//(at least 1 special,1 number,minimum 8)
            if (!pwdRegex.test(jQuery.trim($(this).val()))) {
                labelText = $(this).attr('title');
                $(this).parent().append("<span class='error errMessage'>Invalid " + labelText + ". Use at least one number and one special character with minimum 8 character.</span>");
                $(this).addClass("errBox");   
                hasError = true;
            }
        }
    });

     //===========Compare Field Validator==============
    var validatePass = divContainerID + " .compareTextField";
    var firstValue = $("#Password").val();
    $(validatePass).each(function () {
        if ($(this).val() != firstValue && $(this).val() != '') {
            labelText = $(this).attr('title');
            $(this).parent().append('<span class="error errMessage">' + labelText + ' does not match the field above.</span>');
            $(this).addClass("errBox");          
            hasError = true;
        }
    });

    var validatePass = divContainerID + " .compareTextField1";
    var firstValue = $("#NewPassword").val();
    $(validatePass).each(function () {
        if ($(this).val() != firstValue && $(this).val() != '') {
            labelText = $(this).attr('title');
            $(this).parent().append('<span class="error errMessage">' + labelText + ' does not match the field above.</span>');
            $(this).addClass("errBox");
            hasError = true;
        }
    });

    //===========Check Email Field Validator==============
    var validatePassPolicy = divContainerID + " .emailField";
    $(validatePassPolicy).each(function () {
        if ($(this).val() != '') {
            var pwdRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            if (!pwdRegex.test(jQuery.trim($(this).val()))) {
                labelText = $(this).attr('title');
                $(this).parent().append("<span class='error errMessage'>Invalid " + labelText + ".</span>");
                $(this).addClass("errBox");
                hasError = true;
            }
        }
    });

    //===========Validate Only Decimal Values===================
    var validateDecimalValue = divContainerID + " .decimalValue";
    $(validateDecimalValue).each(function () {
        if ($(this).val() != '') {
            var decimalValueReg = /^\d+(\.\d{1,2})?$/;
            if (!decimalValueReg.test(jQuery.trim($(this).val()))) {
                labelText = $(this).attr('title');
                $(this).parent().append('<span class="error errorMessage">Invalid ' + labelText + '.(Only numbers are allowed.)</span>');
                hasError = true;
            }
        }
    });

    

    return hasError;
}