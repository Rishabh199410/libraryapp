﻿




//------------------------ time zone start ----------------------//


function calculate_time_zone() {

    /*
    var minutes;
    var rightNow = new Date();
    var jan1 = new Date(rightNow.getFullYear(), 0, 1, 0, 0, 0, 0);  // jan 1st
    var june1 = new Date(rightNow.getFullYear(), 6, 1, 0, 0, 0, 0); // june 1st
    var temp = jan1.toGMTString();
    var jan2 = new Date(temp.substring(0, temp.lastIndexOf(" ") - 1));
    temp = june1.toGMTString();
    var june2 = new Date(temp.substring(0, temp.lastIndexOf(" ") - 1));
    var std_time_offset = (jan1 - jan2) / (1000 * 60 * 60);
    var daylight_time_offset = (june1 - june2) / (1000 * 60 * 60);
    var dst;
    if (std_time_offset == daylight_time_offset) {
        dst = "0"; // daylight savings time is NOT observed
    } else {
        // positive is southern, negative is northern hemisphere
        var hemisphere = std_time_offset - daylight_time_offset;
        if (hemisphere >= 0)
            std_time_offset = daylight_time_offset;
        dst = "1"; // daylight savings time is observed
    }
    var i;
    debugger
    // Here set the value of hidden field to the ClientTimeZone.
    minutes = convert(std_time_offset);
    // Setting TimeZone to cookie
    document.cookie = "TimeZone=TimeZone=" + minutes + ";path=/";
    return minutes;

    */

    ///*
    var offset = new Date().getTimezoneOffset();
    // use opposite sign
    var sign = offset > 0 ? "-" : "+";
    var hrs = parseInt(offset / 60);
    var mins = parseInt(offset - (hrs * 60));
    hrs = hrs < 0 ? (-1) * hrs : hrs;
    mins = mins < 0 ? (-1) * mins : mins;
    var zone = sign + (hrs >= 10 ? hrs : "0" + hrs) + ":" + (mins >= 10 ? mins : "0" + mins)
    document.cookie = "TimeZone=TimeZone=" + zone + ";path=/";
    return zone;

    // */
}
// This function is to convert the timezoneoffset to Standard format
function convert(value) {
    var hours = parseInt(value);
    value -= parseInt(value);
    value *= 60;
    var mins = parseInt(value);
    value -= parseInt(value);
    value *= 60;
    var secs = parseInt(value);
    var display_hours = hours;
    // handle GMT case (00:00)
    if (hours == 0) {
        display_hours = "00";
    } else if (hours > 0) {
        // add a plus sign and perhaps an extra 0
        display_hours = (hours < 10) ? "+0" + hours : "+" + hours;
    } else {
        // add an extra 0 if needed
        display_hours = (hours > -10) ? "-0" + Math.abs(hours) : hours;
    }
    mins = (mins < 10) ? "0" + mins : mins;
    return display_hours + ":" + mins;
}
// Adding the funtion to onload event of document object
onload = calculate_time_zone;


//---------------------- trimezone end ------------------------//





function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
//=========================Gaurav Arya===========================//
function isHTML(str) {
    //return /<[a-z][\s\S]*>/i.test(str) && !!$(str)[0];
    return (/<[a-z][\s\S]*>/i.test(str) || /<\s*\/\s*\w\s*.*?>|<\s*br\s*>/g.test(str));
}
//====================================================================//