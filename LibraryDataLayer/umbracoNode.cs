//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LibraryDataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class umbracoNode
    {
        public int id { get; set; }
        public bool trashed { get; set; }
        public int parentID { get; set; }
        public Nullable<int> nodeUser { get; set; }
        public int level { get; set; }
        public string path { get; set; }
        public int sortOrder { get; set; }
        public System.Guid uniqueID { get; set; }
        public string text { get; set; }
        public Nullable<System.Guid> nodeObjectType { get; set; }
        public System.DateTime createDate { get; set; }
    }
}
