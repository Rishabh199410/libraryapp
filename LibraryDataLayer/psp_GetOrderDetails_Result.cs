//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LibraryDataLayer
{
    using System;
    
    public partial class psp_GetOrderDetails_Result
    {
        public int id { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string OrderID { get; set; }
        public Nullable<int> SolCartID { get; set; }
        public string memberid { get; set; }
        public string PaymentID { get; set; }
        public string PayerID { get; set; }
        public string CurType { get; set; }
        public Nullable<int> qty { get; set; }
        public Nullable<int> TotalAmount { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsAnswerActive { get; set; }
        public string token { get; set; }
        public Nullable<System.DateTime> paymentDate { get; set; }
        public string status { get; set; }
        public string download { get; set; }
    }
}
