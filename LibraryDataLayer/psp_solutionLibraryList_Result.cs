//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LibraryDataLayer
{
    using System;
    
    public partial class psp_solutionLibraryList_Result
    {
        public int Sol_ID { get; set; }
        public string Subject { get; set; }
        public Nullable<double> Amount { get; set; }
        public Nullable<bool> Status { get; set; }
        public string Title { get; set; }
        public Nullable<int> SolSubID { get; set; }
    }
}
