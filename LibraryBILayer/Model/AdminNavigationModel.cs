﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryBILayer.Model
{
    public class AdminNavigationModel
    {
        public int AdminNavigationID { get; set; }
        public string AdminNavigation { get; set; }
        public string AreaName { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public int? StatusID { get; set; }
        public bool? IsParent { get; set; }
        public int? ParentID { get; set; }
        public string Glyphicon { get; set; }
    }
}
