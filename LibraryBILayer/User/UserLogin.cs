﻿using LibraryDataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryBILayer.User
{
    public class UserLogin
    {
        public tblCollegeStudent UserLoginCheck(string email, string password)
        {
            tblCollegeStudent objUser = null;
            using (var ContextDB = new XpertWebEntities())
            {
                var details = ContextDB.tblCollegeStudents.Where(user => user.Email.Trim().Equals(email) && user.Password.Equals(password)).FirstOrDefault();
                objUser = details;
            }
            return objUser;
        }
    }
}
