﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryDataLayer;

namespace LibraryBILayer.User
{
    public class StudentProfile
    {
        public tblCollegeStudent getStudentById(int Id)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblCollegeStudents.Where(c => c.id == Id).FirstOrDefault());
            }
        }

        public tblCollegeStudent UserProfile(string emailid)
        {
            tblCollegeStudent objUser = null;
            using (var ContextDB = new XpertWebEntities())
            {
                var details = ContextDB.tblCollegeStudents.FirstOrDefault(user => user.Email == emailid);
                objUser = details;
            }
            return objUser;
        }
        public int UpdateProfile(tblCollegeStudent collegeStudent)
        {
            int result = 0;
            using (var context = new XpertWebEntities())
            {
                var details = context.tblCollegeStudents.Single(c => c.Email == collegeStudent.Email);
                //tblCollegeStudent tblCollege = new tblCollegeStudent();
                details.Name = collegeStudent.Name;
                details.Email = collegeStudent.Email;
                details.PhoneNo = collegeStudent.PhoneNo;
                details.Password = collegeStudent.Password;
                details.country = collegeStudent.country;

                result = context.SaveChanges();
                return result;
            }
        }

    }
}
