﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryDataLayer;
using LibraryBILayer.Comman;
using LibraryUtility;
using static LibraryUtility.Enums;

namespace LibraryBILayer.User
{
    public class PendingAssignments
    {
        public List<JoinClassForPCR> getPendingAssignment(string studId)
        {
            using (var context = new XpertWebEntities())
            {
                //List<tblQuestion> Question = context.tblQuestions.ToList();
                //List<tblquestiondetail> QuestionDetails = context.tblquestiondetails.ToList();
                //List<mstservicetype> ServiceType = context.mstservicetypes.ToList();
                var result = from q in context.tblQuestions
                             join qd in context.tblquestiondetails 
                             on q.Id equals qd.QuestionID into qqd
                             from ques in qqd.DefaultIfEmpty()
                             join st in context.mstservicetypes
                             on q.ServiceTypeID equals st.ServiceTypeID into qst
                             from quess in qst.DefaultIfEmpty()
                             where q.StudID == studId
                             select new JoinClassForPCR
                             {
                                 id = q.Id,
                                 Price = ques.Price,
                                 Question = q.Question,
                                 Subject = q.Subject,
                                 //PaymentStatusId = Enum.GetName(typeof(PaymentStatus), q.PaymentStatus),
                                 PaymentStatusId = q.PaymentStatus,
                                 AssignmentStatusID = q.StatusID,
                                 Totalwords = ques.TotalWords,
                                 ServiceType = quess.ServiceType ?? string.Empty,
                                 CreatedOn = q.CreatedOn
                             };
                
                return result.OrderByDescending(x=> x.CreatedOn).ToList();
            }
        }

        public JoinClassForPCR getPendingAssignmentbyId(string Studid,int Id)
        {
            using (var context = new XpertWebEntities())
            {
                //List<tblQuestion> Question = context.tblQuestions.ToList();
                //List<tblquestiondetail> QuestionDetails = context.tblquestiondetails.ToList();
                //List<mstservicetype> ServiceType = context.mstservicetypes.ToList();
                var result = from q in context.tblQuestions
                             join qd in context.tblquestiondetails
                             on q.Id equals qd.QuestionID into qqd
                             from ques in qqd.DefaultIfEmpty()
                             join st in context.mstservicetypes
                             on q.ServiceTypeID equals st.ServiceTypeID into qst
                             from quess in qst.DefaultIfEmpty()
                             where q.StudID == Studid && q.StatusID == (int)Enums.StatusID.Pending && q.Id == Id
                             select new JoinClassForPCR
                             {
                                 id = q.Id,
                                 Price = ques.Price,
                                 Question = q.Question,
                                 Subject = q.Subject,
                                 Totalwords = ques.TotalWords,
                                 ServiceType = quess.ServiceType ?? string.Empty
                             };

                return result.Single();
            }
        }

    }
}
