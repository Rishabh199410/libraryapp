﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryBILayer.Comman
{
    public static class PayPalMaster
    {
        public static string getItemNameAndCost(string itemName, string itemCost, string currency, string mode, int qty, int orderid)
        {

            string returnURL = "";
            if (mode == "live")
            {
                decimal price = Convert.ToDecimal(itemCost);

                returnURL += "https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=cc.eduwebexperts@gmail.com&currency_code=" + currency + "&tax=" + CalculateTax(itemCost, "0");

                returnURL += "&item_name=" + itemName;
                returnURL += "&amount=" + price;
                returnURL += "&cancel_return=https://www.classmaster.com.au/payment-cancel" + "?c_id=" + orderid;
                returnURL += "&return=https://www.classmaster.com.au/payment-success" + "?c_id=" + orderid;
            }

            return returnURL;

        }
        public static string CalculateTax(string Amount, string Discount)
        {
            Amount = Amount.Replace("$", "");
            Discount = Discount.Replace("$", "");
            decimal Amt = Convert.ToDecimal(Amount) - Convert.ToDecimal(Discount);
            decimal TaxRate = ((Amt + Convert.ToDecimal(0.60)) / Convert.ToDecimal(0.91451)) - Amt;
            TaxRate = Decimal.Round(TaxRate, 2);

            return TaxRate.ToString();
        }
    }
}
