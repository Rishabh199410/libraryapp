﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryDataLayer;
namespace LibraryBILayer.Comman
{
    public class GetRegisterUs
    {
        public static int checkOrAddMember(tblCollegeStudent obj)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblCollegeStudents.Where(c => c.Email == obj.Email).Select(c => c).FirstOrDefault();
                if (data != null)
                {
                    i = -1;
                }
                else
                {
                    context.tblCollegeStudents.Add(obj);
                    i = context.SaveChanges();
                }
            }
            return i;
        }
        public tblCollegeStudent UserLoginCheck2(string email, string password)
        {
            tblCollegeStudent objUser = null;
            using (var ContextDB = new XpertWebEntities())
            {
                var details = ContextDB.tblCollegeStudents.Where(user => user.Email.Trim().Equals(email) && user.Password.Equals(password) && user.IsActive == false).FirstOrDefault();
                objUser = details;
            }
            return objUser;
        }
        public static tblCollegeStudent StudentCheck(string email)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblCollegeStudents.Where(c => c.Email == email).Select(c => c).FirstOrDefault());
            }
        }

        public static int InsertPasswordLink(forgetpasswordlink model, string email)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblCollegeStudents.Where(c => c.Email == email).FirstOrDefault();
                if (data == null)
                {
                    i = -1;
                }
                else
                {
                    var list = context.forgetpasswordlinks.Where(x => x.id == data.id).ToList();
                    if (list.Count > 0)
                    {
                        foreach (var item in list)
                        {
                            context.forgetpasswordlinks.Remove(item);
                        }
                    }

                    model.id = data.id;
                    model.CreatedOn = DateTime.UtcNow;
                    context.forgetpasswordlinks.Add(model);
                    i = context.SaveChanges();
                }
            }
            return i;
        }

        public static forgetpasswordlink ForgetPasswordLinkCheckCheck(string Link)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.forgetpasswordlinks.Where(c => c.Link.ToString() == Link.ToString()).FirstOrDefault());
            }
        }

        public static int ResetPassword(string password, string Link)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.forgetpasswordlinks.Where(c => c.Link.ToString() == Link.ToString()).FirstOrDefault();
                var student = context.tblCollegeStudents.Where(c => c.id == data.id).FirstOrDefault();

                student.Password = password;
                i = context.SaveChanges();
            }
            return i;
        }
    }
}
