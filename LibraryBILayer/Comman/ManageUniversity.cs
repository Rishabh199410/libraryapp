﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryDataLayer;
namespace LibraryBILayer.Comman
{
    public class ManageUniversity
    {
        public static int AddOrUpdateUniversity(tblUniversity obj)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var IsData = context.tblUniversities.Where(c => c.University == obj.University).Select(c => c).FirstOrDefault();
                if (IsData != null)
                {
                    if(obj.UID!=0)
                    {
                        var data = context.tblUniversities.Single(c => c.UID == obj.UID);
                        data.University = obj.University;
                        data.UpdatedBy = obj.UpdatedBy;
                        data.UpdatedOn = DateTime.Now;
                        data.URL = obj.URL;
                        data.country = obj.country;
                        data.Logo = obj.Logo;
                        data.shortName = obj.shortName;
                        data.Note = obj.Note;
                        data.description = obj.description;
                        i = context.SaveChanges();
                    }
                }
                else
                {
                    if (obj.UID != 0)
                    {
                        var data = context.tblUniversities.Single(c => c.UID == obj.UID);
                        data.University = obj.University;
                        data.UpdatedBy = obj.UpdatedBy;
                        data.UpdatedOn = DateTime.Now;
                        data.URL = obj.URL;
                        i = context.SaveChanges();
                    }
                    else
                    {
                        context.tblUniversities.Add(obj);
                        i = context.SaveChanges();
                    }
                }                
            }
            return i;
        }
        public static List<tblUniversity> ObjUniversity(bool status)
        {
            List<tblUniversity> myList = new List<tblUniversity>();
            using (var context = new XpertWebEntities())
            {
                myList = context.tblUniversities.Where(c => c.IsActive == status).Select(c => c).AsEnumerable().ToList();
            }
            return myList;
        }
        public static int DeleteUniversityData(int ID, bool status, string email)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblUniversities.Single(c=>c.UID==ID);
                data.IsActive = status;
                data.UpdatedBy = email;
                data.UpdatedOn = DateTime.Now;
                i = context.SaveChanges();
            }
            return i;
        }
        public static int FinalDeleteUniversityData(int ID, bool status, string email)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblUniversities.Single(c => c.UID == ID);
                context.tblUniversities.Remove(data);
                i = context.SaveChanges();
            }
            return i;
        }
        public static tblUniversity getUniversity(int id)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblUniversities.Where(c => c.UID == id).Select(c => c).FirstOrDefault());
            }
        }
    }
}
