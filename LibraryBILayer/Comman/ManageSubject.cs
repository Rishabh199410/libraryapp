﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryDataLayer;

namespace LibraryBILayer.Comman
{
    public class ManageSubject
    {
        public static int AddOrUpdateSubject(tblSolutionSubject obj)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var IsData = context.tblSolutionSubjects.Where(c => c.Subject == obj.Subject).Select(c => c).FirstOrDefault();
                if (IsData != null)
                {
                    i = -1;
                }
                else
                {
                    if (obj.id != 0)
                    {
                        var data = context.tblSolutionSubjects.Single(c => c.id == obj.id);
                        data.Subject = obj.Subject;
                        data.UpdatedBy = obj.UpdatedBy;
                        data.UpdatedOn = DateTime.Now;
                        data.Url = obj.Url;
                        i = context.SaveChanges();
                    }
                    else
                    {
                        context.tblSolutionSubjects.Add(obj);
                        i = context.SaveChanges();
                    }
                }
            }
            return i;
        }
        public static List<tblSolutionSubject> ObjSubjects(bool status)
        {
            List<tblSolutionSubject> myList = new List<tblSolutionSubject>();
            using (var context = new XpertWebEntities())
            {
                myList = context.tblSolutionSubjects.Where(c => c.IsActive == status).OrderByDescending(c=>c.id).Select(c => c).AsEnumerable().ToList();
            }
            return myList;
        }
        public static int DeleteSubjectData(int ID, bool status, string email)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblSolutionSubjects.Single(c => c.id == ID);
                data.IsActive = status;
                data.UpdatedBy = email;
                data.UpdatedOn = DateTime.Now;
                i = context.SaveChanges();
            }
            return i;
        }
        public static int FinalDeleteSubjectData(int ID, bool status, string email)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblSolutionSubjects.Single(c => c.id == ID);
                context.tblSolutionSubjects.Remove(data);
                i = context.SaveChanges();
            }
            return i;
        }
        public static tblSolutionSubject getSubject(int id)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblSolutionSubjects.Where(c => c.id == id).Select(c => c).FirstOrDefault());
            }
        }
    }
}
