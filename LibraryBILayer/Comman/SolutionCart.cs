﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryBILayer.Comman
{
    public class SolutionCart
    {
        public int? id { get; set; }
        public string subject { get; set; }
        public int? solution_id { get; set; }
        public string desc { get; set; }
        public decimal? price { get; set; }
        public int? qty { get; set; }
        public string solcartid { get; set; }
    }
}
