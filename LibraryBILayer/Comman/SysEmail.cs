﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace LibraryBILayer.Comman
{
    public class SysEmail
    {
        #region SysEmail property
        public string SMTPPassword { get; set; }
        public string SMTPUserName { get; set; }
        public string SMTPServer { get; set; }

        public int SMTPport { get; set; }
        public string Subject { get; set; }
        public string CCMailList { get; set; }
        public string BCCMailList { get; set; }
        public ArrayList AttachedDocumentNname { get; set; }
        public ArrayList AttachedDocumentPath { get; set; }
        public string MailFrom { get; set; }
        public string MailTo { get; set; }
        public ArrayList MailTo1 { get; set; }
        public string MailBODY { get; set; }
        public string UserName { get; set; }

        #endregion

        public void sendmail2(string tomail, string subject, string bodyText)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["SMTPADDRESS"] != null)
                SMTPServer = System.Configuration.ConfigurationManager.AppSettings["SMTPADDRESS"].ToString();

            if (System.Configuration.ConfigurationManager.AppSettings["SMTPUSERNAME"] != null)
                SMTPUserName = System.Configuration.ConfigurationManager.AppSettings["SMTPUSERNAME"].ToString();

            if (System.Configuration.ConfigurationManager.AppSettings["SMTPPASSWORD"] != null)
                SMTPPassword = System.Configuration.ConfigurationManager.AppSettings["SMTPPASSWORD"].ToString();

            if (System.Configuration.ConfigurationManager.AppSettings["SMTPPORT"] != null)
                SMTPport = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SMTPPORT"]);         

            MailMessage msgMail = new MailMessage();

            MailMessage myMessage = new MailMessage();
            myMessage.From = new MailAddress(SMTPUserName, "classmaster.com.au");
            myMessage.To.Add(tomail);
            myMessage.Subject = subject;
            myMessage.IsBodyHtml = true;

            myMessage.Body = bodyText;


            SmtpClient mySmtpClient = new SmtpClient();
            System.Net.NetworkCredential myCredential = new System.Net.NetworkCredential(SMTPUserName, SMTPPassword);
            mySmtpClient.Host = SMTPServer;
            mySmtpClient.UseDefaultCredentials = false;
            mySmtpClient.Credentials = myCredential;
            mySmtpClient.ServicePoint.MaxIdleTime = 1;

            mySmtpClient.Send(myMessage);
            myMessage.Dispose();
        }
    }
}
