﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryDataLayer;
using LibraryUtility;

namespace LibraryBILayer.Comman
{
    public class JoinClassForPCR
    {
        public int id { get; set; }
        public string Question { get; set; }
        public Nullable<decimal> Price { get; set; }
        public int? PaymentStatusId { get; set; }
        public int? AssignmentStatusID { get; set; }
        public string Subject { get; set; }
        public Nullable<int> Totalwords { get; set; }
        public string ServiceType { get; set; }
        public DateTime? CreatedOn { get; set; }

    }
}
