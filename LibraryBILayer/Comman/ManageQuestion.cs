﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryDataLayer;
using LibraryUtility;

namespace LibraryBILayer.Comman
{
    public class ManageQuestion
    {
        public static int AddQuestion(tblQuestion tblQuestion)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                context.tblQuestions.Add(tblQuestion);
                i = context.SaveChanges();
            }
            return i;
        }

        public static int AddQuestionDetail(tblquestiondetail tblQuestion)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                context.tblquestiondetails.Add(tblQuestion);
                i = context.SaveChanges();
            }
            return i;
        }

        public static void UpdateMemberQuestion(string quesid, string memberid)
        {
            using (var context = new XpertWebEntities())
            {
                var data = context.tblQuestions.Single(c => c.QuestionId == quesid);
                data.StudID = memberid;
                context.SaveChanges();
            }
        }

        public static void UpdateSessionQuestion(string SessionID, string StudentID)
        {
            using (var context = new XpertWebEntities())
            {
                var data = context.tblQuestions.Where(x => x.SessionID.ToLower() == SessionID.ToLower()).ToList();
                if (data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        item.StudID = StudentID;
                    }
                    context.SaveChanges();
                }              
               
            }
        }

        public static List<mstservice> GetCDRServiceList()
        {
            using (var context = new XpertWebEntities())
            {
                return (context.mstservices.Where(x => x.ServiceTypeID == (Int32)Enums.ServiceTypeID.CDRServices).OrderBy(c => c.ServiceType).AsEnumerable().ToList());
            }
        }

        public static List<msttimeslot> GetCDRTimeList()
        {
            using (var context = new XpertWebEntities())
            {
                return (context.msttimeslots.Where(x => x.ServiceTypeID == (Int32)Enums.ServiceTypeID.CDRServices).OrderBy(c => c.TimeSlot).AsEnumerable().ToList());
            }
        }

        public static decimal? GetCDRServicePrice(int ServiceID, int TimeSlotID)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.mstcdrprices.Where(x => x.ServiceID == ServiceID && x.TimeSlotID == TimeSlotID).FirstOrDefault().CDRPrice);
            }
        }

        public static decimal? GetPlagPrice(int TotalWords)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.mstplagiarismprices.Where(x =>x.StartingWord<= TotalWords && x.EndingWord > TotalWords).FirstOrDefault().PlagPrice);
            }
        }

        public static List<msttimeslot> GetPRTimeList()
        {
            using (var context = new XpertWebEntities())
            {
                return (context.msttimeslots.Where(x => x.ServiceTypeID == (Int32)Enums.ServiceTypeID.ProofReading).OrderBy(c => c.TimeSlot).AsEnumerable().ToList());
            }
        }
        public static List<msteducationlevel> GetEducationList()
        {
            using (var context = new XpertWebEntities())
            {
                return (context.msteducationlevels.OrderBy(c => c.EducationLevel).AsEnumerable().ToList());
            }
        }

        public static List<mstservice> GetPRServiceList()
        {
            using (var context = new XpertWebEntities())
            {
                return (context.mstservices.Where(x => x.ServiceTypeID == (Int32)Enums.ServiceTypeID.ProofReading).OrderBy(c => c.ServiceType).AsEnumerable().ToList());
            }
        }

    }
}
