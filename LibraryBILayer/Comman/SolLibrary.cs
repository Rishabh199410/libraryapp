﻿using LibraryBILayer.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryDataLayer;
using LibraryUtility;

namespace LibraryBILayer.Comman
{
    public static class SolLibrary
    {
        public static List<BindSubject> GetSubjectList()
        {
            using (var context = new XpertWebEntities())
            {
                var data = (from c in context.tblSolutionSubjects
                            select new BindSubject
                            {
                                SolSubID = c.SolSubID ?? Int32.MinValue,
                                Subject = c.Subject
                            }).ToList();
                return data;
            }
        }
        public static List<BindSubject> GetSubjectListURL()
        {
            using (var context = new XpertWebEntities())
            {
                var data = (from c in context.psp_tblSolutionSubjects_Url()
                            select new BindSubject
                            {
                                SolSubID = c.SolSubID ?? Int32.MinValue,
                                Subject = c.Subject
                            }).ToList();
                return data;
            }
        }
        public static List<SolutionLibr> GetSolutionLibrary()
        {

            using (var context = new XpertWebEntities())
            {

                return (from c in context.tblSolutionLibraries
                        join s in context.tblSolutionSubjects on c.SolSubID equals s.SolSubID
                        where c.Status == true
                        orderby c.Sol_ID descending
                        select new SolutionLibr
                        {
                            Subject = s.Subject,
                            Sol_ID = c.Sol_ID,
                            Amount = c.Amount,
                            SolSubID = c.SolSubID,
                            UrlText = c.UrlText,
                            Title = c.Title,
                            SolContent = c.SolContent,
                            UrlID = c.UrlID,
                            AnswerPreview = c.AnswerPreview
                        }).ToList();
            }
        }
        public static List<SolutionLibr> SearchSolutionLibrary(string str)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblSolutionLibraries
                        join s in context.tblSolutionSubjects on c.SolSubID equals s.SolSubID
                        where c.SolContent.Contains(str) && c.Status == true
                        select new SolutionLibr
                        {
                            Subject = s.Subject,
                            Sol_ID = c.Sol_ID,
                            Amount = c.Amount,
                            SolSubID = c.SolSubID,
                            UrlText = c.UrlText,
                            Title = c.Title,
                            SolContent = c.SolContent,
                            UrlID = c.UrlID
                        }).ToList();
            }
        }
        public static List<SolutionLibr> GetSolutionLibrID(string url)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblSolutionLibraries
                        join s in context.tblSolutionSubjects on c.SolSubID equals s.SolSubID
                        join p in context.tblMainSubjects on s.SolSubID equals p.SolSubID
                        where c.Status == true && c.UrlText==url
                        select new SolutionLibr
                        {
                            Subject = s.Subject,
                            Sol_ID = c.Sol_ID,
                            Amount = c.Amount,
                            SolSubID = c.SolSubID,
                            Title = c.Title,
                            Tags=c.Tags,
                            Description=c.Description,
                            SolContent = c.SolContent,
                            UrlText = c.UrlText,
                            SubjectCode=p.SubjectCode,
                            AnswerPreview = c.AnswerPreview,
                            Question_File_ID=c.Question_File_ID
                        }).ToList();
            }
        }
        public static List<SolutionLibr> GetSolutionLibrIDMain(int solid)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblSolutionLibraries
                        join s in context.tblSolutionSubjects on c.SolSubID equals s.SolSubID
                        where c.Status == true && c.Sol_ID == solid
                        select new SolutionLibr
                        {
                            Subject = s.Subject,
                            Sol_ID = c.Sol_ID,
                            Amount = c.Amount,
                            SolSubID = c.SolSubID,
                            Title = c.Title,
                            SolContent = c.SolContent,
                            UrlText = c.UrlText,
                            AnswerPreview = c.AnswerPreview
                        }).ToList();
            }
        }
        public static int CountCart(string ip)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.tbl_SolCart
                        where c.IP == ip
                        select c).Count();
            }
        }
        public static List<SolutionLibr> GetSolutionLibrMyID(string url)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblSolutionLibraries
                        join s in context.tblSolutionSubjects on c.SolSubID equals s.SolSubID
                        where s.IsActive == false && s.Url == url && c.Status==true
                        orderby c.Sol_ID descending
                        select new SolutionLibr
                        {
                            Subject = s.Subject,
                            Sol_ID = c.Sol_ID,
                            Amount = c.Amount,
                            SolSubID = c.SolSubID,
                            Title = c.Title,
                            SolContent = c.SolContent,
                            UrlText = c.UrlText,
                            UrlID = c.UrlID,
                            AnswerPreview = c.AnswerPreview

                        }).ToList();
            }
        }
        public static List<SolutionLibr> NewGetSolutionLibrMyID(int? solid)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblSolutionLibraries
                        join s in context.tblSolutionSubjects on c.SolSubID equals s.SolSubID
                        where c.Status == true && c.SolSubID == solid
                        orderby c.Sol_ID descending
                        select new SolutionLibr
                        {
                            Subject = s.Subject,
                            Sol_ID = c.Sol_ID,
                            Amount = c.Amount,
                            SolSubID = c.SolSubID,
                            Title = c.Title,
                            SolContent = c.SolContent,
                            UrlText = c.UrlText,
                            UrlID = c.UrlID
                        }).ToList();
            }
        }
        public static List<SolutionLibr> GetSolutionLbr()
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblSolutionLibraries
                        join s in context.tblSolutionSubjects on c.SolSubID equals s.SolSubID
                        where c.Status == true
                        select new SolutionLibr
                        {
                            Title = c.Title,
                            Sol_ID = c.Sol_ID,
                            UrlText = c.UrlText
                        }).ToList();
            }
        }
        public static List<SolutionLibr> GetSolutionLbrURL()
        {

            using (var context = new XpertWebEntities())
            {
                return (from c in context.psp_SolutionLibr_URL(true)
                        select new SolutionLibr
                        {
                            Title = c.Title,
                            Sol_ID = c.Sol_ID,
                            UrlText = c.UrlText
                        }).ToList();
            }
        }
        public static List<SolutionLibr> GetSolutionLbrURLMain()
        {

            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblSolutionLibraries
                        where c.Status == true
                        select new SolutionLibr
                        {
                            UrlText = c.UrlText
                        }).ToList();
            }
        }
        public static int CountArticle(string url)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblSolutionLibraries
                        join p in context.tblSolutionSubjects on c.SolSubID equals p.SolSubID
                        where p.Url == url && p.IsActive == false && c.Status==true
                        select c).Count();
            }
        }
        public static List<SolutionLibr> GetSearchData(string search)
        {
            List<SolutionLibr> objSearch = new List<SolutionLibr>();
            using (var context = new XpertWebEntities())
            {
                //objSearch = (from c in context.sp_search_lib(search)
                //             select new SolutionLibr
                //             {
                //                 UrlText = c.UrlText,
                //                 Title = c.Title,
                //                 SolContent = c.SolContent
                //             }).AsEnumerable().ToList();

                objSearch = (from c in context.tblSolutionLibraries
                             where c.Title.Contains(search)
                             select new SolutionLibr
                             {
                                 UrlText = c.UrlText,
                                 Title = c.Title,
                                 SolContent = c.SolContent
                             }).AsEnumerable().ToList();
            }
            return objSearch;
        }
        public static int? GetMaxValue()
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblSolutionSubjects.Max(p => p.SolSubID));
            }
        }

        public static string getSubject(string url)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblSolutionSubjects
                        where c.Url == url
                        select c.Subject).FirstOrDefault();
            }
        }
        public static int AddCart(tbl_SolCart obj)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = new tbl_SolCart();
                data.date = obj.date;
                data.SolCartID = obj.SolCartID;
                data.Sol_ID = obj.Sol_ID;
                data.Amount = obj.Amount;
                data.IP = obj.IP;
                data.qty = obj.qty;
                data.IsActive = obj.IsActive;
                context.tbl_SolCart.Add(data);
                i = context.SaveChanges();
            }
            return i;
        }
        public static int DelCart(int id)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tbl_SolCart.Single(c => c.id == id);
                context.tbl_SolCart.Remove(data);
                i = context.SaveChanges();
            }
            return i;
        }
        public static string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];

        }
        public static int checkOrder(string orderid, string status)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                i = (from c in context.tbl_orderLibrary
                     where c.OrderID == orderid && c.status == status
                     select c).Count();
            }
            return i;
        }
        public static List<SolutionCart> GetCartDetails(string ip)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.psp_maincart(ip)
                        select new SolutionCart
                        {
                            id = c.id,
                            solcartid = c.SolCartID,
                            subject = c.Subject,
                            price = c.Amount,
                            desc = c.Title,
                            qty = c.qty
                        }).ToList();
            }
        }
        public static tbl_SolCart CheckSol(string ip_address, int? sol_id)
        {
         
            using (var context = new XpertWebEntities())
            {
             return (from c in context.tbl_SolCart
                         where c.IP == ip_address && c.Sol_ID==sol_id
                         select c).FirstOrDefault();
            }
           
        }
        public static bool compareFile(string token, string id)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.tbl_orderLibrary
                        where c.token == token && c.OrderID == id
                        select c).Any();
            }
        }
        public static List<SolutionCart> ListSol(string ip, string studid)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.psp_getCart(ip, studid)
                        select new SolutionCart
                        {
                            id = c.id,
                            price = c.Amount,
                            desc = c.Title,
                            qty = c.qty,
                            solution_id = c.Sol_ID,
                            solcartid = c.SolCartID,
                            subject = c.Subject
                        }).ToList();

            }
        }
        public static decimal getSolAmount(string orderid)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.psp_sumtab(orderid)
                        select c.Amount).Single().Value;
            }
        }
        public static int getSolQty(string orderid)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.psp_sumtab(orderid)
                        select c.qty).Single().Value;
            }
        }
        //public static int Add_Address(tbl_studAddress obj)
        //{
        //    int i = 0;
        //    using (var context = new XpertWebEntities())
        //    {
        //        var data = new tbl_studAddress();
        //        data.studid = obj.studid;
        //        data.date = obj.date;
        //        data.address = obj.address;
        //        data.city = obj.city;
        //        data.state = obj.state;
        //        data.phone = obj.phone;
        //        data.zip_code = obj.zip_code;
        //        context.tbl_studAddress.Add(data);
        //        i = context.SaveChanges();
        //    }
        //    return i;
        //}
        //public static int updateAdr(tbl_studAddress obj)
        //{
        //    int i = 0;
        //    using (var context = new XpertWebEntities())
        //    {
        //        i = (from c in context.psp_checkaddress(obj.studid, obj.date, obj.address, obj.city, obj.phone, obj.state, obj.zip_code, obj.date)
        //             select c).Single().Value;
        //    }
        //    return i;
        //}
        //public static int Update_Address(tbl_studAddress obj, string Studid)
        //{
        //    int i = 0;
        //    using (var context = new XpertWebEntities())
        //    {
        //        var data = context.tbl_studAddress.Single(c => c.studid == Studid);
        //        data.phone = obj.phone;
        //        data.state = obj.state;
        //        data.address = obj.address;
        //        data.city = obj.city;
        //        data.zip_code = obj.zip_code;
        //        i = context.SaveChanges();
        //    }
        //    return i;
        //}
        //public static int checkAddress(string studid)
        //{
        //    using (var context = new XpertWebEntities())
        //    {
        //        return (from c in context.tbl_studAddress
        //                where c.studid == studid
        //                select c).Count();
        //    }
        //}
        public static decimal SumTotal(string studid)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.psp_getamount(studid)
                        select c).Single().Value;
            }
        }
        public static int SumQTY(string studid)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.psp_getqty(studid)
                        select c).Single().Value;
            }
        }
        public static int Order_Library(tbl_orderLibrary obj)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                i = (from c in context.psp_orderSolution(obj.OrderID, obj.memberid, obj.qty, obj.IsAnswerActive, obj.IsActive, obj.TotalAmount, obj.CurType, obj.CreatedDate)
                     select c).Single().Value;
                i = context.SaveChanges();
            }
            return i;
        }
        public static int UpdateSolutionToken(string orderid, string token)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tbl_orderLibrary.Single(c => c.OrderID == orderid);
                data.token = token;
                i = context.SaveChanges();
            }
            return i;
        }
        public static int UpdateSolutionPayment(tbl_orderLibrary obj, string orderid)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                i = (from c in context.psp_finalcart(orderid, obj.IsAnswerActive, obj.IsActive, obj.paymentDate, obj.token, obj.status, obj.CurType)
                     select c).Single().Value;
            }
            return i;
        }
        public static int UpdateOrderID(string ip, string orderid)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var IsExists = context.tbl_SolCart.Where(c => c.IP == ip && c.IsActive==false).Select(c => c).FirstOrDefault();
                if (IsExists != null)
                {
                    var data = context.tbl_SolCart.Where(c => c.IP == ip && c.IsActive == false).ToList();
                    foreach (var item in data)
                    {
                        item.order_id = orderid;
                    }
                    //data.order_id = orderid;
                    i = context.SaveChanges();
                }
                else
                {
                    i = 0;
                }
            }
            return i;
        }
        public static int UpdateToken(string orderid, string token)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tbl_orderLibrary.Single(c => c.OrderID == orderid);
                data.token = token;
                i = context.SaveChanges();
            }
            return i;
        }
        public static string GetTokenOrderID(string token)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tbl_orderLibrary.Where(c => c.token == token).Select(c => c.OrderID).FirstOrDefault());
            }
        }
        public static List<tbl_orderLibrary> GetStudentMemberid(string orderid)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.psp_GetOrderDetails(orderid)
                        where c.OrderID == orderid
                        select new tbl_orderLibrary
                        {
                            memberid = c.memberid,
                            TotalAmount = c.TotalAmount
                        }).ToList();
            }
        }
        public static List<tbl_SolCart> GetSubjectByID(string orderid)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.psp_cartidData(orderid)
                        where c.order_id == orderid
                        select new tbl_SolCart
                        {
                            Sol_ID = c.Sol_ID
                        }).ToList();
            }
        }
        public static string GetFilePath(int sol_id)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblSolutionLibraries
                        join s in context.tblAnswerLibraries on c.AnswerID equals s.id
                        where c.Sol_ID == sol_id
                        select s.FilePath).SingleOrDefault();
            }
        }
        public static string GetFileSubject(int? sol_id)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblSolutionLibraries
                        join s in context.tblSolutionSubjects on c.SolSubID equals s.SolSubID
                        where c.Sol_ID == sol_id
                        select s.Subject).SingleOrDefault();
            }
        }
        public static List<tbl_SolCart> GetCartdataID(int? sol_id)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.psp_cartidDataSolID(sol_id)
                        select new tbl_SolCart
                        {
                            SolCartID = c.SolCartID,
                            Amount = c.Amount
                        }).ToList();
            }
        }

        public static List<tblCollegeStudent> GetStudentDetails(string memberid)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.psp_studentdata(memberid)
                        where c.StudID == memberid
                        select new tblCollegeStudent
                        {
                            Name = c.Name,
                            Email = c.Email
                        }).ToList();
            }
        }
        public static int Update_OrderID(string orderid, string memberid)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tbl_SolCart.Single(c => c.StudID == memberid);
                data.order_id = orderid;
                i = context.SaveChanges();
            }
            return i;
        }
        //public static List<tbl_studAddress> GetAddress(string studid)
        //{
        //    using (var context = new XpertWebEntities())
        //    {
        //        return (from c in context.psp_listaddressStudent(studid)
        //                where c.studid == studid
        //                select new tbl_studAddress
        //                {
        //                    address = c.address,
        //                    state = c.state,
        //                    zip_code = c.zip_code,
        //                    phone = c.phone,
        //                    city = c.city
        //                }).ToList();

        //    }
        //}
        public static List<GetDataSolution> MakeList(int id)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblSolutionLibraries
                        join s in context.tblAnswerLibraries on c.AnswerID equals s.id
                        where c.Sol_ID == id
                        select new GetDataSolution
                        {
                            filename = s.FileName,
                            filepath = s.FilePath
                        }).ToList();
            }
        }

        public static int UpdateQuestionID(int QuestionID)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblQuestions.Where(c => c.Id == QuestionID).FirstOrDefault();
                if (data != null)
                {
                    data.PaymentStatus = (Int32)Enums.PaymentStatus.Paid;
                    i = context.SaveChanges();
                }
                else
                {
                    i = 0;
                }
            }
            return i;
        }

    }
    public class SearchReport
    {
        public string search { set; get; }
    }
    public class SolCart
    {
        public int id { get; set; }
        public string SolCartID { get; set; }
        public int SolSubId { get; set; }
        public decimal Amount { get; set; }
        public int qty { get; set; }
        public int IsActive { get; set; }
    }
    public class GetDataSolution
    {
        public string filepath { get; set; }
        public string subject { get; set; }
        public string filename { get; set; }
    }
}
