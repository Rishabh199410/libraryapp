﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryDataLayer;
using LibraryUtility;

namespace LibraryBILayer.Comman
{
    public class Account
    {      
        public static List<tblusernavigation> GetNavigationMenu()
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblusernavigations.Where(c => c.StatusID == (Int32)Enums.RowStatusID.Active).OrderBy(x=> x.Order).AsEnumerable().ToList());
            }
        }
    }
}
