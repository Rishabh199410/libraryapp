﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryDataLayer;

namespace LibraryBILayer.Admin
{
    public class AdminAuthontication
    {
        public tblAdminUser AdminLogin(string Email, string password)
        {
            tblAdminUser userInfo = null;
            try
            {
                using (var context = new XpertWebEntities())
                {
                    userInfo = context.tblAdminUsers.Where(user => user.EmailId == Email && user.Password == password).Select(c=>c).FirstOrDefault();
                    //userInfo = details;

                }
            }
            catch (Exception ex)
            {
                //Helper.ExceptionHandler.WriteToLogFile(ex.ToString());
            }
            return userInfo;
        }

        //Admin Registration
        public int AdminRegister(tblAdminUser obj)
        {
            int result = 0;
            using (var context = new XpertWebEntities())
            {
                if (obj != null)
                {
                    context.tblAdminUsers.Add(obj);
                    result = context.SaveChanges();
                }
            }
            return result;
        }

        //Check Email-Id Existance
        public string Check_EmailId(string emailid)
        {
            string Result = "";
            var email = "";
            using (var context = new XpertWebEntities())
            {
                email = (from e in context.tblAdminUsers where e.EmailId == emailid select e.EmailId).FirstOrDefault();
            }

            if (!String.IsNullOrEmpty(email))
            {
                Result = "EmailId Already Exist";
            }

            else
            {
                Result = "Not Exist";
            }
            return Result;
        }

        public tblAdminUser GetAdminDetails(int AdminId)//GetAdminDetails()
        {
            tblAdminUser obj = new tblAdminUser();
            using (var context = new XpertWebEntities())
            {
                obj = (from e in context.tblAdminUsers where e.AdminId == AdminId select e).FirstOrDefault();
            }
            return obj;
        }

        //public int UpdateSubAdminProfile(tblAdminUser obj)
        //{
        //    int result = 0;
        //    using (var context = new TagTutorModelContainer())
        //    {
        //        if (obj.AdminId != null || obj.AdminId > 0)
        //        {
        //            var subadmindetails = (from m in context.tblAdminUsers where m.AdminId == obj.AdminId select m).FirstOrDefault();
        //            subadmindetails.FirstName = obj.FirstName;
        //            subadmindetails.LastName = obj.LastName;
        //            subadmindetails.EmailId = obj.EmailId;
        //            subadmindetails.Contact = obj.Contact;
        //            subadmindetails.Password = obj.Password;
        //            subadmindetails.JobTitle = obj.JobTitle;
        //            //subadmindetails.UpdatedBy =
        //            subadmindetails.UpdatedOn = DateTime.UtcNow;
        //            subadmindetails.RoleId = Convert.ToInt32(obj.RoleId);
        //            result = context.SaveChanges();
        //        }
        //    }
        //    return result;
        //}

        //public List<tblAdminUser> SubAdminList()
        //{
        //    List<tblAdminUser> data = new List<tblAdminUser>();
        //    using (var context = new TagTutorModelContainer())
        //    {
        //        data = (from a in context.tblAdminUsers where a.IsDeleted == false select a).AsEnumerable().ToList();
        //    }
        //    return data;
        //}

        //public string Deleted(int Id)
        //{
        //    using (var context = new TagTutorModelContainer())
        //    {
        //        var data = (from e in context.tblAdminUsers where e.AdminId == Id select e).FirstOrDefault();
        //        data.IsDeleted = true;
        //        data.IsActive = false;
        //        context.SaveChanges();
        //    }
        //    return "Record is successfully deleted";
        //}


    }
}

