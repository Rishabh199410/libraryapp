﻿using LibraryDataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryBILayer.Admin
{
    public static class ManageStudent
    {
        //public static List<SP_GETSTUDENT_Result> StudentDetails(bool IsActive)
        //{
        //    List<SP_GETSTUDENT_Result> Obj = new List<SP_GETSTUDENT_Result>();
        //    using (var context = new XpertWebEntities())
        //    {
        //        Obj = context.SP_GETSTUDENT().AsEnumerable().ToList();
        //        Obj = Obj.Where(x => x.IsActive == IsActive).ToList();
        //    }
        //    return Obj;
        //}
        public static List<tblCollegeStudent> StudentDetailsSearch(string search)
        {
            List<tblCollegeStudent> Obj = new List<tblCollegeStudent>();
            using (var context = new XpertWebEntities())
            {
                Obj = (from m in context.tblCollegeStudents where m.Email.Contains(search) select m).AsEnumerable().OrderByDescending(x => x.id).ToList();
            }
            return Obj;
        }
        public static int DeleteStudentData(int ID, bool status, string email)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblCollegeStudents.Single(c => c.id == ID);
                data.IsActive = status;
                data.UpdatedBy = email;
                data.UpdatedOn = DateTime.Now;
                i = context.SaveChanges();
            }
            return i;
        }
        public static int FinalDeleteStudentData(int ID, bool status, string email)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblCollegeStudents.Single(c => c.id == ID);
                context.tblCollegeStudents.Remove(data);
                i = context.SaveChanges();
            }
            return i;
        }
        public static tblCollegeStudent GetStudentData(string id)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblCollegeStudents.Where(c => c.StudID == id).Select(c => c).FirstOrDefault());
            }
        }
    }
}
