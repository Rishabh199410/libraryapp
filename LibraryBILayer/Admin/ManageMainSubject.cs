﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryDataLayer;

namespace LibraryBILayer.Admin
{
    public class ManageMainSubject
    {
        public static int AddOrUpdateMainSubject(tblMainSubject obj)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var IsExists = context.tblMainSubjects.Where(c => c.SubjectCode == obj.SubjectCode).Select(c => c).FirstOrDefault();

                if (IsExists != null)
                {
                    if (obj.MSID > 0)
                    {
                        var data = context.tblMainSubjects.Single(c => c.MSID == obj.MSID);
                        data.UpdatedBy = obj.UpdatedBy;
                        data.SubjectName = obj.SubjectName;
                        data.UpdatedOn = obj.UpdatedOn;
                        data.Country = obj.Country;
                        data.SolSubID = obj.SolSubID;
                        //data.MSID = obj.SolSubID;
                        data.SubjectCode = obj.SubjectCode;
                        data.UID = obj.UID;
                        i = context.SaveChanges();
                    }
                    else
                    {
                        i = -1;
                    }
                }
                else
                {
                    context.tblMainSubjects.Add(obj);
                    i = context.SaveChanges();
                }
            }
            return i;
        }
        public static tblMainSubject getSingleMain(int MSID)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblMainSubjects.Where(c => c.MSID == MSID).Select(c => c).FirstOrDefault());
            }
        }
        public static List<SolutionLibr> GetAllMainSubject(bool IsActive)
        {
            List<SolutionLibr> tblMain = new List<SolutionLibr>();
            using (var context = new XpertWebEntities())
            {
                tblMain = (from c in context.sp_getMainSubject(IsActive)
                           select new SolutionLibr
                           {
                               SubjectCode = c.SubjectCode,
                               Subject = c.SubjectName,
                               country = c.Country,
                               Status = c.IsActive,
                               MSID = c.MSID,
                               university = c.University
                           }).AsEnumerable().ToList();
            }
            return tblMain;
        }
        //public static List<tblMainSubject> GetSearchAllMainSubject(string subject,bool IsActive)
        //{
        //    List<tblMainSubject> tblMain = new List<tblMainSubject>();
        //    using (var context = new XpertWebEntities())
        //    {
        //        tblMain = (from c in context.tblMainSubjects
        //                   where c.IsActive == IsActive && c.SubjectName.Contains(subject)
        //                   orderby c.MSID descending
        //                   select c).AsEnumerable().ToList();
        //    }
        //    return tblMain;
        //}
        public static List<SolutionLibr> GetSearchAllMainSubject(string subject, bool IsActive)
        {
            List<SolutionLibr> tblMain = new List<SolutionLibr>();
            using (var context = new XpertWebEntities())
            {
                tblMain = (from c in context.sp_getMainSearchSubject(subject)
                          select new SolutionLibr
                          {
                             SubjectCode=c.SubjectCode,
                             Subject=c.SubjectName,
                             country=c.Country,
                             Status=c.IsActive,
                             MSID=c.MSID,
                             university=c.University
                          }).AsEnumerable().ToList();
            }
            return tblMain;
        }
        public static int DeleteSubjectData(int ID, bool status, string email)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblMainSubjects.Single(c => c.MSID == ID);
                data.IsActive = status;
                data.UpdatedBy = email;
                data.UpdatedOn = DateTime.Now;
                i = context.SaveChanges();
            }
            return i;
        }
        public static int FinalDeleteSubjectData(int ID, bool status, string email)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblMainSubjects.Single(c => c.MSID == ID);
                context.tblMainSubjects.Remove(data);
                i = context.SaveChanges();
            }
            return i;
        }
        public List<tblSolutionSubject> BindCategory(int UID)
        {
            //this.dbEntity.Configuration.ProxyCreationEnabled = false;

            List<tblSolutionSubject> lstCountry = new List<tblSolutionSubject>();
            using (var context = new XpertWebEntities())
            {
                lstCountry = (from c in context.tblSolutionSubjects
                              join p in context.tblMainSubjects on c.SolSubID equals p.SolSubID
                              where p.UID == UID
                              select c).ToList();
            }
            return lstCountry;
        }
        public List<tblUniversity> BindUniversity()
        {
            //this.dbEntity.Configuration.ProxyCreationEnabled = false;

            List<tblUniversity> lstUniversity = new List<tblUniversity>();
            using (var context = new XpertWebEntities())
            {
                lstUniversity = context.tblUniversities.ToList();
            }
            return lstUniversity;
        }
        public List<tblMainSubject> BindSubject(int SolSubID,int UID)
        {
            //this.dbEntity.Configuration.ProxyCreationEnabled = false;

            List<tblMainSubject> lstSubject = new List<tblMainSubject>();
            using (var context = new XpertWebEntities())
            {
                lstSubject = context.tblMainSubjects.Where(c => c.SolSubID == SolSubID && c.UID==UID).ToList();
            }
            return lstSubject;
        }
        public List<tblMainSubject> BindMainSubjectByCode(int code)
        {
            List<tblMainSubject> lstSubject = new List<tblMainSubject>();
            using (var context = new XpertWebEntities())
            {
                lstSubject = context.tblMainSubjects.Where(c => c.MSID == code).ToList();
            }
            return lstSubject;
        }
        public List<tblMainSubject> BindMainSubjectByName(string name)
        {
            List<tblMainSubject> lstSubject = new List<tblMainSubject>();
            using (var context = new XpertWebEntities())
            {
                lstSubject = context.tblMainSubjects.Where(c => c.SubjectName == name).ToList();
            }
            return lstSubject;
        }
    }
}
