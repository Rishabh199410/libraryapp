﻿using LibraryDataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryBILayer.Admin
{
    public class ManagePlagiarism
    {
        #region Update or Delete
        public static int AddorUpdatePlagiarism(mstplagiarismprice obj)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.mstplagiarismprices.Where(c => c.PlagPriceID == obj.PlagPriceID).FirstOrDefault();
                if (data != null)
                {
                    if (obj.PlagPriceID > 0)
                    {
                        var data2 = context.mstplagiarismprices.Single(c => c.PlagPriceID == obj.PlagPriceID);
                        data2.UpdatedBy = obj.UpdatedBy;
                        data2.UpdatedOn = obj.UpdatedOn;
                        data2.PlagPrice = obj.PlagPrice;
                        i = context.SaveChanges();
                        // data2.
                    }

                }
                else
                {
                    context.mstplagiarismprices.Add(obj);
                    i = context.SaveChanges();
                }
            }
            return i;
        }
        #endregion

        #region Get By Id
        public static mstplagiarismprice GetPlagiarism(int ID)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.mstplagiarismprices.Where(c => c.PlagPriceID == ID).FirstOrDefault());
            }
        }
        #endregion

        #region Delete
     

        #endregion

        #region Get Plagarism
        public static List<mstplagiarismprice> GetPlagiarismList()
        {
            using (var context = new XpertWebEntities())
            {
                return (context.mstplagiarismprices.OrderByDescending(c => c.PlagPriceID).OrderBy(x=> x.PlagPrice).AsEnumerable().ToList());
            }
        }
        #endregion


    }
}
