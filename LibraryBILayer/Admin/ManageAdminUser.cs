﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryBILayer.Model;
using LibraryDataLayer;
namespace LibraryBILayer.Admin
{
    public class ManageAdminUser
    {
        public static int AddorUpdateAdminUser(tblAdminUser tbladminuser)
        {
            int result = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblAdminUsers.Where(c => c.AdminId == tbladminuser.AdminId).FirstOrDefault();
                if (data != null)
                {
                    var data2 = context.tblAdminUsers.Single(c => c.AdminId == tbladminuser.AdminId);
                    data2.FirstName = tbladminuser.FirstName;
                    data2.LastName = tbladminuser.LastName;
                    data2.EmailId = tbladminuser.EmailId;
                    data2.Password = tbladminuser.Password;
                    data2.RoleID = tbladminuser.RoleID;
                    result = context.SaveChanges();

                }
                else
                {
                    tbladminuser.StatusID = 1;
                    tbladminuser.CreatedBy = 1;
                    tbladminuser.ModifiedBy = 1;
                    context.tblAdminUsers.Add(tbladminuser);
                    result = context.SaveChanges();
                }
            }

            return result;
        }

        public static tblAdminUser GetAdminUser(int Id)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblAdminUsers.Single(c => c.AdminId == Id));
            }
        }
        public static int DeleteAdminUser(int id, int StatusId)
        {
            int result = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblAdminUsers.Single(c => c.AdminId == id);
                data.StatusID = StatusId;
                result = context.SaveChanges();
                //context.tbladminroles.Remove(data);
            }
            return result;
        }

        public static int FinalDeleteAdminUser(int id)
        {
            int result = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblAdminUsers.Single(c => c.AdminId == id);
                context.tblAdminUsers.Remove(data);           
                result = context.SaveChanges();              
            }
            return result;
        }

        public static List<AdminUserModel> GetAdminUserList()
        {
            using (var context = new XpertWebEntities())
            {
                List<AdminUserModel> list = (from user in context.tblAdminUsers
                                             join role in context.tbladminroles on user.RoleID equals role.RoleID
                                             select new AdminUserModel
                                             {
                                                 AdminId = user.AdminId,
                                                 FirstName = user.FirstName,
                                                 LastName = user.LastName,
                                                 RoleName = role.RoleName,
                                                 StatusID = user.StatusID
                                             }).AsEnumerable().ToList();

                return list;
            }
        }

        public static List<tblAdminUser> GetAdminUserList(int IsActive)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblAdminUsers.Where(c => c.StatusID == IsActive).OrderByDescending(c => c.AdminId).AsEnumerable().ToList());
            }
        }

    }
}
