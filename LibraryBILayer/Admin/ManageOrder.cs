﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryDataLayer;
namespace LibraryBILayer.Admin
{
    public class ManageOrder
    {
        public static List<tbl_orderLibrary> GetOrderList(bool IsActive)
        {
            List<tbl_orderLibrary> orderList = new List<tbl_orderLibrary>();
            using (var context = new XpertWebEntities())
            {
                orderList = (context.tbl_orderLibrary.Where(c => c.IsActive == IsActive).OrderByDescending(c=>c.CreatedDate).Select(c => c).AsEnumerable().ToList());
            }
            return orderList;
        }
        public static List<SolutionLibr> GetSolList(string orderid)
        {
            List<SolutionLibr> list = new List<SolutionLibr>();
            using (var context = new XpertWebEntities())
            {
                list = (from c in context.psp_getProducts(orderid)
                        select new SolutionLibr
                        {
                            Amount=c.Amount,
                            Subject=c.Subject,
                            university=c.University,
                            Title=c.Title,
                            SubjectCode=c.SubJectCode
                        }).AsEnumerable().ToList();
            }
            return list;
        }
        public static tblCollegeStudent GetStudentD(string memberid)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblCollegeStudents.Where(c => c.StudID == memberid).FirstOrDefault());
            }
        }
        public static SolutionLibr GetSolution(string OrderID)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.psp_getOrder(OrderID)
                        select new SolutionLibr
                        {
                            Amount = c.TotalAmount,
                            Qty = c.qty,
                            CreatedOn = c.CreatedDate,
                            memberid = c.memberid,
                            OrderID = c.OrderID,
                            PaymentDate = c.paymentDate,
                            PaymentStatus = c.status,
                            name = c.Name,
                            email = c.Email,
                            phone = c.PhoneNo,
                            country = c.country
                        }).FirstOrDefault();
            }
        }
    }
}
