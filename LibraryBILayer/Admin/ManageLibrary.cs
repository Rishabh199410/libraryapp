﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryDataLayer;

namespace LibraryBILayer.Admin
{
    public static class ManageLibrary
    {
        public static int AddSolution(tblSolutionLibrary obj)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = new tblSolutionLibrary();
                data.SolSubID = obj.SolSubID;
                data.CreatedOn = obj.CreatedOn;
                data.AnswerID = obj.AnswerID;
                data.CreatedBy = obj.CreatedBy;
                data.SubjectName = obj.SubjectName;
                data.Country = obj.Country;
                data.UrlText = obj.UrlText;
                data.Tags = obj.Tags;
                data.Description = obj.Description;
                data.SolContent = obj.SolContent;
                data.IsAnswer = obj.IsAnswer;
                data.Title = obj.Title;
                data.Amount = obj.Amount;
                data.Status = obj.Status;
                data.UrlID = obj.UrlID;
                data.AnswerPreview = obj.AnswerPreview;
                data.UID = obj.UID;
                data.MSID = obj.MSID;
                data.Question_File_ID = obj.Question_File_ID;
                context.tblSolutionLibraries.Add(data);
                i = context.SaveChanges();
            }
            return i;
        }

        public static int checkSolution(string title)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                i = (from c in context.tblSolutionLibraries
                     where c.Title == title
                     select c).Count();
            }
            return i;
        }
        public static int UpdateSolutionID(int solid, int ID)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblQuestionFiles.Single(c => c.ID == solid);
                data.Sol_ID = ID;
                i = context.SaveChanges();
            }
            return i;
        }
        public static List<tblQuestionFile> GetQuestionFiles(string solid)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblQuestionFiles.Where(c => c.Question_File_ID == solid).AsEnumerable().ToList());
            }
        }
        public static int getSolID()
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.solLibs
                        where c.id == 1
                        select c.value).Single().Value;
            }
        }
        public static int UpdateQuestionFiles(string file_id, string updatedBy)
        {
            using (var context = new XpertWebEntities())
            {
                var data = (from c in context.tblQuestionFiles.Where(c => c.Question_File_ID == file_id) select c).FirstOrDefault();
                //var data = context.tblQuestionFiles.Where(c => c.Question_File_ID == file_id);
                data.IsActive = false;
                data.UpdatedBy = updatedBy;
                data.UpdatedOn = DateTime.Now;
                return context.SaveChanges();
            }
        }
        public static tblSolutionLibrary CheckIfSolFile(string file_id)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblSolutionLibraries.Where(c => c.Question_File_ID == file_id).FirstOrDefault());
            }
        }
        public static tblQuestionFile CheckIfQuestionFile(string file_id)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblQuestionFiles.Where(c => c.Question_File_ID == file_id).FirstOrDefault());
            }
        }
        public static List<tblQuestionFile> GetQuestionFile(int Sol_ID)
        {
            //List<tblQuestionFile> myList = new List<tblQuestionFile>();
            using (var context = new XpertWebEntities())
            {
                return (context.tblQuestionFiles.Where(c => c.Sol_ID == Sol_ID && c.IsActive == true).Select(c => c).ToList());
            }
        }
        public static void updateSolID()
        {
            using (var context = new XpertWebEntities())
            {
                var data = context.solLibs.Single(c => c.id == 1);
                data.value = data.value + 1;
                context.SaveChanges();
            }
        }
        public static int AddAnswer(tblAnswerLibrary obj)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = new tblAnswerLibrary();
                //data.FileName = obj.FileName;
                data.FilePath = obj.FilePath;
                data.IsActive = obj.IsActive;
                context.tblAnswerLibraries.Add(data);
                i = context.SaveChanges();
            }
            return i;
        }

        public static int AddQuestionFile(tblQuestionFile obj)
        {
            using (var context = new XpertWebEntities())
            {
                context.tblQuestionFiles.Add(obj);
                return context.SaveChanges();
            }
        }

        public static int UpdateAnswer(tblAnswerLibrary obj)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblAnswerLibraries.Single(c => c.id == obj.id);
                data.FileName = obj.FileName;
                data.FilePath = obj.FilePath;
                context.tblAnswerLibraries.Add(data);
                i = context.SaveChanges();
            }
            return i;
        }
        public static int UpdateSolution(tblSolutionLibrary obj)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblSolutionLibraries.Single(c => c.Sol_ID == obj.Sol_ID);
                data.SolSubID = obj.SolSubID;
                data.Title = obj.Title;
                data.UrlText = obj.UrlText;
                data.UpdatedOn = obj.UpdatedOn;
                data.Country = obj.Country;
                data.SubjectName = obj.SubjectName;
                data.UpdatedBy = obj.UpdatedBy;
                data.AnswerID = obj.AnswerID;
                data.Tags = obj.Tags;
                data.IsAnswer = obj.IsAnswer;
                data.Description = obj.Description;
                data.SolContent = obj.SolContent;
                data.Amount = obj.Amount;
                data.Question_File_ID = obj.Question_File_ID;
                data.AnswerPreview = obj.AnswerPreview;
                i = context.SaveChanges();
            }
            return i;
        }

        public static int GetTopSolutionLibraryID()
        {

            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblIDs
                        select c.mainid).Single().Value;
            }
        }
        public static void updateID()
        {
            using (var context = new XpertWebEntities())
            {
                var data = context.tblIDs.Single(c => c.id == 1);
                data.mainid = data.mainid + 1;
                context.SaveChanges();
            }
        }
        public static int GetAnswerLibraryID()
        {

            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblAnswerLibraries
                        select c.id).Max();
            }
        }
        public static List<SolutionLibr> GetSolutionLibrary(int SolSubID)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.psp_solutionLibraryList(SolSubID)
                        select new SolutionLibr
                        {
                            Sol_ID = c.Sol_ID,
                            SolSubID = c.SolSubID,
                            Subject = c.Subject,
                            Title = c.Title,
                            Amount = c.Amount,
                            Status = c.Status
                        }).ToList();
            }
        }

        public static int DeleteSolutionLibrary(int Sol_ID, bool value, string email)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblSolutionLibraries.Single(c => c.Sol_ID == Sol_ID);
                data.Status = value;
                data.UpdatedBy = email;
                data.UpdatedOn = DateTime.Now;
                i = context.SaveChanges();
                return i;
            }
        }
        public static int DeleteSolution(int Sol_ID, bool value, string email)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblSolutionLibraries.Single(c => c.Sol_ID == Sol_ID);
                //context.tblSolutionLibraries.delete
                context.tblSolutionLibraries.Remove(data);
                i = context.SaveChanges();
                return i;
            }
        }

        public static int DeleteQuestionSolutionLib(int Id)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblQuestionFiles.Single(c => c.ID == Id);
                //context.tblSolutionLibraries.delete
                context.tblQuestionFiles.Remove(data);
                i = context.SaveChanges();
                return i;
            }
        }
        public static List<SolutionLibr> BindSolutionLibraryStatus0(int status)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.psp_solutionLibraryListChecked(status)
                        join u in context.tblUniversities on c.University equals u.University
                        select new SolutionLibr
                        {
                            Sol_ID = c.Sol_ID,
                            SolSubID = c.SolSubID,
                            Subject = c.Subject,
                            Title = c.Title,
                            university = u.shortName,
                            SubjectName = c.SubjectName,
                            country = c.Country,
                            SubjectCode = c.SubjectCode,
                            Amount = c.Amount,
                            Status = c.Status,
                            CreatedOn = c.CreatedOn
                        }).ToList();
            }
        }
        public static tblCollegeStudent GetStudentDatas(string Id)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblCollegeStudents.Where(c => c.StudID == Id).Select(c => c).FirstOrDefault());
            }
        }
        public static List<tblSolutionLibrary> GetOrderSubject(string OrderID)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblSolutionLibraries
                        join p in context.tbl_SolCart on c.Sol_ID equals p.Sol_ID
                        where p.order_id == OrderID
                        select c).AsEnumerable().ToList();
            }
        }
        public static List<SolutionLibr> BindSolutionLibraryStatusSearch(string title)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.psp_solutionLibraryListSearch(title)
                        select new SolutionLibr
                        {
                            Sol_ID = c.Sol_ID,
                            university = c.University,
                            SolSubID = c.SolSubID,
                            SubjectCode = c.SubjectCode,
                            Subject = c.Subject,
                            Title = c.Title,
                            Amount = c.Amount,
                            Status = c.Status
                        }).ToList();
            }
        }
        public static List<SolutionLibr> BindEditSolutionLibrary(int Sol_ID)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.psp_EditsolutionLibrary(Sol_ID)
                        select new SolutionLibr
                        {
                            Sol_ID = c.Sol_ID,
                            SolSubID = c.SolSubID,
                            AnswerID = c.AnswerID,
                            IsAnswer = c.IsAnswer,
                            SolContent = c.SolContent,
                            Amount = c.Amount,
                            Title = c.Title,
                            UrlText = c.UrlText,
                            AnswerfilePath = c.FilePath,
                            AnswerPreview = c.answerPreview,
                            UID = c.UID,
                            country = c.Country,
                            SubjectName = c.SubjectName,
                            MSID = c.MSID,
                            Question_File_ID = c.Question_File_ID,
                            Tags = c.Tags,
                            Description = c.Description
                        }).ToList();
            }
        }
        public static List<SolutionLibr> BindEditSolutionLibraryWithoutAnswer(int Sol_ID)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblSolutionLibraries
                        where c.Sol_ID == Sol_ID
                        select new SolutionLibr
                        {
                            Sol_ID = c.Sol_ID,
                            SolSubID = c.SolSubID,
                            AnswerID = c.AnswerID,
                            SolContent = c.SolContent,
                            Amount = c.Amount,
                            IsAnswer = c.IsAnswer,
                            Title = c.Title,
                            UrlText = c.UrlText,
                            UID = c.UID,
                            country = c.Country,
                            SubjectName = c.SubjectName,
                            MSID = c.MSID,
                            Question_File_ID = c.Question_File_ID,
                            Tags = c.Tags,
                            Description = c.Description
                        }).ToList();
            }
        }
        public static bool CheckIfAnswer(int ID)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblSolutionLibraries.Where(c => c.Sol_ID == ID && c.AnswerID != 0).Any());
            }
        }
        public static List<SolutionLibr> BindEditSolutionLibraryStatus(int status)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.psp_EditsolutionLibraryByStatus(status)
                        select new SolutionLibr
                        {
                            Sol_ID = c.Sol_ID,
                            SolSubID = c.SolSubID,
                            AnswerID = c.AnswerID,
                            SolContent = c.SolContent,
                            Amount = c.Amount,
                            Title = c.Title,
                            AnswerfilePath = c.FilePath,
                            AnswerPreview = c.answerPreview
                        }).ToList();
            }
        }
        public static tblSolutionLibrary GetURLID()
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblSolutionLibraries
                        where c.Sol_ID == 1
                        select c).FirstOrDefault();
            }
        }
        public static int GetMaxURLID()
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblSolutionLibraries
                        select c.UrlID).Max().Value;
            }
        }
        public static string GetUniversityName(int UID)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblUniversities.Where(c => c.UID == UID).Select(c => c.University).FirstOrDefault());
            }
        }
        public static List<QuestionLib> QuestionList(int ServiceTypeID)
        {
            using (var context = new XpertWebEntities())
            {
                //return (from c in context.tblQuestions
                //        join p in context.tblSolutionSubjects on c.SolSubID equals p.SolSubID
                //        join q in context.tblCollegeStudents on c.StudID equals q.StudID
                //        orderby c.Id descending
                //        select new QuestionLib
                //        {
                //            CreatedOn=c.CreatedOn,
                //            Subject=p.Subject,
                //            Question=c.Question,
                //            QuestionID=c.QuestionId,
                //            StudentID=c.StudID,
                //            Name=q.Name,
                //            Email=q.Email,
                //            ID=c.Id,
                //            File=c.FileNames
                //        }).AsEnumerable().ToList();

                return (from c in context.tblQuestions
                        join q in context.tblCollegeStudents on c.StudID equals q.StudID
                        join p in context.mstpaymentstatus on c.PaymentStatus equals p.StatusID
                        join s in context.mststatus on c.StatusID equals s.StatusID
                        where c.ServiceTypeID == ServiceTypeID
                        orderby c.StatusID, c.Id descending
                        select new QuestionLib
                        {
                            CreatedOn = c.CreatedOn,
                            Subject = c.Subject,
                            Question = c.Question,
                            QuestionID = c.QuestionId,
                            StudentID = c.StudID,
                            Name = q.Name,
                            Email = q.Email,
                            ID = c.Id,
                            File = c.FileNames,
                            Status = s.Status,
                            PaymentStatus = p.Status
                        }).AsEnumerable().ToList();
            }
        }
        public static QuestionLib QuestionData(int ID)
        {

            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblQuestions
                        join q in context.tblCollegeStudents on c.StudID equals q.StudID
                        join d in context.tblquestiondetails on c.Id equals d.QuestionID
                        where c.Id == ID
                        orderby c.Id descending
                        select new QuestionLib
                        {
                            CreatedOn = c.CreatedOn,
                            Subject = c.Subject,
                            Question = c.Question,
                            QuestionID = c.QuestionId,
                            StudentID = c.StudID,
                            StatusID = c.StatusID,
                            Phone = q.PhoneNo,
                            SolID = c.Sol_ID,
                            Name = q.Name,
                            Email = q.Email,
                            ID = c.Id,
                            File = c.FileNames,
                            TotalWords = d.TotalWords,
                            EstimatedPrice = d.Price
                        }).FirstOrDefault();
            }
        }

        public static QuestionLib ViewPlagiarismByID(int ID)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblQuestions
                        join q in context.tblCollegeStudents on c.StudID equals q.StudID
                        join d in context.tblquestiondetails on c.Id equals d.QuestionID
                        where c.Id == ID
                        orderby c.Id descending
                        select new QuestionLib
                        {
                            CreatedOn = c.CreatedOn,
                            Subject = c.Subject,
                            Question = c.Question,
                            QuestionID = c.QuestionId,
                            StudentID = c.StudID,
                            StatusID = c.StatusID,
                            Phone = q.PhoneNo,
                            SolID = c.Sol_ID,
                            Name = q.Name,
                            Email = q.Email,
                            ID = c.Id,
                            File = c.FileNames,
                            TotalWords = d.TotalWords,
                            EstimatedPrice = d.Price
                        }).FirstOrDefault();
            }
        }

        public static QuestionLib ViewCDRServiceByID(int ID)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblQuestions
                        join q in context.tblCollegeStudents on c.StudID equals q.StudID
                        join d in context.tblquestiondetails on c.Id equals d.QuestionID
                        //join t in context.msttimeslots on d.TimeSlotID equals t.TimeSlotID
                        join s in context.mstservices on d.ServiceID equals s.ServiceID
                        where c.Id == ID
                        orderby c.Id descending
                        select new QuestionLib
                        {
                            CreatedOn = c.CreatedOn,
                            Subject = c.Subject,
                            Question = c.Question,
                            QuestionID = c.QuestionId,
                            StudentID = c.StudID,
                            Phone = q.PhoneNo,
                            SolID = c.Sol_ID,
                            Name = q.Name,
                            Email = q.Email,
                            StatusID = c.StatusID,
                            ID = c.Id,
                            File = c.FileNames,
                            //Time = t.TimeSlot,
                            EstimatedPrice = d.Price,
                            Service = s.ServiceType
                        }).FirstOrDefault();
            }
        }

        public static QuestionLib ViewProofreadingByID(int ID)
        {
            using (var context = new XpertWebEntities())
            {
                return (from c in context.tblQuestions
                        join q in context.tblCollegeStudents on c.StudID equals q.StudID
                        join d in context.tblquestiondetails on c.Id equals d.QuestionID
                        join t in context.msttimeslots on d.TimeSlotID equals t.TimeSlotID
                        join s in context.msteducationlevels on d.EducationLevelID equals s.EducationLevelID
                        where c.Id == ID
                        orderby c.Id descending
                        select new QuestionLib
                        {
                            CreatedOn = c.CreatedOn,
                            Subject = c.Subject,
                            Question = c.Question,
                            QuestionID = c.QuestionId,
                            StudentID = c.StudID,
                            Phone = q.PhoneNo,
                            SolID = c.Sol_ID,
                            Name = q.Name,
                            Email = q.Email,
                            ID = c.Id,
                            File = c.FileNames,
                            Time = t.TimeSlot,
                            EstimatedPrice = d.Price,
                            TotalWords = d.TotalWords,
                            EductionLevel = s.EducationLevel
                        }).FirstOrDefault();
            }
        }

        public static List<mstservice> GetService(int ServiceTypeID)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.mstservices.Where(c => c.ServiceTypeID == ServiceTypeID).ToList());
            }
        }

        public static tblSolutionLibrary getSolu(int Id)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblSolutionLibraries.Where(c => c.Sol_ID == Id).FirstOrDefault());
            }
        }

        public static int UpdateQuestionID(int Id, int? StatusID, decimal? price)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblQuestions.Single(c => c.Id == Id);
                data.StatusID = StatusID;

                var detail = context.tblquestiondetails.Where(c => c.QuestionID == Id).FirstOrDefault();
                if (detail != null)
                {
                    detail.Price = price;
                }

                i = context.SaveChanges();
            }
            return i;
        }

        public static List<mstplagiarismprice> GetPlagiarismPrice()
        {
            using (var context = new XpertWebEntities())
            {
                return (context.mstplagiarismprices.OrderBy(x => x.PlagPrice).ToList());
            }
        }

    }
    public class BindSubject
    {
        public int UrlID { get; set; }
        public int Sol_ID { get; set; }
        public int SolSubID { get; set; }
        public string Subject { get; set; }
    }
    public class SolutionLibr
    {
        public DateTime? CreatedOn { get; set; }
        public int? Sol_ID { get; set; }
        public int? SolSubID { get; set; }
        public string SubjectCode { get; set; }
        public string Subject { get; set; }
        public string Title { get; set; }
        public double? Amount { get; set; }
        public bool? Status { get; set; }
        public string Tags { get; set; }
        public string Description { get; set; }
        public string AnswerfilePath { get; set; }
        public string memberid { get; set; }
        public string AnswerPreview { get; set; }
        public int? AnswerID { get; set; }
        public int? CheckStatus { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string Question_File_ID { get; set; }
        public string SolContent { get; set; }
        public string PaymentStatus { get; set; }
        public string UrlText { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string country { get; set; }
        public string SubjectName { get; set; }
        public string OrderID { get; set; }
        public int? Qty { get; set; }
        public bool? IsAnswer { get; set; }
        public int? UrlID { get; set; }
        public int? ID { get; set; }
        public string university { get; set; }
        public int? MSID { get; set; }
        public int? UID { get; set; }
    }
    public class FreeSam
    {
        public string subject { get; set; }
        public string title { get; set; }
        public int? word_limit { get; set; }
        public int? flag { get; set; }
        public int id { get; set; }
        public int MstCatList { get; set; }
        public string filePath { get; set; }
        public string url_text { get; set; }
        public int? pageView { get; set; }
        public int? download { get; set; }
        [Required(ErrorMessage = "Please enter sample content")]

        public string sampleContent { get; set; }
    }
}
