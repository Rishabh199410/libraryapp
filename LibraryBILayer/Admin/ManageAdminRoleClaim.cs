﻿using System;
using System.Collections.Generic;
using System.Linq;
using LibraryDataLayer;
using LibraryUtility;

namespace LibraryBILayer.Admin
{
    public class ManageAdminRoleClaim
    {
        public static List<tbladminrole> GetAdminRole()
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tbladminroles.Where(c => c.StatusID == (Int32)Enums.RowStatusID.Active).AsEnumerable().ToList());
            }
        }

        public static List<tbladminnavigation> GetNavigation()
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tbladminnavigations.Where(c => c.StatusID == (Int32)Enums.RowStatusID.Active).AsEnumerable().ToList());
            }
        }

        public static List<tbladminroleclaim> GetRoleClaimsByID(int RoleID)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tbladminroleclaims.Where(c => c.RoleID == RoleID).AsEnumerable().ToList());
            }
        }

        public static bool RemoveRoleClaims(int RoleID)
        {
            bool isSuccess = false;
            using (var context = new XpertWebEntities())
            {
                List<tbladminroleclaim> roleClaimList = context.tbladminroleclaims.Where(c => c.RoleID == RoleID).AsEnumerable().ToList();
                if (roleClaimList.Count > 0)
                {
                    foreach (var item in roleClaimList)
                    {
                        context.tbladminroleclaims.Remove(item);
                    }

                    if (context.SaveChanges() > 0)
                    {
                        isSuccess = true;
                        return isSuccess;
                    }
                }
                else
                {
                    isSuccess = true;
                    return isSuccess;
                }
            }
            return isSuccess;
        }

        public static int AddRoleClaims(List<tbladminroleclaim> roleClaimsList)
        {
            int returnValue = 0;
            using (var context = new XpertWebEntities())
            {
                foreach (var item in roleClaimsList)
                {
                    context.tbladminroleclaims.Add(item);
                }

                returnValue = context.SaveChanges();
            }
            return returnValue;
        }
    }
}
