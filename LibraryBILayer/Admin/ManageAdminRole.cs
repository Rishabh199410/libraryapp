﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryDataLayer;
namespace LibraryBILayer.Admin
{
    public class ManageAdminRole
    {
        public static int AddorUpdateAdminRole(tbladminrole tbladminrole)
        {
            int result = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tbladminroles.Where(c => c.RoleID == tbladminrole.RoleID).FirstOrDefault();
                if (data != null)
                {
                    var data2 = context.tbladminroles.Single(c => c.RoleID == tbladminrole.RoleID);
                    data2.RoleName = tbladminrole.RoleName;
                    result = context.SaveChanges();

                }
                else
                {
                    tbladminrole.StatusID = 1;
                    tbladminrole.CreatedBy = 1;
                    tbladminrole.ModifiedBy = 1;
                    context.tbladminroles.Add(tbladminrole);
                    result = context.SaveChanges();
                }
            }
            
                return result;
        }

        public static tbladminrole GetAdminRole(int Id)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tbladminroles.Single(c => c.RoleID == Id));
            }
        }
        public static int DeleteAdminRole(int id, int StatusId)
        {
            int result = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tbladminroles.Single(c => c.RoleID == id);
                data.StatusID = StatusId;
                result = context.SaveChanges();
                //context.tbladminroles.Remove(data);
            }
                return result;
        }

        public static int FinalDeleteAdminRole(int id)
        {
            int result = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tbladminroles.Single(c => c.RoleID == id);
                context.tbladminroles.Remove(data);

                var adminUser = context.tblAdminUsers.Where(x => x.RoleID == id).ToList();
                if (adminUser.Count > 0 )
                {
                    foreach (var item in adminUser)
                    {
                        var dataAdmin = context.tblAdminUsers.Single(c => c.AdminId == item.AdminId);
                        context.tblAdminUsers.Remove(dataAdmin);
                    }
                }
                
                result = context.SaveChanges();
            }
            return result;
        }

        public static List<tbladminrole> GetAdminRoleList()
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tbladminroles.OrderBy(c => c.RoleID).AsEnumerable().ToList());
            }
        }

        public static List<tbladminrole> GetAdminRoleList(int IsActive)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tbladminroles.Where(c => c.StatusID == IsActive).OrderByDescending(c => c.RoleID).AsEnumerable().ToList());
            }
        }

    }
}
