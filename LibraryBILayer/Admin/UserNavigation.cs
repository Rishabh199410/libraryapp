﻿using LibraryBILayer.Model;
using LibraryDataLayer;
using LibraryUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryBILayer.Admin
{
    public class UserNavigation
    {
        #region Update or Delete
        public static int AddorUpdateUserNavigation(tblusernavigation obj)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblusernavigations.Where(c => c.UserNavigationID == obj.UserNavigationID).FirstOrDefault();
                if (data != null)
                {
                    if (obj.UserNavigationID > 0)
                    {
                        var data2 = context.tblusernavigations.Single(c => c.UserNavigationID == obj.UserNavigationID);
                        data2.UserNavigation = obj.UserNavigation;
                        data2.Order = obj.Order;
                        i = context.SaveChanges();
                        // data2.
                    }

                }
                else
                {
                    context.tblusernavigations.Add(obj);
                    i = context.SaveChanges();
                }
            }
            return i;
        }
        #endregion

        #region Get By Id
        public static tblusernavigation GetUserNavigation(int ID)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblusernavigations.Where(c => c.UserNavigationID == ID).FirstOrDefault());
            }
        }
        #endregion

      
        public static List<tblusernavigation> GetUserNavigationList()
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblusernavigations.OrderBy(c => c.Order).AsEnumerable().ToList());
            }
        }

        public static List<AdminNavigationModel> GetAdminNavigationList(int RoleID)
        {
            using (var context = new XpertWebEntities())
            {
                var list = (from claims in context.tbladminroleclaims
                            join nav in context.tbladminnavigations on claims.AdminNavigationID equals nav.AdminNavigationID
                            where claims.RoleID == RoleID && nav.StatusID == (Int32)Enums.RowStatusID.Active
                            select new AdminNavigationModel
                            {
                                AdminNavigationID = nav.AdminNavigationID,
                                AdminNavigation = nav.AdminNavigation,
                                AreaName = nav.AreaName,
                                Controller = nav.Controller,
                                Action = nav.Action,
                                StatusID = nav.StatusID,
                                IsParent = nav.IsParent,
                                ParentID = nav.ParentID,
                                Glyphicon = nav.Glyphicon
                            }).AsEnumerable().ToList();

                return list;
            }
        }


        public static int DeleteUserNavigation(int ID, int IsActive)
        {
            int result = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblusernavigations.Single(c => c.UserNavigationID == ID);
                data.ModifiedBy = 1;
                data.ModifiedOn = DateTime.Now;
                data.StatusID = IsActive; 
                result = context.SaveChanges();
            }
            return result;
        }

    }
}
