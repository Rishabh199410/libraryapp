﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryDataLayer;
namespace LibraryBILayer.Admin
{
    public class ManageBlog
    {
        public static int AddOrUpdateBlog(tblBlogLib obj)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblBlogLibs.Where(c => c.Title == obj.Title).FirstOrDefault();
                if (data != null)
                {
                    if (obj.ID > 0)
                    {
                        var data2 = context.tblBlogLibs.Single(c => c.ID == obj.ID);
                        data2.MetaDesc = obj.MetaDesc;
                        data2.VirtualUrl = obj.VirtualUrl;
                        data2.Content = obj.Content;
                        data2.UpdatedBy = obj.UpdatedBy;
                        data2.Image = obj.Image;
                        data2.BlogCat = obj.BlogCat;
                        data2.MetaKey = obj.MetaKey;
                        data2.UpdatedOn = obj.UpdatedOn;
                        data2.Title = obj.Title;
                        i = context.SaveChanges();
                        // data2.
                    }
                  
                }
                else
                {
                    context.tblBlogLibs.Add(obj);
                    i = context.SaveChanges();
                }
            }
            return i;
        }
        public static List<tblBlogLib> GetBlogList(bool IsActive)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblBlogLibs.Where(c => c.IsActive == IsActive && c.BlogCat!= null).OrderByDescending(c => c.ID).AsEnumerable().ToList());
            }
        }

        public static List<tblBlogLib> GetLatestBlogByCategory(int? CategoryID)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblBlogLibs.Where(c => c.BlogCat == CategoryID).OrderByDescending(c => c.CreatedOn).Take(10).AsEnumerable().ToList());
            }
        }

        public static List<tblBlogCat> GetBlogCatListD(bool IsActive)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblBlogCats.Where(c => c.IsActive == IsActive).OrderByDescending(c => c.Id).AsEnumerable().ToList());
            }
        }
        public static tblBlogCat GetBlogCatListDUrl(string url)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblBlogCats.Where(c => c.Virtual_Url == url).OrderByDescending(c => c.Id).FirstOrDefault());
            }
        }
        public static List<tblBlogLib> GetBlogCatListDUrls(int BlogCat)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblBlogLibs.Where(c => c.BlogCat == BlogCat).OrderByDescending(c => c.ID).AsEnumerable().ToList());
            }
        }
        public static tblBlogLib GetBogLib(int ID)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblBlogLibs.Where(c => c.ID == ID).FirstOrDefault());
            }
        }
        public static tblBlogLib GetBogLib(string Url)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblBlogLibs.Where(c => c.VirtualUrl == Url).FirstOrDefault());
            }
        }
        public static int DeleteMyBlog(int Id)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblBlogLibs.Single(c => c.ID == Id);
                //context.tblSolutionLibraries.delete
                context.tblBlogLibs.Remove(data);
                i = context.SaveChanges();
                return i;
            }
        }
        public static int DeleteBlogs(int ID, bool IsActive, string email)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblBlogLibs.Single(c => c.ID == ID);
                data.IsActive = IsActive;
                data.UpdatedBy = email;
                i = context.SaveChanges();
                return i;
            }
        }
        public static void DeleteBlog(int ID, bool IsActive)
        {
            using (var context = new XpertWebEntities())
            {
                var data = context.tblBlogLibs.Single(c => c.ID == ID);
                data.IsActive = IsActive;
                context.SaveChanges();
            }
        }

        #region BLOG CATEGORY

        public static int AddOrUpdateBlogCat(tblBlogCat obj)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblBlogCats.Where(c => c.BlogCatName == obj.BlogCatName).FirstOrDefault();
                if (data != null)
                {
                    if (obj.Id > 0)
                    {
                        var data2 = context.tblBlogCats.Single(c => c.Id == obj.Id);
                        data2.Meta_Desc = obj.Meta_Desc;
                        data2.Virtual_Url = obj.Virtual_Url;
                        data2.BlogCatName = obj.BlogCatName;
                        data2.UpdatedBy = obj.UpdatedBy;
                        data2.UpdatedOn = obj.UpdatedOn;
                        data2.Meta_Title = obj.Meta_Title;
                        data2.Meta_Keyword = obj.Meta_Keyword;
                        i = context.SaveChanges();
                        // data2.
                    }

                }
                else
                {
                    context.tblBlogCats.Add(obj);
                    i = context.SaveChanges();
                }
            }
            return i;
        }

        public static List<tblBlogCat> GetBlogCatList(bool IsActive)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblBlogCats.Where(c => c.IsActive == IsActive).OrderByDescending(c => c.Id).AsEnumerable().ToList());
            }
        }
        public static tblBlogCat GetBogCat(int ID)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblBlogCats.Where(c => c.Id == ID).FirstOrDefault());
            }
        }
        public static tblBlogCat GetBogCat(string Url)
        {
            using (var context = new XpertWebEntities())
            {
                return (context.tblBlogCats.Where(c => c.Virtual_Url == Url).FirstOrDefault());
            }
        }
        public static int DeleteMyBlogCat(int Id)
        {
            int i = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblBlogCats.Single(c => c.Id == Id);
                //context.tblSolutionLibraries.delete
                context.tblBlogCats.Remove(data);
                i = context.SaveChanges();
                return i;
            }
        }
        public static int DeleteBlogCat(int ID, bool IsActive, string email)
        {
            int result = 0;
            using (var context = new XpertWebEntities())
            {
                var data = context.tblBlogCats.Single(c => c.Id == ID);
                data.UpdatedBy = email;
                data.UpdatedOn = DateTime.Now;
                data.IsActive = IsActive;
                result=context.SaveChanges();
            }
            return result;
        }

        #endregion
    }
}
