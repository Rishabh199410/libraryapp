﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryBILayer.Admin
{
    public class QuestionLib
    {
        public int ID { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string QuestionID { get; set; }
        public string StudentID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string File { get; set; }
        public int? SolID { get; set; }
        public string Phone { get; set; }
        public string Question { get; set; }
        public int? TotalWords { get; set; }
        public decimal? EstimatedPrice { get; set; }
        public string Time { get; set; }
        public int? StatusID { get; set; }
        public string Service { get; set; }
        public string EductionLevel { get; set; }
        public string Status { get; set; }
        public string PaymentStatus { get; set; }
        public int ChangeStatusID { get; set; }
        public List<string> ServiceList { get; set; }
    }
}
