﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryUtility
{
    public class Enums
    {
        public enum ServiceTypeID
        {
            Question24 = 1,
            PlagiarismChecker = 2,
            ProofReading = 3,
            CDRServices = 4
        }

        public enum StatusID
        {
            Pending = 1,
            Completed = 2,
            Rejected = 3           
        }

        public enum PaymentStatus
        {
            Pending =1,
            Paid = 2, 
            Declined = 3
            
        }
        public enum RowStatusID
        {
            Active = 1,
            Deactive = 2,
            Deleted = 3
        }
    }
}
